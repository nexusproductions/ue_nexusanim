﻿
#include "QuaternionMath.h"

class TwistBones 
{
public:
    FTransform* RotationSource;
    EAxis::Type TwisAxis;

    TArray<FTransform> TwistDistribution;
private:
    TArray<FQuat> _restRotations;
public:
    bool RotationSourceIsParent = true;

    bool UpdateAtAll;

    float UpDirectionLerp = 0.25f;

    void Start()
    {
        _restRotations.Init(FQuat(), TwistDistribution.Num());
        for (int i = 0; i < TwistDistribution.Num(); i++)
        {
            _restRotations[i] = TwistDistribution[i].GetRotation();
        }
    }

    // We need to reset the twist because in our retargetting we are setting the final world rotation, and this would be affected by the parents if the parents are the twist bones
    void ResetTwistBones()
    {
        for (int i = 0; i < TwistDistribution.Num(); i++)
        {
            TwistDistribution[i].SetRotation(_restRotations[i]);
        }
    }

    // We want this to update after the animation system, so our animation system has to call this method.
    
    void UpdateTwist()
    {
        if (!UpdateAtAll) return;
        // Swing / twist decomposition, so the twist can be distributed on multiple joints
        FQuat rotation = RotationSource->GetRotation();// * Quaternion.Inverse(_rotationSourceRest);
        // Get the up direction part of the way to this rotation point to avoid bad numerical situations near the 'pole' of the up direction
        FVector upDirection = FQuat::Slerp(FQuat::Identity, rotation, UpDirectionLerp) * FVector::UpVector;
        
        FQuat space = FRotationMatrix::MakeFromXZ(rotation * FVector::ForwardVector, upDirection).ToQuat();
        
        FQuat twist = FQuat::Identity;
        FQuat swing = FQuat::Identity;
        // If the rotation source is the top parent (for example shoulder), then the swing has to take place in the space of it's parent.
        if (RotationSourceIsParent)
        {
            FQuat rotationInSpace = space.Inverse() * rotation;
            twist = QuaternionMath::Twist(rotationInSpace, TwisAxis);
            swing = QuaternionMath::Swing(rotationInSpace, twist);
        }
        else // If it's the bottom child (like the hand) we just use the direct rotation
        {
            twist = QuaternionMath::Twist(rotation, TwisAxis);
            swing = QuaternionMath::Swing(rotation, twist);
        }
        
        float twistAmount = 1.0f / (TwistDistribution.Num());
        FQuat distributedTwist = FQuat::Slerp(FQuat::Identity, twist, twistAmount);
        for (int i = 0; i < TwistDistribution.Num(); i++)
        {
            if (TwistDistribution[i].Identical(RotationSource, 0))
            {
                // If the rotation source is the top parent, then the swing has to take place in the space of it's parent.
                if (RotationSourceIsParent)
                {
                    TwistDistribution[i].GetRotation() = space * distributedTwist * swing;
                }
                else
                {
                    TwistDistribution[i].GetRotation() = distributedTwist * swing;
                }
            }
            else
            { 
                TwistDistribution[i].GetRotation() = distributedTwist;
            }
        }
    }
};
