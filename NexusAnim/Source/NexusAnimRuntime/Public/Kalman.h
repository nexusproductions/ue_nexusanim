﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "Kalman.generated.h"

/**
 * 
 */

UCLASS(BlueprintType)
class NEXUSANIMRUNTIME_API UKalmanFloat : public UObject
{
	GENERATED_BODY()
public:
	UKalmanFloat();
	explicit UKalmanFloat(const FObjectInitializer& ObjectInitializer);

private:
	double Q;
	double R;
	double X;
	double P;
public:
	UFUNCTION(BlueprintCallable, Category = "Kalman")
	float Update(float Value);

	UFUNCTION(BlueprintCallable, Category = "Kalman")
	void SetQ(const float QBase, const float QExponent) {
		Q = FMath::Pow(QBase, QExponent);
	}

	void SetQ(const double NewQ)
	{
		Q = NewQ;
	}

	UFUNCTION(BlueprintCallable, Category="Kalman")
	void SetR(const float RBase, const float RExponent) {
		R = FMath::Pow(RBase, RExponent);
	}

	void SetR(const double NewR)
	{
		R = NewR;
	}
};

UCLASS(BlueprintType)
class NEXUSANIMRUNTIME_API UKalmanVector : public UObject
{
	GENERATED_BODY()
public:
	UKalmanVector();
	explicit UKalmanVector(const FObjectInitializer& ObjectInitializer);

private:
	double Q;
	double R;
	double Xx;
	double Xy;
	double Xz;
	double P;

public:
	UFUNCTION(BlueprintCallable, Category = "Kalman")
	FVector Update(FVector Value);

	UFUNCTION(BlueprintCallable, Category = "Kalman")
	void SetQ(const float QBase, const float QExponent)
	{
		Q = FMath::Pow(QBase, QExponent);
	}

	void SetQ(const double NewQ)
	{
		Q = NewQ;
	}
	
	UFUNCTION(BlueprintCallable, Category = "Kalman")
	void SetR(const float RBase, const float RExponent)
	{
		R = FMath::Pow(RBase, RExponent);
	}

	void SetR(const double NewR)
	{
		R = NewR;		
	}
};
