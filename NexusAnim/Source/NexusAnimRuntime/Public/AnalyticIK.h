﻿#pragma once

static void SoftIK(const FVector rootPosition, const FVector IKGoalPosition,
	const float ChainLength, const float Softness, const float Stretchiness,
	FVector& IKPosition, float& StretchFactor)
{
	FVector TargetPos = IKGoalPosition;
	const float SoftDist = Softness * ChainLength / 2;
	FVector DiffPos = TargetPos - rootPosition;
	const float Curlen = DiffPos.Size();
	IKPosition = IKGoalPosition;
	const float Diff = Curlen - (ChainLength - SoftDist);
	
	if (Diff > 0 && SoftDist >= 0.00001)
	{
		DiffPos = DiffPos * (1.0F / Curlen);
		const float DA = ChainLength - SoftDist;
		const float NewLength = SoftDist * (1.0f - FMath::Exp(-(Curlen - DA) / SoftDist)) + DA;
		DiffPos *= NewLength;
		TargetPos = DiffPos + rootPosition;
		IKPosition = TargetPos;
	};
	StretchFactor = 1.0f;
	if (Stretchiness > 0.0) {
		if (SoftDist < 0.00001f) {
			StretchFactor = FMath::Clamp(ChainLength / Curlen, 0.00001f, 1.0f);
		}
		else {
			StretchFactor = FVector::Distance(rootPosition, IKPosition) / FVector().Dist(rootPosition, IKGoalPosition);
		};
	};
	StretchFactor = FMath::Clamp(StretchFactor, 0.00001f, 1.0f);
}

inline bool CalcIK_2DTwoBoneAnalytic(const bool bFlipSolveDirection,
                                     const float Length1, const float Length2, const float TargetX, const float TargetY,
                                     float& Angle1, float& Angle2) {
	constexpr float Epsilon = 0.0001f; // used to prevent division by small numbers
	bool bFoundValidSolution = true;
	float targetDistSqr = (TargetX * TargetX + TargetY * TargetY);
	//===
	// Compute a new value for angle2 along with its cosine
	float SinAngle2;
	float CosAngle2;

	const float CosAngle2Denom = 2 * Length1 * Length2;
	if (CosAngle2Denom > Epsilon)
	{
		CosAngle2 = (Length1 * Length1 + Length2 * Length2 - targetDistSqr) / (CosAngle2Denom);

		// if our result is not in the legal cosine range, we can not find a
		// legal solution for the target
		if ((CosAngle2 < -1.0) || (CosAngle2 > 1.0)) {
			bFoundValidSolution = false;
		}

		// clamp our value into range so we can calculate the best
		// solution when there are no valid ones
		CosAngle2 = FMath::Max(-1.0f, FMath::Min(1.0f, CosAngle2));

		// compute a new value for angle2
		Angle2 = FMath::Acos(CosAngle2);
		Angle2 = PI - Angle2;
		
		// adjust for the desired bend direction
		if (!bFlipSolveDirection)
			Angle2 = -Angle2;

		// compute the sine of our angle
		SinAngle2 = FMath::Sin(Angle2);
	}
	else
	{
		// At leaset one of the bones had a zero length. This means our
		// solvable domain is a circle around the origin with a radius
		// equal to the sum of our bone lengths.
		float totalLenSqr = (Length1 + Length2) * (Length1 + Length2);
		if (targetDistSqr < (totalLenSqr - Epsilon) || targetDistSqr >(totalLenSqr + Epsilon)) {
			bFoundValidSolution = false;
		}

		// Only the value of angle1 matters at this point. We can just
		// set angle2 to zero. 
		Angle2 = 0.0f;
		CosAngle2 = 1.0f;
		SinAngle2 = 0.0f;
	}

	// Calculate angle1 using the same maths as angle 2
	float cosAngle1 = (Length1 * Length1 + targetDistSqr - Length2 * Length2) / (2 * FMath::Sqrt(targetDistSqr)*Length1);
	if ((cosAngle1 < -1.0) || (cosAngle1 > 1.0)) {
		bFoundValidSolution = false;
	}
	cosAngle1 = FMath::Max(-1.0f, FMath::Min(1.0f, cosAngle1));
	Angle1 = FMath::Acos(cosAngle1);
	
	//===
	// 
	/*
	// Compute the value of angle1 based on the sine and cosine of angle2
	// This gives me strange results?
	float triAdjacent = length1 + length2 * cosAngle2;
	float triOpposite = length2 * sinAngle2;

	float tanY = targetY * triAdjacent - targetX * triOpposite;
	float tanX = targetX * triAdjacent + targetY * triOpposite;

	// Note that it is safe to call Atan2(0,0) which will happen if targetX and
	// targetY are zero
	angle1 = FMath::Atan2(tanY, tanX);
	*/
	
	return bFoundValidSolution;
}

static FVector CalculatePoleVectorLocation(FTransform Root, FTransform Bend, float LocalOffset = 0.1f, FVector LocalDirection = FVector::ForwardVector) {
	return Bend.GetTranslation() + Bend.GetRotation().RotateVector(LocalDirection) * LocalOffset;
}

static FVector SpaceTransferLocation(
	FVector Location,
	const FTransform FromSpace = FTransform::Identity,
	const FTransform ToSpace = FTransform::Identity,
	const float ScaleRatio = 1.0f, const FRotator Rotation = FRotator::ZeroRotator)
{
	Location = FromSpace.ToMatrixNoScale().InverseTransformPosition(Location);
	Location = Location * ScaleRatio;
	Location = Rotation.RotateVector(Location);
	return ToSpace.ToMatrixNoScale().TransformPosition(Location);
}

static FVector SpaceTransferLocationNonUniform(
	FVector Location,
	const FTransform FromSpace = FTransform::Identity,
	const FTransform ToSpace = FTransform::Identity,
	const FVector ScaleRatio = FVector::OneVector,
	const FRotator Rotation = FRotator::ZeroRotator)
{
	Location = FromSpace.ToMatrixNoScale().InverseTransformPosition(Location);
	// Multiply by the arm ratio
	Location = Location * ScaleRatio;
	Location = Rotation.RotateVector(Location);
	return ToSpace.ToMatrixNoScale().TransformPosition(Location);
}