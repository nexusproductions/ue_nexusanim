﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "NexusMocap.h"

#include "Kalman.h"
//#include "RBFCalibrationDataBP.h"
#include "RBFSolverNexus.h"
#include "Components/ActorComponent.h"
#include "RBFComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRBFComponent, Log, All);

USTRUCT(BlueprintType)
struct FNamedCurve {
	GENERATED_BODY()
public:

	FNamedCurve(FName Name = "", float Weight = 0.0f) : Name(Name), Weight(Weight) {}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Weight;
};

USTRUCT(BlueprintType)
struct FRBFShape {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FNamedCurve> InputChannels;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FNamedCurve> OutputChannels;
};


UCLASS(BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NEXUSANIMRUNTIME_API URBFComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	//UPROPERTY()
	URBFSolver Rbf;

	// Sets default values for this component's properties
	URBFComponent();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, FRBFShape> CalibrationShapes;
	//TArray<KalmanFilter> KalmanFilters;

	UPROPERTY(VisibleAnywhere)
	TArray<double> RBFOutputs;
	UPROPERTY(EditAnywhere)
	TEnumAsByte<ERBFSmoothingType> SmoothingType;
	UPROPERTY(EditAnywhere)
	double SmoothingRadius;

	UPROPERTY(EditAnywhere)
	TArray<FName> PositiveBlendShapeOutputs;
	UPROPERTY(EditAnywhere)
	TArray<FName> NegativeBlendShapeOutputs;
	UPROPERTY(BlueprintReadWrite)
	TMap<FName, float> InData;
	UPROPERTY(BlueprintReadOnly)
	TMap<FName, float> OutData;
	UPROPERTY(EditAnywhere)
	double KalmanQ;
	UPROPERTY(EditAnywhere)
	double KalmanR;
	UPROPERTY(BlueprintReadOnly)
	bool isSetup;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void PruneInDataToChannels(TSet<FName> Channels, TMap<FName, float> InputChannels, TMap<FName, float>& PrunedInputChannels);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void CalibrateShape(FName ShapeName, TMap<FName, float> InputChannels, TArray<FNamedCurve> OutputChannels, FRBFShape& OutShape);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void SetupRBF();

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void UpdateRBF();

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TArray<FNamedCurve> CurvesMapToNamedChannelArray(TMap<FName, float> Curves);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TMap<FName, float> NamedChannelArrayToCurvesMap(TArray<FNamedCurve> Curves);
};
