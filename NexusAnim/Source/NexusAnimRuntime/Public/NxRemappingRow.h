﻿// Copyright 2018 Xsens Technologies B.V., Inc. All Rights Reserved.

#pragma once
#include "NxMappingEnum.h"
#include "Engine\DataTable.h"

#include "NxRemappingRow.generated.h"


USTRUCT(BlueprintType)
struct FNxRemappingRowHandle : public FDataTableRowHandle
{
	GENERATED_USTRUCT_BODY()

public:
	FNxRemappingRowHandle()
		: XsensRemapId(ENxXsensMapping::Root), RemapId("None")
	{}

	FNxRemappingRowHandle(ENxXsensMapping XsensId, FName RemapId)
		: XsensRemapId(XsensId), RemapId(RemapId)
	{}

	/** XSens Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = RemappingRow)
	ENxXsensMapping XsensRemapId;

	/** UE4 Remap Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = RemappingRow)
	FName RemapId;
};