﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// #include <iostream>
#include "NMatrix.h"
#include "Eigen"
#include "UObject/Object.h"
#include "CoreMinimal.h"
//#include "MathUtil.h"
#include <string>
#include <sstream>
#include "Containers/StringConv.h"
#include "RBFSolver.generated.h"

//#define EPSILON = 0.00000001
using namespace Eigen;
typedef Matrix<double, Dynamic, Dynamic, RowMajor> RowMatrix;
typedef Matrix<double, Dynamic, Dynamic, ColMajor> ColMatrix;

DECLARE_LOG_CATEGORY_EXTERN(LogRBF, Log, All);


UENUM()
enum ERBFSmoothingType {
	None,
	InverseMultiQuadraticBiharmonic,
	BeckertWendlandC2Basis
};

//UCLASS(Blueprintable)
class NEXUSANIMRUNTIME_API URBFSolver
{
public:
	void Init(FNMatrix sampleArray, FNMatrix outputValues);
	double Distance(TArray<double> vectorA, TArray<double> vectorB);

	TArray<double> Interpolate(TArray<double> newSample);
	static RowMatrix MakeRowMatrixFromnxMatrix(FNMatrix inMatrix);
	static FNMatrix MakenxMatrixFromRowMatrix(RowMatrix inMatrix);
	TEnumAsByte<ERBFSmoothingType> SmoothingType;
	double smoothingRadius;
private:
	static RowMatrix PseudoInverse(const RowMatrix& a, double epsilon);
//	UPROPERTY()
	FNMatrix _sampleArray;
//	UPROPERTY()
	FNMatrix _featureMatrix;
//	UPROPERTY()
	TArray<double> _featureNormals;
//	UPROPERTY()
	FNMatrix _distances;
//	UPROPERTY()
	FNMatrix _theta;
//	UPROPERTY()
	FNMatrix _outputValues;

	double _distancesLength;

	static FString RowMatrixToString(const RowMatrix& mat) {
		std::stringstream ss;
		ss << mat;
		std::string s = ss.str();
		FString fs = s.c_str();
		return fs;
	}

	static FString ColMatrixToString(const ColMatrix& mat) {
		std::stringstream ss;
		ss << mat;
		std::string s = ss.str();
		FString fs = s.c_str();
		return fs;
	}

	static FNMatrix InverseMultiQuadraticBiharmonic(FNMatrix matrix, double radius);
	static FNMatrix BeckertWendlandC2Basis(FNMatrix matrix, double radius);
	//static VectorXd InverseMultiQuadraticBiharmonic(VectorXd matrix, double radius);
};
