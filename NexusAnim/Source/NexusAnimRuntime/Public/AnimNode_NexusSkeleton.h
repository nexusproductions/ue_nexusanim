﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "NexusSkeleton.h"
#include "RetargetNexusSkeleton.h"
#include "Animation/AnimNodeBase.h"
#include "AnimNode_NexusSkeleton.generated.h"
/**
 *
 */

USTRUCT(BlueprintType)
struct NEXUSANIMRUNTIME_API FAnimNode_NexusSkeleton : public FAnimNode_Base
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink BasePose;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NexusSkeleton, meta = (PinShownByDefault))
	UNxSkeletonBase* NexusSkeleton;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NexusSkeleton, meta = (PinShownByDefault))
	TMap<TEnumAsByte<EMocapJoint>, FName> Remapping;
	UPROPERTY()
	URetargetNexusSkeleton* RetargetNexusSkeleton;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NexusSkeleton)
	USkeletalMesh* SkeletalMesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NexusSkeleton)
	UAnimationAsset* TPoseAnimationAsset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NexusSkeleton, meta = (PinShownByDefault))
	TMap<FName, FTransform> InputTransforms;
	
	
public:
	FAnimNode_NexusSkeleton();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext& Context) override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;
	// End of FAnimNode_Base interface
};
