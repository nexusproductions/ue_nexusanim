﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NexusSkeleton.h"
#include "GameFramework/Actor.h"
#include "NexusSkeletonTest.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogNexusSkeletonTest, Log, All);

UCLASS(Blueprintable)
class NEXUSANIMRUNTIME_API ANexusSkeletonTest : public AActor
{
	GENERATED_BODY()

	


	
public:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UNxSkeletonXsens* NxSkeleton;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UNxSkeletonXsens* NxSkeleton2;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<FName, FTransform> InitialWorldTransforms;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<FTransform> InitialXsensTransforms =
		TArray<FTransform>{
			FTransform(FVector(0, 0, 0)), //Root
			FTransform(FVector(0, 0, 20)), //Pelvis
			FTransform(FVector(0, 0, 4)), //L5
			FTransform(FVector(0, 0, 4)), //L3
			FTransform(FVector(0, 0, 4)), //T12
			FTransform(FVector(0, 0, 4)), //T8
			FTransform(FVector(0, 0, 4)), //Neck
			FTransform(FVector(0, 0, 4)), //Head
			FTransform(FVector(2, 0, 0)), //RightShoulder
			FTransform(FVector(5, 0, 0)), //RightUpperArm
			FTransform(FVector(5, 0, 0)), //RightForeArm
			FTransform(FVector(5, 0, 0)), //RightHand
			FTransform(FVector(-2, 0, 0)), //LeftShoulder
			FTransform(FVector(-5, 0, 0)), //LeftUpperArm
			FTransform(FVector(-5, 0, 0)), //LeftForeArm
			FTransform(FVector(-5, 0, 0)), //LeftHand
			FTransform(FVector(3, 0, 0)), //RightUpperLeg
			FTransform(FVector(0, 0, -8)), //RightLowerLeg
			FTransform(FVector(0, 0, -8)), //RightFoot
			FTransform(FVector(0, 4, -4)), //RightToe
			FTransform(FVector(-3, 0, 0)), //LeftUpperLeg
			FTransform(FVector(0, 0, -8)), //LeftLowerLeg
			FTransform(FVector(0, 0, -8)), //LeftFoot
			FTransform(FVector(0, 4, -4)), //LeftToe
			FTransform(FVector(0, 0, 0)), //Prop1
			FTransform(FVector(0, 0, 0)), //Prop2
			FTransform(FVector(0, 0, 0)), //Prop3
			FTransform(FVector(0, 0, 0)), //Prop4
			FTransform(FVector(0, 0, 0)), //LeftCarpus
			FTransform(FVector(0, 0, 0)), //LeftFirstMC
			FTransform(FVector(0, 0, 0)), //LeftFirstPP
			FTransform(FVector(0, 0, 0)), //LeftFirstDP
			FTransform(FVector(0, 0, 0)), //LeftSecondMC
			FTransform(FVector(0, 0, 0)), //LeftSecondPP
			FTransform(FVector(0, 0, 0)), //LeftSecondMP
			FTransform(FVector(0, 0, 0)), //LeftSecondDP
			FTransform(FVector(0, 0, 0)), //LeftThirdMC
			FTransform(FVector(0, 0, 0)), //LeftThirdPP
			FTransform(FVector(0, 0, 0)), //LeftThirdMP
			FTransform(FVector(0, 0, 0)), //LeftThirdDP
			FTransform(FVector(0, 0, 0)), //LeftFourthMC
			FTransform(FVector(0, 0, 0)), //LeftFourthPP
			FTransform(FVector(0, 0, 0)), //LeftFourthMP
			FTransform(FVector(0, 0, 0)), //LeftFourthDP
			FTransform(FVector(0, 0, 0)), //LeftFifthMC
			FTransform(FVector(0, 0, 0)), //LeftFifthPP
			FTransform(FVector(0, 0, 0)), //LeftFifthMP
			FTransform(FVector(0, 0, 0)), //LeftFifthDP
			FTransform(FVector(0, 0, 0)), //RightCarpus
			FTransform(FVector(0, 0, 0)), //RightFirstMC
			FTransform(FVector(0, 0, 0)), //RightFirstPP
			FTransform(FVector(0, 0, 0)), //RightFirstDP
			FTransform(FVector(0, 0, 0)), //RightSecondMC
			FTransform(FVector(0, 0, 0)), //RightSecondPP
			FTransform(FVector(0, 0, 0)), //RightSecondMP
			FTransform(FVector(0, 0, 0)), //RightSecondDP
			FTransform(FVector(0, 0, 0)), //RightThirdMC
			FTransform(FVector(0, 0, 0)), //RightThirdPP
			FTransform(FVector(0, 0, 0)), //RightThirdMP
			FTransform(FVector(0, 0, 0)), //RightThirdDP
			FTransform(FVector(0, 0, 0)), //RightFourthMC
			FTransform(FVector(0, 0, 0)), //RightFourthPP
			FTransform(FVector(0, 0, 0)), //RightFourthMP
			FTransform(FVector(0, 0, 0)), //RightFourthDP
			FTransform(FVector(0, 0, 0)), //RightFifthMC
			FTransform(FVector(0, 0, 0)), //RightFifthPP
			FTransform(FVector(0, 0, 0)), //RightFifthMP
			FTransform(FVector(0, 0, 0)), //RightFifthDP"

		};
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere);
	TArray<FTransform> XsensWorldTransforms;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<FName, FName> Parents;
	// Sets default values for this actor's properties
	ANexusSkeletonTest();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	void InitSkeleton();
	UFUNCTION(BlueprintCallable)
	void Draw();
};
