﻿#pragma once

#include "NexusSkeleton.h"
#include "Kalman.h"
#include "Animation/PoseAsset.h"
#include "RetargetNexusSkeleton.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogNexusSkeleton, Log, All);

UCLASS(BlueprintType)
class URetargetNexusSkeleton : public UObject
{
	GENERATED_BODY()
public:
	
	void Init(USkeletalMesh* InSkeletalMesh, UAnimationAsset* InTPoseAnimationAsset)
	{
		SkeletalMesh = InSkeletalMesh;
		TPoseAnimationAsset = InTPoseAnimationAsset;
		FootHeightFalloff = 0.01f;
		//MocapSkeleton = MakeShared<UNxSkeletonMocap>(UNxSkeletonMocap());
		LeftHandKalman = NewObject<UKalmanVector>();
		RightHandKalman = NewObject<UKalmanVector>();
		LeftFootKalman = NewObject<UKalmanVector>();
		RightFootKalman = NewObject<UKalmanVector>();
	}
	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	USkeletalMesh* SkeletalMesh;
	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	UAnimationAsset* TPoseAnimationAsset;
	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	UPoseAsset* TPoseAsset;

	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	UNxSkeletonMocap* MocapSkeleton;
	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	UNxSkeletonMocap* RetargetSkeleton;
	UPROPERTY(BlueprintReadWrite, Category=NexusRetarget)
	UNxSkeletonBase* FinalSkeleton;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=NexusRetarget)
	TMap<TEnumAsByte<EMocapJoint>, FName> MocapToFinalRemap;

	TMap<FName, FTransform> MocapWorldTransforms;
	TMap<FName, FTransform> MocapLocalTransforms;
	TArray<FQuat> FinalRotations;
	
	float RetargetArmLength1 = 0.0f;
	float RetargetArmLength2 = 0.0f;
	float RetargetLegLength1 = 0.0f;
	float RetargetLegLength2 = 0.0f;
	float IKSoftness = 0.01f;
	float ArmRatio = 1.0f;
	float HipsRatio = 1.0f;
	float FootHeightRatio = 1.0f;
	float SourceFootHeight = 0.0f;
	float FootHeightFalloff = 0.01f;

	UPROPERTY()
	UKalmanVector* LeftHandKalman;
	UPROPERTY()
	UKalmanVector* RightHandKalman;
	UPROPERTY()
	UKalmanVector* LeftFootKalman;
	UPROPERTY()
	UKalmanVector* RightFootKalman;
	
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void UpdateMocapSkeletonRotations(TMap<FName, FQuat> JointRotations) const;

	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void UpdateMocapSkeletonTranslations(TMap<FName, FVector> JointTranslations) const;

	void GetFinalJointRotations()
	{
		FinalRotations.Init(FQuat::Identity, 0);
		//FinalRotations.Add()
	}
	
	FName GetRemappedJointName(int32 JointIndex) const;
	
	FName GetRemappedJointName(const FName JointName) const;
	
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void RemapFinalSkeleton() const;
	
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void RetargetMocapSkeleton() const;

	void RetargetLeftHand() const
	{
		FTransform SourceChest = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3);
		FTransform SourceShoulder = MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftShoulder);
		FTransform SourceArm = MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftArm);
		FTransform SourceForeArm = MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftForeArm);
		FTransform SourceHand = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftHand);
		
		FTransform TargetChest = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Spine3);
		FTransform TargetShoulder = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftShoulder);
		FTransform TargetArm = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftArm);
		FTransform TargetForeArm = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftForeArm);
		
		FVector PoleVector = CalculatePoleVectorLocation(SourceArm, SourceForeArm, 0.1, FVector::BackwardVector);
		PoleVector = SpaceTransferLocation(PoleVector,SourceShoulder,TargetShoulder, ArmRatio);
		
		// Space Transfer Hands to set IK Target
		FVector IKTarget = SpaceTransferLocation(SourceHand.GetTranslation(), SourceChest, TargetChest, ArmRatio);
		
		IKTarget = LeftHandKalman->Update(IKTarget);
		
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget-TargetArm.GetTranslation(),
			PoleVector - TargetArm.GetTranslation()).ToQuat();
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetArm.GetTranslation());
		// MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3).InverseTransformPosition()

		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
		
		SoftIK(RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftArm).GetTranslation(),
			IKTarget,
			RetargetArmLength1 + RetargetArmLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);

		// Solve IK
		float ArmAngle;
		float ElbowAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			false,
			RetargetArmLength1,
			RetargetArmLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			ArmAngle,
			ElbowAngle);
		FQuat ArmWorldRotation = RotationOfIKChain * FQuat::MakeFromEuler(FVector::UpVector * ArmAngle);
		FQuat ElbowLocalRotation = FQuat::MakeFromEuler(FVector::UpVector * ElbowAngle);
			
		// Apply Rotations
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::LeftArm, ArmWorldRotation);
		RetargetSkeleton->SetJointLocalRotation(EMocapJoint::LeftForeArm, ElbowLocalRotation);
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::LeftHand, SourceHand.GetRotation());
	}

	void RetargetRightHand() const
	{
		FTransform SourceChest = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3);
		FTransform SourceShoulder = MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightShoulder);
		FTransform SourceArm = MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightArm);
		FTransform SourceForeArm = MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightForeArm);
		FTransform SourceHand = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightHand);
		
		FTransform TargetChest = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Spine3);
		FTransform TargetShoulder = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightShoulder);
		FTransform TargetArm = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightArm);
		FTransform TargetForeArm = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightForeArm);
		
		FVector ArmPoleVector = CalculatePoleVectorLocation(SourceArm, SourceForeArm, 0.1, FVector::BackwardVector);
		ArmPoleVector = SpaceTransferLocation(ArmPoleVector,SourceShoulder,TargetShoulder, ArmRatio);
		
		//FVector IKTarget = SourceChest.InverseTransformPosition(SourceHand);
		
		// Space Transfer Hands to set IK Target
		FVector IKTarget = SpaceTransferLocation(SourceHand.GetTranslation(), SourceChest, TargetChest, ArmRatio);
		
		IKTarget = RightHandKalman->Update(IKTarget);
		
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget-TargetArm.GetTranslation(),
			ArmPoleVector - TargetArm.GetTranslation()).ToQuat();
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetArm.GetTranslation());
		// MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3).InverseTransformPosition()

		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
		
		SoftIK(RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightArm).GetTranslation(),
			IKTarget,
			RetargetArmLength1 + RetargetArmLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);

		// Solve IK
		float ArmAngle;
		float ElbowAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			false,
			RetargetArmLength1,
			RetargetArmLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			ArmAngle,
			ElbowAngle);
		FQuat ArmWorldRotation = RotationOfIKChain * FQuat::MakeFromEuler(FVector::UpVector * ArmAngle);
		FQuat ElbowLocalRotation = FQuat::MakeFromEuler(FVector::UpVector * ElbowAngle);
			
		// Apply Rotations
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::RightArm, ArmWorldRotation);
		RetargetSkeleton->SetJointLocalRotation(EMocapJoint::RightForeArm, ElbowLocalRotation);
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::RightHand, SourceHand.GetRotation());
	}

	void RetargetLeftFoot() const
	{
		FTransform SourceReference = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Reference);
		FTransform SourceHips = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Hips);
		FTransform SourceUpLeg = MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftUpLeg);
		FTransform SourceLeg = MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftLeg);
		FTransform SourceFoot = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftFoot);

		FTransform TargetReference = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Reference);
		FTransform TargetHips = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Hips);
		FTransform TargetUpLeg = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftUpLeg);
		FTransform TargetLeg = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftLeg);
		FTransform TargetFoot = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::LeftFoot);

		FVector PoleVector = CalculatePoleVectorLocation(SourceUpLeg, SourceLeg, 0.1, FVector::ForwardVector);
		PoleVector = SpaceTransferLocation(PoleVector,SourceReference,TargetReference, HipsRatio);
				
		// Space Transfer Feet to set IK Target
		const float AnkleHeightHeuristic = CalculateFootHeightHeuristic(TargetReference.TransformPosition(TargetFoot.GetTranslation()).Z);
		
		FVector IKTarget = SpaceTransferLocationNonUniform(SourceFoot.GetTranslation(),SourceReference,TargetReference,
			FVector(HipsRatio, HipsRatio, FMath::Lerp(FootHeightRatio, HipsRatio, AnkleHeightHeuristic)));

		IKTarget = LeftFootKalman->Update(IKTarget);
		
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget-TargetUpLeg.GetTranslation(),
			PoleVector - TargetUpLeg.GetTranslation()).ToQuat();
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetUpLeg.GetTranslation());
		// MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3).InverseTransformPosition()

		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
		
		SoftIK(RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightArm).GetTranslation(),
			IKTarget,
			RetargetLegLength1 + RetargetLegLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);

		// Solve  IK
		float LegAngle;
		float KneeAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			false,
			RetargetArmLength1,
			RetargetArmLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			LegAngle,
			KneeAngle);
		FQuat LegWorldRotation = RotationOfIKChain * FQuat::MakeFromEuler(FVector::RightVector * LegAngle);
		FQuat KneeLocalRotation = FQuat::MakeFromEuler(FVector::UpVector * KneeAngle);
			
		// Apply Rotations
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::LeftUpLeg, LegWorldRotation);
		RetargetSkeleton->SetJointLocalRotation(EMocapJoint::LeftLeg, KneeLocalRotation);
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::LeftFoot, MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftFoot).GetRotation());
	}

	void RetargetRightFoot() const
	{
		FTransform SourceReference = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Reference);
		FTransform SourceHips = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Hips);
		FTransform SourceUpLeg = MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightUpLeg);
		FTransform SourceLeg = MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightLeg);
		FTransform SourceFoot = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightFoot);

		FTransform TargetReference = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Reference);
		FTransform TargetHips = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::Hips);
		FTransform TargetUpLeg = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightUpLeg);
		FTransform TargetLeg = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightLeg);
		FTransform TargetFoot = RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightFoot);

		FVector PoleVector = CalculatePoleVectorLocation(SourceUpLeg, SourceLeg, 0.1, FVector::ForwardVector);
		PoleVector = SpaceTransferLocation(PoleVector,SourceReference,TargetReference, HipsRatio);
				
		// Space Transfer Feet to set IK Target
		const float AnkleHeightHeuristic = CalculateFootHeightHeuristic(TargetReference.TransformPosition(TargetFoot.GetTranslation()).Z);
		
		FVector IKTarget = SpaceTransferLocationNonUniform(SourceFoot.GetTranslation(),SourceReference,TargetReference,
			FVector(HipsRatio, HipsRatio, FMath::Lerp(FootHeightRatio, HipsRatio, AnkleHeightHeuristic)));

		IKTarget = RightFootKalman->Update(IKTarget);
		
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget-TargetUpLeg.GetTranslation(),
			PoleVector - TargetUpLeg.GetTranslation()).ToQuat();
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetUpLeg.GetTranslation());
		// MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3).InverseTransformPosition()

		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
		
		SoftIK(RetargetSkeleton->GetJointWorldTransform(EMocapJoint::RightArm).GetTranslation(),
			IKTarget,
			RetargetLegLength1 + RetargetLegLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);

		// Solve IK
		float LegAngle;
		float KneeAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			false,
			RetargetLegLength1,
			RetargetLegLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			LegAngle,
			KneeAngle);
		FQuat LegWorldRotation = RotationOfIKChain * FQuat::MakeFromEuler(FVector::RightVector * LegAngle);
		FQuat KneeLocalRotation = FQuat::MakeFromEuler(FVector::UpVector * KneeAngle);
			
		// Apply Rotations
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::RightUpLeg, LegWorldRotation);
		RetargetSkeleton->SetJointLocalRotation(EMocapJoint::RightLeg, KneeLocalRotation);
		RetargetSkeleton->SetJointWorldRotation(EMocapJoint::RightFoot, MocapSkeleton->GetJointWorldTransform(EMocapJoint::RightFoot).GetRotation());
	}
	
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void Setup();
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void Update();
	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void SetupMocapSkeleton();

	UFUNCTION(BlueprintCallable, Category=NexusRetarget)
	void SetupFinalSkeleton();

	void GetArmRatio()
	{
		const float SourceArmLength = MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftShoulder).GetTranslation().Y -
			MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::Spine3).GetTranslation().Y + 
			MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftArm).GetTranslation().Size() +
			MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftForeArm).GetTranslation().Size() +
			MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftHand).GetTranslation().Size();

		const float TargetArmLength = RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftShoulder).GetTranslation().Y -
			RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::Spine3).GetTranslation().Y + 
			RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftArm).GetTranslation().Size() +
			RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftForeArm).GetTranslation().Size() +
			RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::LeftHand).GetTranslation().Size();
		ArmRatio = TargetArmLength / SourceArmLength;
	}

	void GetHipsRatio()
	{
		const float SourceHipHeight = MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::Reference).InverseTransformPosition(
			MocapSkeleton->GetJointWorldRestTransform(EMocapJoint::Hips).GetTranslation()).Z;
		const float TargetHipHeight = RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::Reference).InverseTransformPosition(
			RetargetSkeleton->GetJointWorldRestTransform(EMocapJoint::Hips).GetTranslation()).Z;
		HipsRatio = TargetHipHeight / SourceHipHeight;
	}
	
	void SetupRetargetSkeleton()
	{
		RetargetSkeleton = NewObject<UNxSkeletonMocap>();
		RetargetSkeleton->Init(MocapSkeleton);
		for(TPair<EMocapJoint, FName> Remap : MocapToFinalRemap)
		{
			const FName MocapJointName = RetargetSkeleton->MocapJointNames[Remap.Key];
			const FName FinalJointName = Remap.Value;
			const float JointLength = FinalSkeleton->GetJointLength(FinalJointName);
			FVector ScaledTranslation = RetargetSkeleton->GetJointLocalTransform(MocapJointName).GetTranslation();
			ScaledTranslation.Normalize();
			ScaledTranslation *= JointLength;
			RetargetSkeleton->SetJointLocalTranslation(MocapJointName, ScaledTranslation);
		}
		
		//RetargetSkeleton->Init(MocapSkeleton->GetJointLocalTransforms(), MocapSkeleton->JointParentsMap);
		//RetargetSkeleton = MakeShared<UNxSkeletonMocap>(UNxSkeletonMocap());
		// Get Translation using the remap table to look up world transforms on the final skeleton to get length of bones
		// Scale all bones by this?
	}

	UFUNCTION(BlueprintPure, Category=NexusRetarget)
	float CalculateFootHeightHeuristic(const float FootHeight) const
	{
		return FMath::Clamp(1.0f - FMath::Abs(FootHeight - SourceFootHeight) * FootHeightFalloff, 0.0f, 1.0f);
	}
};
