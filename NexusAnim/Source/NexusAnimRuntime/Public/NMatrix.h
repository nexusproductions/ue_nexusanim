﻿#pragma once


#include "NMatrix.generated.h"



USTRUCT()
struct FNMatrix
{
	GENERATED_BODY()

	FNMatrix();
	FNMatrix(size_t numrows, size_t numcols);
	FNMatrix(TArray<double> vector);
	private:
	UPROPERTY()
	int rows;
	UPROPERTY()
	int columns;
public:
	UPROPERTY()
	TArray<double> matrix;

	double& operator()(int row, int column)
	{
		return matrix[row * columns + column]; // note 2D coordinates are flattened to 1D
	}

	double operator()(int row, int column) const
	{
		return matrix[row * columns + column];
	}


	size_t getRows() const
	{
		return rows;
	}

	size_t getColumns() const
	{
		return columns;
	}

	FString ToString() {
		FString outputString = "";
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {
				outputString += FString::SanitizeFloat(this->operator()(r, c));
				outputString += ", ";
			} 
			outputString += "\n";
		}
		return outputString;
	}
};