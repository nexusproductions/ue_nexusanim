﻿#pragma once

#include "CoreMinimal.h"

#include "UObject/Object.h"
#include "AnalyticIK.h"
#include "DrawDebugHelpers.h"
#include "NxMappingEnum.h"
#include "GeometryCollection/GeometryCollectionAlgo.h"
#include "NexusSkeleton.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogNxSkeleton, Log, All);
//DEFINE_LOG_CATEGORY(LogNxSkeleton);

class UNexusSkeleton : public UObject
{
	
};

UENUM()
enum EBoneSpace
{
	Local,
	Parent,
	World
};

UENUM()
enum EMocapJoint
{
	Reference,
	Hips,
	
	LeftUpLeg,
	LeftLeg,
	LeftFoot,
	LeftToeBase,

	RightUpLeg,
	RightLeg,
	RightFoot,
	RightToeBase,

	Spine,
	Spine1,
	Spine2,
	Spine3,

	Neck,
	Head,

	LeftShoulder,
	LeftArm,
	LeftForeArm,
	LeftHand,

	LeftHandIndex1,
	LeftHandIndex2,
	LeftHandIndex3,

	LeftHandMiddle1,
	LeftHandMiddle2,
	LeftHandMiddle3,

	LeftHandRing1,
	LeftHandRing2,
	LeftHandRing3,

	LeftHandPinky1,
	LeftHandPinky2,
	LeftHandPinky3,

	LeftHandThumb1,
	LeftHandThumb2,
	LeftHandThumb3,

	RightShoulder,
	RightArm,
	RightForeArm,
	RightHand,    

	RightHandIndex1,
	RightHandIndex2,
	RightHandIndex3,

	RightHandMiddle1,
	RightHandMiddle2,
	RightHandMiddle3,

	RightHandRing1,
	RightHandRing2,
	RightHandRing3,

	RightHandPinky1,
	RightHandPinky2,
	RightHandPinky3,

	RightHandThumb1,
	RightHandThumb2,
	RightHandThumb3,
	NumJoints
};

USTRUCT()
struct FJointIndex
{
	GENERATED_BODY()

	int32 Index;
	
};

USTRUCT()
struct FMocapJointIndex : public FJointIndex
{
	GENERATED_BODY()
	int32 Reference = -1;
	int32 Hips = 0;
};

UCLASS(BlueprintType)
class UNxSkeletonBase : public UObject
{
public:
	GENERATED_BODY()

	void Init(TMap<FName, FTransform> JointTransforms, TMap<FName, FName> JointParents)
	{
		SetLength(JointTransforms.Num());
		//UE_LOG(LogNxSkeleton, Log,  TEXT("Num Xforms: %d"), JointTransforms.Num());
		//UE_LOG(LogNxSkeleton, Log, TEXT("Num Parents: %d"), JointTransforms.Num());
		// TODO: Validate maps
		// JointTransformArray.Init(FTransform(), GetLength());
		// JointParentsArray.Init(-1, GetLength());
		// JointNameArray.Init(FName(), GetLength());
		JointTransformArray.Empty();
		JointParentsArray.Empty();
		JointNameArray.Empty();
		JointTransformArray.Reserve(GetLength());
		JointParentsArray.Reserve(GetLength());
		JointNameArray.Reserve(GetLength());
		
		//UE_LOG(LogNxSkeleton, Log, TEXT("Have initialised arrays"));
		TArray<TPair<FName, FTransform>> RootJoints;
		for(TPair<FName, FTransform> Joint : JointTransforms)
		{
			if(!JointParents.Contains(Joint.Key))
			{
				//UE_LOG(LogNxSkeleton, Log, TEXT("RootJoint!: %s"), *Joint.Value.ToString());
				RootJoints.Add(Joint);
			}
		}
		
		for(TPair<FName, FTransform> RootJoint : RootJoints)
		{
			AddJointHierarchyToArrays(JointTransforms, JointParents, RootJoint.Key);
		}
	}
	void Init(TArray<FTransform> Transforms, TArray<FName> Names, TArray<int32> Parents, EBoneSpace Space)
	{
		SetLength(Transforms.Num());
		if(Names.Num() != GetLength()) return;

		// TODO: Validate maps
		if(Space == Local) JointTransformArray = Transforms;
		else if(Space == World)
		{
			JointTransformArray.Empty();
			JointTransformArray.Reserve(GetLength());
			for(int i = 0; i < GetLength(); i++)
			{
				if(Parents[i]<0 || Parents[i] >= GetLength()) JointTransformArray.Add(Transforms[i]);
				else JointTransformArray.Add(Transforms[i] * Transforms[Parents[i]].Inverse());
			}
		}
		JointNameArray = Names;
		JointParentsArray = Parents;
	}

	void Init(TMap<FName, FTransform> JointTransforms, TMap<FName, FName> JointParents, FName LocomotionRoot)
	{
		Init(JointTransforms, JointParents);
		LocomotionRootIndex = JointNameArray.IndexOfByKey(LocomotionRoot);
	}

	void Init(UNxSkeletonBase* NxSkeleton)
	{
		SetLength(NxSkeleton->Length);
		JointParentsArray = NxSkeleton->JointParentsArray;
		JointTransformArray = NxSkeleton->JointTransformArray;
		JointNameArray = NxSkeleton->JointNameArray;
	}

	void AddJointHierarchyToArrays(TMap<FName, FTransform> Transforms, const TMap<FName, FName> JointParents, const FName ParentJointName, const int32 ParentIndex=-1)
	{
		// Parent index is very high on log
		//UE_LOG(LogNxSkeleton, Log, TEXT("AddJointHierarchyToArrays: %s, %d"), *ParentJointName.ToString(), ParentIndex);
		const int32 CurrentIndex = JointNameArray.Num();
		JointParentsArray.Add(ParentIndex);
		JointNameArray.Add(ParentJointName);
		JointTransformArray.Add(*Transforms.Find(ParentJointName));
		JointNameIndexMap.Add(ParentJointName, CurrentIndex);
		for(TPair<FName, FName> JointParent : JointParents)
		{
			if(JointParent.Value == ParentJointName)
			{
				//UE_LOG(LogNxSkeleton, Log, TEXT("HasChild: %s:%s"), *JointParent.Key.ToString(), *JointParent.Value.ToString());
				AddJointHierarchyToArrays(Transforms, JointParents, JointParent.Key, CurrentIndex);
			}
		}
	}
//Actually should be private with safe accessor methods
public:
	//private:
	
public:
	UPROPERTY(BlueprintReadOnly)
	TArray<FTransform> JointTransformArray;
	UPROPERTY(BlueprintReadOnly)
	TArray<FName> JointNameArray;
	UPROPERTY(BlueprintReadOnly)
	TMap<FName, int32> JointNameIndexMap;
	UPROPERTY(BlueprintReadOnly)
	TArray<int32> JointParentsArray;

	UPROPERTY(BlueprintReadOnly)
	TArray<FVector> RestPositions;
	TArray<FQuat> RestRotations;
	TArray<FVector> RestScale;

	int32 StaticRootIndex = 0;
	int32 LocomotionRootIndex = -1;
	
	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	bool IsValidIndex(int32 Index)
	{
		return JointTransformArray.IsValidIndex(Index);
	}
	
	void SetLocomotionRoot(int32 JointIndex)
	{
		if(IsValidIndex(JointIndex) || JointIndex == -1) LocomotionRootIndex = JointIndex;
	}

	UFUNCTION(BlueprintCallable)
	void SetLocomotionRoot(FName JointName)
	{
		SetLocomotionRoot(GetJointIndex(JointName));
	}
	
	/*	
	FJointChain MakeChainFromNames(const FName ChainName, TArray<FName> JointNames) const
	{
		TArray<int32> JointIndices;
		JointIndices.Reserve(JointNames.Num());
		for(const FName JointName : JointNames)
		{
			JointIndices.Add(GetJointIndex(JointName));
		}
		return FJointChain(ChainName, JointIndices, JointNames);
	}	
	*/
	
	FTransform* operator[](const int32 Index)
	{
		return &JointTransformArray[Index];
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Length = 0;

	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	virtual int32 GetLength() 
	{
		return Length;
	}
	
	UFUNCTION(BlueprintCallable, Category=NexusSkeleton)
	void SetLength(int32 NewLength){
	    Length = NewLength;
	}

	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	TArray<FTransform> GetJointTransformArray(){
	    return JointTransformArray;
	}
	
	UFUNCTION(BlueprintCallable, Category=NexusSkeleton)
	void SetJointTransformArray(TArray<FTransform> NewJointArray){
	    JointTransformArray = NewJointArray;
	}

	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	TArray<int32> GetJointParentsArray(){
	    return JointParentsArray;
	}
	
	UFUNCTION(BlueprintCallable, Category=NexusSkeleton)
	void SetJointParentsArray(TArray<int32> NewParentsArray){
	    JointParentsArray = NewParentsArray;
	}

	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	TMap<FName, FName> GetJointParentsMap()
	{
		TMap<FName, FName> JointParentsMap;
		JointParentsMap.Reserve(GetLength());
		for(int i = 0; i < GetLength(); i++)
		{
			JointParentsMap.Add(GetJointName(i), GetJointName(GetParentIndex(i)));	
		}
		return JointParentsMap;
	}
	
	UFUNCTION(BlueprintPure, Category=NexusSkeleton)
	TArray<FName> GetJointNameArray(){
	    return JointNameArray;
	}
	
	UFUNCTION(BlueprintCallable, Category=NexusSkeleton)
	void SetJointNameArray(TArray<FName> NewNameArray){
	    JointNameArray = NewNameArray;
	}
	
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	int32 GetJointIndex(const FName JointName) const
	{
		return JointNameArray.Find(JointName);
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FName GetJointName(const int32 JointIndex) const
	{
		if(!JointNameArray.IsValidIndex(JointIndex)) return FName();
		return JointNameArray[JointIndex];
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	int32 GetParentIndex(const int32 Index)
	{
		if (!JointParentsArray.IsValidIndex(Index)) return -1;
		return JointParentsArray[Index];
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FName GetParentName(const FName Name)
	{
		return GetJointName(GetParentIndex(GetJointIndex(Name)));
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	bool HasParent(const int32 JointIndex)
	{
		return GetParentIndex(JointIndex) >= 0;
	}
	
	bool HasParent(const FName JointName)
	{
		return HasParent(GetJointIndex(JointName));
	}
	
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	bool HasParentByName(const FName JointName)
	{
		return HasParent(JointName);
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	bool HasChildren(const int32 JointIndex)
	{
		return GetChildIndices(JointIndex).Num() > 0;
	}

	bool HasChildren(const FName JointName)
	{
		return HasChildren(GetJointIndex(JointName));
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	bool HasChildrenByName(const FName JointName)
	{
		return HasChildren(JointName);
	}
	
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	float GetJointLength(const int32 JointIndex)
	{
		const TArray<int32> JointIndices = GetChildIndices(JointIndex);
		if (JointIndices.Num() == 0) return 0;
		return GetJointLocalTransform(JointIndex).GetTranslation().Size();
	}

	float GetJointLength(const FName JointName)
	{
		return GetJointLength(GetJointIndex(JointName));
	}
	
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	TArray<int32> GetChildIndices(const int32 Index) //, const bool Recursive=false)
	{
		TArray<int32> ChildIndices;
		for(int32 i = 0; i < JointParentsArray.Num(); i++)
		{
			if(JointParentsArray[i]==Index) ChildIndices.AddUnique(i);
		}
		return ChildIndices;
	}

	TArray<int32> GetChildIndices(const FName Name)
	{
		return GetChildIndices(GetJointIndex(Name));
	}
	
	TArray<FName> GetChildNames(const int32 Index)
	{
		TArray<FName> ChildNames;
		const TArray<int32> ChildIndices = GetChildIndices(Index);
		for(int32 i = 0; i< ChildIndices.Num(); i++)
		{
			ChildNames.Add(GetJointName(i));
		}
		return ChildNames;
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	TArray<FName> GetChildNames(const FName Name)
	{
		return GetChildNames(GetJointIndex(Name));
	}
	
	
	// Get Joint Transforms
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FTransform GetJointLocalTransform(const int32 JointIndex) const
	{
		if(!JointTransformArray.IsValidIndex(JointIndex)) return FTransform();
		return JointTransformArray[JointIndex];
	} 

	FTransform GetJointLocalTransform(const FName JointName) const
	{
		return GetJointLocalTransform(GetJointIndex(JointName));
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FTransform GetJointWorldTransform(const int32 JointIndex) const
	{
		if(!JointParentsArray.IsValidIndex(JointIndex)) return FTransform();
		FTransform LocalTransform = GetJointLocalTransform(JointIndex);
		int32 ParentIndex = JointParentsArray[JointIndex];
		if(ParentIndex < 0 || ParentIndex==JointIndex) return LocalTransform;
		FTransform ParentWorldTransform = GetJointWorldTransform(JointParentsArray[JointIndex]);
		return LocalTransform * ParentWorldTransform;
	}

	FTransform GetJointWorldTransform(const FName JointName) const
	{
		return GetJointWorldTransform(GetJointIndex(JointName));
	}
	
	// Get Rest Pose Transform
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FTransform GetJointLocalRestTransform(const int32 JointIndex)
	{
		if(!RestPositions.IsValidIndex(JointIndex) || !RestRotations.IsValidIndex(JointIndex)) return FTransform::Identity;
		FTransform RestTransform;
		RestTransform.SetTranslation(RestPositions[JointIndex]);
		RestTransform.SetRotation(RestRotations[JointIndex]);
		return RestTransform;
	}

	FTransform GetJointLocalRestTransform(const FName JointName)
	{
		return GetJointLocalRestTransform(GetJointIndex(JointName));
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	FTransform GetJointWorldRestTransform(const int32 JointIndex)
	{
		const int32 ParentIndex = GetParentIndex(JointIndex);
		FTransform LocalTransform = GetJointLocalTransform(JointIndex);
		if(ParentIndex <= 0) return LocalTransform;
		const FTransform ParentWorldTransform = GetJointWorldTransform(ParentIndex);
		return LocalTransform * ParentWorldTransform;
	}

	FTransform GetJointWorldRestTransform(const FName JointName)
	{
		return GetJointWorldRestTransform(GetJointIndex(JointName));
	}
	
	// Get Joint Transforms Sequentially
	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	TArray<FTransform> GetJointLocalTransforms() const
	{
		TArray<FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(Length);
		for(int32 i=0; i<Length; i++)
		{
			JointLocalTransforms.Add(GetJointLocalTransform(i));
		}
		return JointLocalTransforms; 
	}
	
	TMap<int32, FTransform> GetJointLocalTransforms(TArray<int32> JointIndices) const
	{
		TMap<int32, FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(JointIndices.Num());
		for(int32 JointIndex : JointIndices)
		{
			JointLocalTransforms.Add(JointIndex, GetJointLocalTransform(JointIndex));
		}
		return JointLocalTransforms;
	}

	TMap<FName, FTransform> GetJointLocalTransforms(TArray<FName> JointNames) const
	{
		TMap<FName, FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(JointNames.Num());
		for(FName JointName : JointNames)
		{
			JointLocalTransforms.Add(JointName, GetJointLocalTransform(JointName));
		}
		return JointLocalTransforms;
	}

	UFUNCTION(BlueprintPure, Category = NexusSkeleton)
	TArray<FTransform> GetJointWorldTransforms() const
	{
		TArray<FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(Length);
		for(int32 i=0; i<Length; i++)
		{
			JointLocalTransforms.Add(GetJointWorldTransform(i));
		}
		return JointLocalTransforms; 
	}
	
	TMap<int32, FTransform> GetJointWorldTransforms(TArray<int32> JointIndices) const
	{
		TMap<int32, FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(JointIndices.Num());
		for(int32 JointIndex : JointIndices)
		{
			JointLocalTransforms.Add(JointIndex, GetJointWorldTransform(JointIndex));
		}
		return JointLocalTransforms;
	}

	TMap<FName, FTransform> GetJointWorldTransforms(TArray<FName> JointNames) const
	{
		TMap<FName, FTransform> JointLocalTransforms;
		JointLocalTransforms.Reserve(JointNames.Num());
		for(FName JointName : JointNames)
		{
			JointLocalTransforms.Add(JointName, GetJointWorldTransform(JointName));
		}
		return JointLocalTransforms;
	}
	
	// Set Joint Transforms
	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointLocalTransform(const int32 JointIndex, const FTransform Transform)
	{
		if(!JointTransformArray.IsValidIndex(JointIndex)) return;
		JointTransformArray[JointIndex].SetTranslation(Transform.GetTranslation());
		JointTransformArray[JointIndex].SetRotation(Transform.GetRotation());
		JointTransformArray[JointIndex].SetScale3D(Transform.GetScale3D());
	}

	void SetJointLocalTransform(const FName JointName, const FTransform Transform)
	{
		SetJointLocalTransform(GetJointIndex(JointName), Transform);
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointLocalRotation(const int32 JointIndex, const FQuat Rotation)
	{
		if(!JointTransformArray.IsValidIndex(JointIndex)) return;
		JointTransformArray[JointIndex].SetRotation(Rotation);
	}

	void SetJointLocalRotation(const FName JointName, const FQuat Rotation)
	{
		SetJointLocalRotation(GetJointIndex(JointName), Rotation);
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointWorldRotation(const int32 JointIndex, const FQuat Rotation)
	{
		SetJointLocalRotation(
			JointIndex, 
			GetJointWorldTransform(GetParentIndex(JointIndex)).GetRotation().Inverse() * Rotation);
	}

	void SetJointWorldRotation(const FName JointName, const FQuat Rotation)
	{
		SetJointWorldRotation(GetJointIndex(JointName), Rotation);
	}
    
    FVector GetJointLocalTranslation(const int32 JointIndex){
        return GetJointLocalTransform(JointIndex).GetTranslation();
    }
    
    FTransform GetJointRelativeTransform(const int32 JointIndex, const int32 ParentIndex)
    {
        return GetJointWorldTransform(JointIndex) * GetJointWorldTransform(ParentIndex).Inverse();
    }
    
	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointLocalTranslation(const int32 JointIndex, const FVector Translation)
	{
		if(!JointTransformArray.IsValidIndex(JointIndex)) return;
		JointTransformArray[JointIndex].SetTranslation(Translation);
	}

	void SetJointLocalTranslation(const FName JointName, const FVector Translation)
	{
		SetJointLocalTranslation(GetJointIndex(JointName), Translation);
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointWorldTranslation(const int32 JointIndex, const FVector Translation)
	{
		// Careful with this, don't use to set multiple joints, use SetJointWorldTranslations which will sort joints first
		if(!JointTransformArray.IsValidIndex(JointIndex)) return;
		JointTransformArray[JointIndex].SetTranslation(GetJointWorldTransform(GetParentIndex(JointIndex)).InverseTransformPosition(Translation));
	}

	void SetJointWorldTranslation(const FName JointName, const FVector Translation)
	{
		SetJointWorldTranslation(GetJointIndex(JointName), Translation);
	}
	
	// Set multiple joints sequentially
	void SetJointLocalRotations(TMap<int32, FQuat> JointRotations)
	{
		JointRotations.KeySort([](int32 A, int32 B)
			{
				return A < B;
			});
		
		for(TPair<int32, FQuat> JointRotation : JointRotations) 
		{
			SetJointLocalRotation(JointRotation.Key, JointRotation.Value);
		}
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointLocalRotations(TMap<FName, FQuat> JointRotations)
	{
		TMap<int32, FQuat> JointRotationsByIndex;

		for (TPair<FName, FQuat> JointRotation : JointRotations){
			JointRotationsByIndex.Add(GetJointIndex(JointRotation.Key), JointRotation.Value);
		}
		SetJointLocalRotations(JointRotationsByIndex);
	}

	void SetJointWorldRotations(TMap<int32, FQuat> JointRotations)
	{
		// Sort by index meaning we'll do the joints higher in the hierarchy first
		JointRotations.KeySort([](int32 A, int32 B)
			{
				return A < B;
			});
		
		for(TPair<int32, FQuat> JointRotation : JointRotations) 
		{
			SetJointWorldRotation(JointRotation.Key, JointRotation.Value);
		}
	}
	
	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointWorldRotations(TMap<FName, FQuat> JointRotations)
	{
		TMap<int32, FQuat> JointRotationsByIndex;

		for (TPair<FName, FQuat> JointRotation : JointRotations){
			JointRotationsByIndex.Add(GetJointIndex(JointRotation.Key), JointRotation.Value);
		}
		SetJointWorldRotations(JointRotationsByIndex);
	}

	void SetJointLocalTranslations(TMap<int32, FVector> JointTranslations)
	{
		JointTranslations.KeySort([](int32 A, int32 B)
			{
				return A < B;
			});
		
		for(TPair<int32, FVector> JointTranslation : JointTranslations) 
		{
			SetJointLocalTranslation(JointTranslation.Key, JointTranslation.Value);
		}
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointLocalTranslations(TMap<FName, FVector> JointTranslations)
	{
		TMap<int32, FVector> JointTranslationsByIndex;

		for (TPair<FName, FVector> JointTranslation : JointTranslations){
			JointTranslationsByIndex.Add(GetJointIndex(JointTranslation.Key), JointTranslation.Value);
		}
		SetJointLocalTranslations(JointTranslationsByIndex);
	}

	void SetJointWorldTransform(int32 JointIndex, FTransform Transform)
	{
		SetJointLocalTransform(JointIndex, Transform * GetJointWorldTransform(GetParentIndex(JointIndex)).Inverse());
	}

	void SetJointWorldTransform(FName JointName, FTransform Transform)
	{
		SetJointWorldTransform(GetJointIndex(JointName), Transform);
	}
	
	void SetJointWorldTranslations(TMap<int32, FVector> JointTranslations)
	{
		// Sort by index meaning we'll do the joints higher in the hierarchy first
		JointTranslations.KeySort([](int32 A, int32 B)
			{
				return A < B;
			});
		
		for(TPair<int32, FVector> JointTranslation : JointTranslations) 
		{
			SetJointWorldTranslation(JointTranslation.Key, JointTranslation.Value);
		}
	}

	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void SetJointWorldTranslations(TMap<FName, FVector> JointTranslations)
	{
		TMap<int32, FVector> JointTranslationsByIndex;

		for (TPair<FName, FVector> JointTranslation : JointTranslations){
			JointTranslationsByIndex.Add(GetJointIndex(JointTranslation.Key), JointTranslation.Value);
		}
		SetJointWorldTranslations(JointTranslationsByIndex);
	}
	
	UFUNCTION(BlueprintCallable, Category = NexusSkeleton)
	void StoreRestPose()
	{
		RestPositions = TArray<FVector>();
		RestPositions.Reserve(Length);
		RestRotations = TArray<FQuat>();
		RestRotations.Reserve(Length);
		RestScale = TArray<FVector>();
		RestScale.Reserve(Length);
		
		for (int i = 0; i < Length; i++)
		{
			const FTransform* CurrentTransform = this->operator[](i);
			
			RestPositions[i] = CurrentTransform->GetTranslation();
			RestRotations[i] = CurrentTransform->GetRotation();
			RestScale[i] = CurrentTransform->GetScale3D();
		}
	}

	void DrawDebug(UWorld* World, FColor Color, float Thickness, float LifeTime, bool DrawAxes, float AxesLength);
};

UCLASS()
class UNxSkeletonXsens : public UNxSkeletonBase
{
public:
	GENERATED_BODY()
	using UNxSkeletonBase::Init;

	UFUNCTION(BlueprintCallable)
	void Init(TArray<FTransform> JointTransforms, EBoneSpace BoneSpace=Local);
/*
	int32 GetLength(){
	    return 68;
	}
	*/
	
	void InitXsens(){
	    SetLength(68);
	    
	    SetJointParentsArray(TArray<int32>
            {
                -1, 0, 1, 2, 3, 4, 5, 6, 5, 8, 9, 10, 5, 12, 13, 14, 1, 16, 17, 18, 1, 20, 21, 22, 0, 0, 0, 0,
                15, 28, 29, 30, 28, 32, 33, 34, 28, 36, 37, 38, 28, 40, 41, 42, 28, 44, 45, 46,		//left hand
                11, 48, 49, 50, 48, 52, 53, 54, 48, 56, 57, 58, 48, 60, 61, 62, 48, 64, 65, 66  	//right hand
            });
        
        SetJointNameArray(
            TArray<FName>
            {
                "Root",
                "Pelvis",
                "L5",
                "L3",
                "T12",
                "T8",
                "Neck",
                "Head",
                "RightShoulder",
                "RightUpperArm",
                "RightForeArm",
                "RightHand",
                "LeftShoulder",
                "LeftUpperArm",
                "LeftForeArm",
                "LeftHand",
                "RightUpperLeg",
                "RightLowerLeg",
                "RightFoot",
                "RightToe",
                "LeftUpperLeg",
                "LeftLowerLeg",
                "LeftFoot",
                "LeftToe",
                "Prop1",
                "Prop2",
                "Prop3",
                "Prop4",
                "LeftCarpus",
                "LeftFirstMC",
                "LeftFirstPP",
                "LeftFirstDP",
                "LeftSecondMC",
                "LeftSecondPP",
                "LeftSecondMP",
                "LeftSecondDP",
                "LeftThirdMC",
                "LeftThirdPP",
                "LeftThirdMP",
                "LeftThirdDP",
                "LeftFourthMC",
                "LeftFourthPP",
                "LeftFourthMP",
                "LeftFourthDP",
                "LeftFifthMC",
                "LeftFifthPP",
                "LeftFifthMP",
                "LeftFifthDP",
                "RightCarpus",
                "RightFirstMC",
                "RightFirstPP",
                "RightFirstDP",
                "RightSecondMC",
                "RightSecondPP",
                "RightSecondMP",
                "RightSecondDP",
                "RightThirdMC",
                "RightThirdPP",
                "RightThirdMP",
                "RightThirdDP",
                "RightFourthMC",
                "RightFourthPP",
                "RightFourthMP",
                "RightFourthDP",
                "RightFifthMC",
                "RightFifthPP",
                "RightFifthMP",
                "RightFifthDP"
            });

	}
};

UCLASS()
class UNxSkeletonMocap : public UNxSkeletonBase
{
public:
	GENERATED_BODY()
	/*
		UNxSkeletonMocap() : UNxSkeletonBase(){
			Length = EMocapJoint::NumJoints;
			JointNameArray = MocapJointNames;
			JointTransformArray.Init(FTransform(), Length);
			JointNameIndexMap.Reserve(Length);
			for(int i = 0; i < Length; i++)
			{
				//auto MocapJointEnum = static_cast<EMocapJoint>(i);
				JointNameIndexMap.Add(JointNameArray[i], i);
			}
		}
	*/
	using UNxSkeletonBase::Init;
	
	void Init(TArray<FTransform> JointTransforms);
	
	UFUNCTION(BlueprintCallable, Category=NexusSkeleton)
	void Init(TMap<FName, FTransform> JointTransforms);
	
	TArray<int32> JointParentsArray = TArray<int32>
	{
		-1,
		EMocapJoint::Reference,
		EMocapJoint::Hips,
		EMocapJoint::LeftUpLeg,
		EMocapJoint::LeftLeg,
		EMocapJoint::LeftFoot,
		EMocapJoint::Hips,
		EMocapJoint::RightUpLeg,
		EMocapJoint::RightLeg,
		EMocapJoint::RightFoot,
		EMocapJoint::Hips,
		EMocapJoint::Spine,
		EMocapJoint::Spine1,
		EMocapJoint::Spine2,
		EMocapJoint::Spine3,
		EMocapJoint::Neck,
		EMocapJoint::Spine3,
		EMocapJoint::LeftShoulder,
		EMocapJoint::LeftArm,
		EMocapJoint::LeftForeArm,
		EMocapJoint::LeftHand,
		EMocapJoint::LeftHandIndex1,
		EMocapJoint::LeftHandIndex2,
		EMocapJoint::LeftHand,
		EMocapJoint::LeftHandMiddle1,
		EMocapJoint::LeftHandMiddle2,
		EMocapJoint::LeftHand,
		EMocapJoint::LeftHandRing1,
		EMocapJoint::LeftHandRing2,
		EMocapJoint::LeftHand,
		EMocapJoint::LeftHandPinky1,
		EMocapJoint::LeftHandPinky2,
		EMocapJoint::LeftHand,
		EMocapJoint::LeftHandThumb1,
		EMocapJoint::LeftHandThumb2,
		EMocapJoint::Spine3,
		EMocapJoint::RightShoulder,
		EMocapJoint::RightArm,
		EMocapJoint::RightForeArm,
		EMocapJoint::RightHand,
		EMocapJoint::RightHandIndex1,
		EMocapJoint::RightHandIndex2,
		EMocapJoint::RightHand,
		EMocapJoint::RightHandMiddle1,
		EMocapJoint::RightHandMiddle2,
		EMocapJoint::RightHand,
		EMocapJoint::RightHandRing1,
		EMocapJoint::RightHandRing2,
		EMocapJoint::RightHand,
		EMocapJoint::RightHandPinky1,
		EMocapJoint::RightHandPinky2,
		EMocapJoint::RightHand,
		EMocapJoint::RightHandThumb1,
		EMocapJoint::RightHandThumb2,
	};
	
	TArray<FName> MocapJointNames = {
		"Reference",
		"Hips",
		"LeftUpLeg",
		"LeftLeg",
		"LeftFoot",
		"LeftToeBase",
		"RightUpLeg",
		"RightLeg",
		"RightFoot",
		"RightToeBase",
		"Spine",
		"Spine1",
		"Spine2",
		"Spine3",
		"Neck",
		"Head",
		"LeftShoulder",
		"LeftArm",
		"LeftForeArm",
		"LeftHand",
		"LeftHandIndex1",
		"LeftHandIndex2",
		"LeftHandIndex3",
		"LeftHandMiddle1",
		"LeftHandMiddle2",
		"LeftHandMiddle3",
		"LeftHandRing1",
		"LeftHandRing2",
		"LeftHandRing3",
		"LeftHandPinky1",
		"LeftHandPinky2",
		"LeftHandPinky3",
		"LeftHandThumb1",
		"LeftHandThumb2",
		"LeftHandThumb3",
		"RightShoulder",
		"RightArm",
		"RightForeArm",
		"RightHand",    
		"RightHandIndex1",
		"RightHandIndex2",
		"RightHandIndex3",
		"RightHandMiddle1",
		"RightHandMiddle2",
		"RightHandMiddle3",
		"RightHandRing1",
		"RightHandRing2",
		"RightHandRing3",
		"RightHandPinky1",
		"RightHandPinky2",
		"RightHandPinky3",
		"RightHandThumb1",
		"RightHandThumb2",
		"RightHandThumb3"
	};
	int32 StaticRootIndex = EMocapJoint::Reference;
	int32 LocomotionRootIndex = EMocapJoint::Hips;
/*
	using UNxSkeletonBase::GetJointWorldTransform;
	
	FTransform GetJointWorldTransform(ENxXsensMapping JointIndex)
	{
		return GetJointWorldTransform(static_cast<int32>(JointIndex));
	}
	*/
};
