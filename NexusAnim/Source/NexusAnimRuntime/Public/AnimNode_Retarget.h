// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Animation/AnimNodeBase.h"
#include "RetargetNexusSkeleton.h"

#include "AnimNode_Retarget.generated.h"
/**
 *
 */
DECLARE_LOG_CATEGORY_EXTERN(LogRetarget, Log, All)

USTRUCT(BlueprintType)
struct NEXUSANIMRUNTIME_API FTransformArray 
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BonesTransfroms")
		TArray <FTransform> Transforms;

};
USTRUCT(BlueprintType)
struct NEXUSANIMRUNTIME_API FAnimNode_Retarget : public FAnimNode_Base
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FPoseLink BasePose;

public:
	FAnimNode_Retarget();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext& Context) override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate(FPoseContext& Output) override;
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;
	// End of FAnimNode_Base interface

	UPROPERTY(BlueprintReadWrite, transient, Category = Copy, meta = (PinShownByDefault))
	TWeakObjectPtr<USkeletalMeshComponent> SourceMeshComponent;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	// float Alpha;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget)
	TMap<FName, FName> BoneMappings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	TMap<FName, FTransform> LiveLinkTransforms;

private:
	TArray<FName> XsensBoneNames = { "Root", "Pelvis", "L5", "L3", "T12", "T8", "Neck", "Head", "RightShoulder", "RightUpperArm", "RightForeArm", "RightHand", "LeftShoulder", "LeftUpperArm", "LeftForeArm", "LeftHand", "RightUpperLeg", "RightLowerLeg", "RightFoot", "RightToe", "LeftUpperLeg", "LeftLowerLeg", "LeftFoot", "LeftToe", "Prop1", "Prop2", "Prop3", "Prop4", "LeftCarpus", "LeftFirstMC", "LeftFirstPP", "LeftFirstDP", "LeftSecondMC", "LeftSecondPP", "LeftSecondMP", "LeftSecondDP", "LeftThirdMC", "LeftThirdPP", "LeftThirdMP", "LeftThirdDP", "LeftFourthMC", "LeftFourthPP", "LeftFourthMP", "LeftFourthDP", "LeftFifthMC", "LeftFifthPP", "LeftFifthMP", "LeftFifthDP", "RightCarpus", "RightFirstMC", "RightFirstPP", "RightFirstDP", "RightSecondMC", "RightSecondPP", "RightSecondMP", "RightSecondDP", "RightThirdMC", "RightThirdPP", "RightThirdMP", "RightThirdDP", "RightFourthMC", "RightFourthPP", "RightFourthMP", "RightFourthDP", "RightFifthMC", "RightFifthPP", "RightFifthMP", "RightFifthDP" };
	TWeakObjectPtr<USkeletalMesh> SkeletalMesh;
	TArray<FTransform> LiveLinkTransformArray;
	//TWeakObjectPtr<USkeletalMeshComponent>	CurrentlyUsedSourceMeshComponent;
	TWeakObjectPtr<USkeletalMesh> SourceMesh;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	FTransformArray TransformArray1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	TArray<FTransform> TransformArray2;*/
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	FSubjectFrameHandle LiveLinkAnimationFrameData;*/
};
