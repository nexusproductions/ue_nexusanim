// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

//#include "NexusSkeleton.h"
#include "NexusAnimBPLib.generated.h"

/**
 * 
 */


UCLASS()
class UNexusAnimBPLib : public UBlueprintFunctionLibrary
{
public:
	GENERATED_UCLASS_BODY()

	UFUNCTION(BlueprintPure, Category = NexusAnim)
	static float ScaleFloat(float A, float B);
	
	UFUNCTION(BlueprintPure, Category = NexusAnim)
	static void GetBoneTransformAtTimeFromAnimSequence(UAnimSequence* MyAnimSequence,
		float AnimTime, int BoneIdx, bool bUseRawDataOnly, FTransform& OutTransform);
};
