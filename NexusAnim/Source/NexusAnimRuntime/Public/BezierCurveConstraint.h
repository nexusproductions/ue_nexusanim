﻿#pragma once

#include "QuaternionMath.h"
#include "DrawDebugHelpers.h"
/*
namespace Mocap
{
    // This intended to put the twist bones on a curve from the start to the end of the chain
    class BezierCurveConstraint
    {
    public:
        FTransform PreviousTransform;
        FTransform CurveStart;
        FTransform CurveEnd;
        FTransform NextTransform;
        
        TArray<FTransform> BonesToPosition;
    private:
        TArray<float> _tValues;
        float _lengthOfChain = 0;
    public:
        float Bendiness = 0.25f;

        QuaternionMath::AimDirection AimDirection;
        QuaternionMath::AimDirection UpDirection;
    private:
        FVector A;
        FVector B;
        FVector C;
        FVector D;
    
        TArray<FVector> _restPositions;
        TArray<FQuat> _restRotations;
    public:
        void Start()
        {
            _tValues.Init(0, BonesToPosition.Num());
            _lengthOfChain = FVector::Distance(CurveStart.GetTranslation(), CurveEnd.GetTranslation());
            for (int i = 0; i < BonesToPosition.Num(); i++)
            {
                float distanceFromStart = FVector::Distance(BonesToPosition[i].GetTranslation(), CurveStart.GetTranslation());
                _tValues[i] = distanceFromStart / _lengthOfChain;
            }
            _restRotations.Init(FQuat(), BonesToPosition.Num());
            _restPositions.Init(FVector(), BonesToPosition.Num());
            for (int i = 0; i < BonesToPosition.Num(); i++)
            {
                _restRotations[i] = BonesToPosition[i].localRotation;
                _restPositions[i] = BonesToPosition[i].localPosition;
            }
        }
        
        // We need to reset the bones because in our retargetting we are setting the final world rotation, and this would be affected by the parents if the parents are the twist bones
        void ResetBones()
        {
            for (int i = 0; i < BonesToPosition.Num(); i++)
            {
                BonesToPosition[i].localRotation = _restRotations[i];
                BonesToPosition[i].localPosition = _restPositions[i];
            }
        }

        void UpdateCurveConstraint()
        {
            // This is working with World Transforms so we need to do...something
            FVector previousTransformPosition = PreviousTransform.position;
            FVector curveStartPosition = CurveStart.position;
            FVector curveEndPosition = CurveEnd.position;
            FVector nextTransformPosition = NextTransform.position;

            // Store the curve end rotation, as we will be rotating each joint
            FQuat curveEndRotation = CurveEnd.rotation;
            
            // We need to construct two intermediate points to help define the curve, A and D are the end points
            //    .B___C.
            //   /       \
            //  A         D
            // /           \
            //Prev         Next
            FVector AtoD = curveEndPosition - curveStartPosition;
            
            FVector AtoDNormalized = AtoD.GetSafeNormal();
            // We use the previous transform and next transform direction to make our guess at where these "bezier handles" should be
            FVector prevToStartDirection = (curveStartPosition - previousTransformPosition).GetSafeNormal();
            FVector nextToEndDirection = (curveEndPosition - nextTransformPosition).GetSafeNormal();
            // We need to use just a portion of the length
            float oneQuarterLength = _lengthOfChain / 4;
            
            // A is the start position
            A = curveStartPosition;
            
            // Now we B is a position that is pointing from the start towards the end
            // or away from the previous joint chain direction,
            // modified by the bendiness
            // Start position + A lerp between the straight line and our bezier handle guess * one quarter of the distance
            B = curveStartPosition +
                        FMath::Lerp(AtoDNormalized, prevToStartDirection, Bendiness) *
                        oneQuarterLength;
            
            // Similarly C is a position that is from the curve end, pointing away from the next joint chain direction
            C = curveEndPosition +
                        FMath::Lerp(AtoDNormalized * -1, nextToEndDirection, Bendiness) * 
                        oneQuarterLength;
            
            // D is the end position
            D = curveEndPosition;
            
            // Store all the updirections (to maintain twist)
            TArray<FVector> upDirections
            upDirections.Init(FVector(), BonesToPosition.Num());
            for (int i = 0; i < BonesToPosition.Num(); i++)
            {
                switch (UpDirection)
                {
                    case QuaternionMath::AimDirection::AIM_X:
                        upDirections[i] = BonesToPosition[i].right;
                        break;
                    case QuaternionMath::AimDirection::AIM_Y:
                        upDirections[i] = BonesToPosition[i].up;
                        break;
                    case QuaternionMath::AimDirection::AIM_Z:
                        upDirections[i] = BonesToPosition[i].forward;
                        break;
                    case QuaternionMath::AimDirection::AIM_NEGX:
                        upDirections[i] = BonesToPosition[i].right * -1;
                        break;
                    case QuaternionMath::AimDirection::AIM_NEGY:
                        upDirections[i] = BonesToPosition[i].up * -1;
                        break;
                    case QuaternionMath::AimDirection::AIM_NEGZ:
                        upDirections[i] = BonesToPosition[i].forward * -1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // Now all the bones should sit somewhere on this curve
            for (int i = 0; i < BonesToPosition.Num(); i++)
            {
                FVector tangent;
                FVector position = Interpolate(A, B, C, D, _tValues[i], tangent);
                if (tangent.SizeSquared() > 0)
                {
                    // Set the direction
                    BonesToPosition[i].rotation = QuaternionMath::LookRotation(tangent, upDirections[i], AimDirection);
                }
                // Set the position
                BonesToPosition[i].position = position;
            }
            // Make sure the final orientation is preserved
            CurveEnd.rotation = curveEndRotation;
        }

        // DeCasteljau Algorithm
        static FVector Interpolate (FVector A, FVector B, FVector C, FVector D, float t, FVector& tangent) 
        {
            //To make it faster
            float oneMinusT = 1 - t;
        
            //Layer 1
            FVector Q = oneMinusT * A + t * B;
            FVector R = oneMinusT * B + t * C;
            FVector S = oneMinusT * C + t * D;

            //Layer 2
            FVector P = oneMinusT * Q + t * R;
            FVector T = oneMinusT * R + t * S;

            //Final interpolated position
            FVector U = oneMinusT * P + t * T;

            tangent = T-U;
            return U;
        }
        
        // Display the curve in editor
        void DebugDraw(UWorld* World)
        {
            //The start position of the line
            FVector lastPos = A;

            //The resolution of the line
            //Make sure the resolution is adding up to 1, so 0.3 will give a gap at the end, but 0.2 will work
            float resolution = 0.2f;

            //How many loops?
            int loops = FMath::FloorToInt(1.0 / resolution);

            FVector tangent;
            for (int i = 1; i <= loops; i++)
            {
                //Which t position are we at?
                float t = i * resolution;

                //Find the coordinates between the control points with a Catmull-Rom spline
                FVector newPos = Interpolate(A,B,C,D,t,tangent);

                //Draw this line segment
                DrawDebugLine(World, lastPos, newPos, FColor::White, false, -1, 0, 0.1);
                //Save this pos so we can draw the next line segment
                lastPos = newPos;
            }
		
            //Also draw lines between the control points and endpoints
            
            DrawDebugLine(World, A, B, FColor::Green, false, -1, 0, 0.1);
            DrawDebugLine(World, C, D, FColor::Green, false, -1, 0, 0.1);
            
        }
    }
}
*/