﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Animation/AnimNodeBase.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "AnimNode_SetBoneRotationsByName.generated.h"

USTRUCT(BlueprintType)
struct NEXUSANIMRUNTIME_API FAnimNode_SetBoneRotationsByName : public FAnimNode_Base //SkeletalControlBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink BasePose;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	TMap<FName, FQuat> BoneRotations;

public:
	FAnimNode_SetBoneRotationsByName();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext& Context) override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void EvaluateComponentSpace_AnyThread(FComponentSpacePoseContext& Output) override;
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;
	
	
	// End of FAnimNode_Base interface
};


