﻿#pragma once
 	

#include "Math/Axis.h"

/*
UENUM(BlueprintType)
enum EAxis : uint8
{
    X,
    Y,
    Z
};
*/
UENUM(BlueprintType)
enum class EAimDirection : uint8
{
    AIM_X,
    AIM_Y,
    AIM_Z,
    AIM_NEGX,
    AIM_NEGY,
    AIM_NEGZ,
};

class QuaternionMath
{
public:
    
    // https://www.chadvernon.com/blog/rbf-quaternion-input/
    // Quaternion Dot
    static double QuaternionDot(FQuat q1, FQuat q2) 
    {
        
        double dotValue = (q1.X * q2.X) + (q1.Y * q2.Y) + (q1.Z * q2.Z) + (q1.W * q2.W);
        // Clamp any floating point error since this will go in to acos
        if (dotValue < -1.0) {
            dotValue = -1.0;
        } else if (dotValue > 1.0) {
            dotValue = 1.0;
        }
        return dotValue;
    }

    // Quaternion Distance
    static double QuaternionDistance(FQuat q1, FQuat q2) 
    { 
        double dot = QuaternionDot(q1, q2);
        
        return FMath::Acos(2.0 * dot * dot - 1.0) / PI;
    }

    // Swing / Twist decomposition. https://www.chadvernon.com/blog/swing-twist/
    // This is useful for taking the quaternion and just getting the twist component, in cases where you want to drive something by the twist (like twist bones)
    // if you need just a portion of the twist, you can just slerp from identity like so: Quaternion.Slerp(Quaternion.identity,twist,weight)
    static FQuat Twist(FQuat rotation, EAxis::Type twistAxis)
    {
        FQuat twist = rotation;
        switch (twistAxis) {
        case EAxis::Type::X:  // X axis
            twist.Y = 0.0f;
            twist.Z = 0.0f;
            break;
            case EAxis::Type::Y:  // Y axis
            twist.X = 0.0f;
            twist.Z = 0.0f;
            break;
            case EAxis::Type::Z:  // Z axis
            twist.X = 0.0f;
            twist.Y = 0.0f;
            break;
        }

        twist.Normalize();
        return twist;
    }

    // Swing / Twist decomposition. https://www.chadvernon.com/blog/swing-twist/
    // This is useful for taking the quaternion and just getting the swing component, in cases where you want to drive something by the swing (ignoring the twist)
    static FQuat Swing(FQuat rotation, FQuat twist)
    {
        return twist.Inverse() * rotation;
    }

    static FQuat xFlip;// = FQuat::MakeFromEuler(FVector(0, -90, 0));
    static FQuat yFlip;// = FQuat::MakeFromEuler(FVector(90, 0, 0));

    // TODO: Implement with given UpDirection so it can be used across the board.
    static FQuat LookRotation(FVector forward, FVector up, EAimDirection type)
    {
        switch (type)
        {
            
            case EAimDirection::AIM_X: // X is forward, Y is still up?
                return FRotationMatrix::MakeFromXZ(forward, up).ToQuat() * xFlip;
            case EAimDirection::AIM_Y: // Y is forward, -Z is up
                return FRotationMatrix::MakeFromXZ(forward, -up).ToQuat() * yFlip; 
            case EAimDirection::AIM_Z:
                return FRotationMatrix::MakeFromXZ(forward, up).ToQuat();
            case EAimDirection::AIM_NEGX: // -X is forward, Y is still up
                return FRotationMatrix::MakeFromXZ(-forward, up).ToQuat() * xFlip;
            case EAimDirection::AIM_NEGY: // -Y is forward, -Z is up
                return FRotationMatrix::MakeFromXZ(-forward, -up).ToQuat() * yFlip;
            case EAimDirection::AIM_NEGZ: 
                return FRotationMatrix::MakeFromXZ(-forward, up).ToQuat();
            default:
                return FRotationMatrix::MakeFromXZ(forward, up).ToQuat();
        }
    }
};
