﻿#pragma once
#include "NxMappingEnum.h"

#include "Engine/DataTable.h"
#include "ARKitRow.generated.h"

USTRUCT(BlueprintType)
struct FARKitRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BlendShapeCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeBlinkLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookDownLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookInLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookOutLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookUpLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeSquintLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeWideLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeBlinkRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookDownRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookInRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookOutRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeLookUpRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeSquintRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EyeWideRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JawForward;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JawRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JawLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JawOpen;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthClose;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthFunnel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthPucker;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthSmileLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthSmileRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthFrownLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthFrownRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthDimpleLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthDimpleRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthStretchLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthStretchRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthRollLower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthRollUpper;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthShrugLower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthShrugUpper;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthPressLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthPressRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthLowerDownLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthLowerDownRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthUpperUpLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouthUpperUpRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrowDownLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrowDownRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrowInnerUp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrowOuterUpLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrowOuterUpRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CheekPuff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CheekSquintLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CheekSquintRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NoseSneerLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NoseSneerRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TongueOut;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeadYaw;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeadPitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeadRoll;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftEyeYaw;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftEyePitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftEyeRoll;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightEyeYaw;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightEyePitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightEyeRoll;
	
	TMap<FName, float> ToMap() const
	{
		return TMap<FName, float>({
			{"BlendShapeCount" , BlendShapeCount},
			{"EyeBlinkLeft" , EyeBlinkLeft},
			{"EyeLookDownLeft" , EyeLookDownLeft},
			{"EyeLookInLeft" , EyeLookInLeft},
			{"EyeLookOutLeft" , EyeLookOutLeft},
			{"EyeLookUpLeft" , EyeLookUpLeft},
			{"EyeSquintLeft" , EyeSquintLeft},
			{"EyeWideLeft" , EyeWideLeft},
			{"EyeBlinkRight" , EyeBlinkRight},
			{"EyeLookDownRight" , EyeLookDownRight},
			{"EyeLookInRight" , EyeLookInRight},
			{"EyeLookOutRight" , EyeLookOutRight},
			{"EyeLookUpRight" , EyeLookUpRight},
			{"EyeSquintRight" , EyeSquintRight},
			{"EyeWideRight" , EyeWideRight},
			{"JawForward" , JawForward},
			{"JawRight" , JawRight},
			{"JawLeft" , JawLeft},
			{"JawOpen" , JawOpen},
			{"MouthClose" , MouthClose},
			{"MouthFunnel" , MouthFunnel},
			{"MouthPucker" , MouthPucker},
			{"MouthRight" , MouthRight},
			{"MouthLeft" , MouthLeft},
			{"MouthSmileLeft" , MouthSmileLeft},
			{"MouthSmileRight" , MouthSmileRight},
			{"MouthFrownLeft" , MouthFrownLeft},
			{"MouthFrownRight" , MouthFrownRight},
			{"MouthDimpleLeft" , MouthDimpleLeft},
			{"MouthDimpleRight" , MouthDimpleRight},
			{"MouthStretchLeft" , MouthStretchLeft},
			{"MouthStretchRight" , MouthStretchRight},
			{"MouthRollLower" , MouthRollLower},
			{"MouthRollUpper" , MouthRollUpper},
			{"MouthShrugLower" , MouthShrugLower},
			{"MouthShrugUpper" , MouthShrugUpper},
			{"MouthPressLeft" , MouthPressLeft},
			{"MouthPressRight" , MouthPressRight},
			{"MouthLowerDownLeft" , MouthLowerDownLeft},
			{"MouthLowerDownRight" , MouthLowerDownRight},
			{"MouthUpperUpLeft" , MouthUpperUpLeft},
			{"MouthUpperUpRight" , MouthUpperUpRight},
			{"BrowDownLeft" , BrowDownLeft},
			{"BrowDownRight" , BrowDownRight},
			{"BrowInnerUp" , BrowInnerUp},
			{"BrowOuterUpLeft" , BrowOuterUpLeft},
			{"BrowOuterUpRight" , BrowOuterUpRight},
			{"CheekPuff" , CheekPuff},
			{"CheekSquintLeft" , CheekSquintLeft},
			{"CheekSquintRight" , CheekSquintRight},
			{"NoseSneerLeft" , NoseSneerLeft},
			{"NoseSneerRight" , NoseSneerRight},
			{"TongueOut" , TongueOut},
			{"HeadYaw" , HeadYaw},
			{"HeadPitch" , HeadPitch},
			{"HeadRoll" , HeadRoll},
			{"LeftEyeYaw" , LeftEyeYaw},
			{"LeftEyePitch" , LeftEyePitch},
			{"LeftEyeRoll" , LeftEyeRoll},
			{"RightEyeYaw" , RightEyeYaw},
			{"RightEyePitch" , RightEyePitch},
			{"RightEyeRoll" , RightEyeRoll}}
		);
	}
};