﻿#pragma once
#include "Kalman.h"
#include "AnalyticIK.h"
#include "DrawDebugHelpers.h"

DECLARE_LOG_CATEGORY_EXTERN(LogNxRetarget, Log, All);

DEFINE_LOG_CATEGORY(LogNxRetarget)
class Retarget
{
public:
	static float SoftFalloff(const float InValue, const float ReferenceValue, const float Falloff=0.01) 
	{
		return FMath::Clamp(1.0f - FMath::Abs(InValue - ReferenceValue) * Falloff, 0.0f, 1.0f);
	}
	
	static bool RetargetHand(
		FTransform SourceChest, FTransform SourceShoulder, FTransform SourceArm, FTransform SourceForeArm, FTransform SourceHand,
		FTransform TargetChest, FTransform TargetShoulder, FTransform TargetArm, FTransform TargetForeArm, FTransform TargetHand,
		FQuat& OutArmRotationWS, FQuat& OutForeArmRotationLS, FQuat& OutHandRotationWS,
		FQuat ShoulderRotation=FQuat::Identity, FQuat ChestRotation=FQuat::Identity,
		UKalmanVector* Kalman=nullptr, float ScaleRatio=1.0f, FVector RotationAxis=FVector::UpVector, bool FlipSolveDirection=false, float IKSoftness=0.01f,
		UWorld* World=nullptr, FColor DebugColor=FColor::Magenta)
	{
		// Pole Vector
		FVector PoleVector = CalculatePoleVectorLocation(SourceArm, SourceForeArm, 0.1, FVector::UpVector);
		/*
		//Debug PoleVector on Source
		DrawDebugLine(World, SourceShoulder.GetTranslation(), PoleVector,
				DebugColor, false,-1, 0, 0.1);
		*/
		PoleVector = SpaceTransferLocation(PoleVector,SourceShoulder,TargetShoulder,
			ScaleRatio, ShoulderRotation.Rotator());

		/*
		// Debug PoleVector on Target
		DrawDebugLine(World, TargetShoulder.GetTranslation(), PoleVector,
						DebugColor, false,-1, 0, 0.1);
		*/
		// Space Transfer Hands to set IK Target
		/*
		// Debug Hand position on source
		DrawDebugLine(World, SourceChest.GetTranslation(), SourceHand.GetTranslation(),
			DebugColor, false,-1, 0, 0.5);
		*/
		FVector IKTarget = SpaceTransferLocation(SourceHand.GetTranslation(),
			SourceChest, TargetChest,
			ScaleRatio, ChestRotation.Rotator());
		
		// Debug transferred Hand position on target
		DrawDebugLine(World, TargetChest.GetTranslation(), IKTarget,
			DebugColor, false,-1, 0, 0.5);
		
		// Kalman
		FVector BeforeKalman = IKTarget;
		if(IsValid(Kalman)) IKTarget = Kalman->Update(IKTarget);
		/*
		// Debug Kalman filtering
		DrawDebugLine(World, BeforeKalman, IKTarget,
			FColor::Magenta, false,-1, 0, 1);
		*/
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget - TargetArm.GetTranslation(),
			PoleVector - TargetArm.GetTranslation()).ToQuat();
		/*
		DrawDebugLine(World, TargetArm.GetTranslation(), IKTarget,
			FColor::Silver, false, -1,0, 0.4);
		*/
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetArm.GetTranslation());
		/*
		DrawDebugLine(World, TargetArm.GetTranslation(), TargetArm.GetTranslation()+RotationOfIKChain*RotatedIKGoal,
			FColor::Magenta, false, -1,0, 0.2);
		DrawDebugLine(World, FVector::ZeroVector, RotatedIKGoal,
			FColor::Magenta, false,-1, 0, 0.5);
		*/
		
		float ArmLength1 = FVector::Distance(TargetArm.GetTranslation(), TargetForeArm.GetTranslation());
		float ArmLength2 = FVector::Distance(TargetForeArm.GetTranslation(), TargetHand.GetTranslation());
		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
					
		SoftIK(FVector::ZeroVector,
			RotatedIKGoal,
			ArmLength1 + ArmLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);
		
		/*
		DrawDebugLine(World, FVector::ZeroVector, SoftenedIKGoal,
			FColor::Emerald, false,-1, 0, 1);
		*/
		/*
		UE_LOG(LogNxRetarget, Log, TEXT("Soften - l1: %f, l2: %f, Target: %s, RotatedTarget: %s, SoftTarget: %s"),
			ArmLength1, ArmLength2, *IKTarget.ToString(), *RotatedIKGoal.ToString(), *SoftenedIKGoal.ToString());
		*/
		/*
		DrawDebugLine(World, RotatedIKGoal, SoftenedIKGoal,
			DebugColor, false, -1, 0, 1);
		*/
		
		// Solve IK
		float ArmAngle;
		float ElbowAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			FlipSolveDirection,
			ArmLength1,
			ArmLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			ArmAngle,
			ElbowAngle);

		/*
		UE_LOG(LogNxRetarget, Log, TEXT("IK - angle1: %f, angle2: %f"),
			ArmAngle, ElbowAngle);
		*/
		FVector ForeArmLS = TargetArm.GetRelativeTransform(TargetForeArm).GetTranslation();
		FVector HandLS = TargetForeArm.GetRelativeTransform(TargetHand).GetTranslation();
		ArmAngle = FMath::RadiansToDegrees(ArmAngle);
		ElbowAngle = FMath::RadiansToDegrees(ElbowAngle);
		ForeArmLS = FQuat::MakeFromEuler(FVector::UpVector * ArmAngle).RotateVector(ForeArmLS);
		HandLS = FQuat::MakeFromEuler(FVector::UpVector * ElbowAngle).RotateVector(HandLS);
		
		DrawDebugLine(World, FVector::ZeroVector, ForeArmLS,
			DebugColor, false, -1, 0, 1);
		DrawDebugLine(World, ForeArmLS, ForeArmLS + HandLS,
			DebugColor, false, -1, 0, 0.5);
		
		OutArmRotationWS = RotationOfIKChain * FQuat::MakeFromEuler(FVector::UpVector * ArmAngle);
		OutForeArmRotationLS = FQuat::MakeFromEuler(FVector::UpVector * ElbowAngle);
		OutHandRotationWS = SourceHand.GetRotation();
		// TODO: Just return rotations to put the arm back as it was to confirm we're handling this properly
		/*
		OutArmRotationWS = TargetArm.GetRotation();
		OutForeArmRotationLS = TargetArm.GetRotation().Inverse() * TargetForeArm.GetRotation();
		OutHandRotationWS = TargetHand.GetRotation();
		*/
		return IKSolutionFound;
	}

	static void RetargetFoot(
		FTransform SourceReference, FTransform SourceHips, FTransform SourceUpLeg, FTransform SourceLeg, FTransform SourceFoot,
		FTransform TargetReference, FTransform TargetHips, FTransform TargetUpLeg, FTransform TargetLeg, FTransform TargetFoot,
		FQuat& OutArmRotationWS, FQuat& OutForeArmRotationLS, FQuat& OutHandRotationWS,
		UKalmanVector* Kalman=nullptr, float ScaleRatio=1.0f,
		float SourceFootHeight=0, float FootHeightRatio=1.0f, bool FlipSolveDirection=false, float IKSoftness=0.01f ) 
	{
		FVector PoleVector = CalculatePoleVectorLocation(SourceUpLeg, SourceLeg, 0.1, FVector::ForwardVector);
		PoleVector = SpaceTransferLocation(PoleVector,SourceReference,TargetReference, ScaleRatio);

		float LegLength1 = FVector::Dist(TargetUpLeg.GetTranslation(), TargetLeg.GetTranslation());
		float LegLength2 = FVector::Dist(TargetLeg.GetTranslation(), TargetFoot.GetTranslation());
		
		// Space Transfer Feet to set IK Target
		const float AnkleHeightHeuristic = SoftFalloff(
			TargetReference.TransformPosition(TargetFoot.GetTranslation()).Z,
			SourceFootHeight);
		
		FVector IKTarget = SpaceTransferLocationNonUniform(SourceFoot.GetTranslation(),SourceReference,TargetReference,
			FVector(ScaleRatio, ScaleRatio, FMath::Lerp(FootHeightRatio, ScaleRatio, AnkleHeightHeuristic)));
		if(IsValid(Kalman)) IKTarget = Kalman->Update(IKTarget);
		
		FQuat RotationOfIKChain = FRotationMatrix::MakeFromXZ(
			IKTarget-TargetUpLeg.GetTranslation(),
			PoleVector - TargetUpLeg.GetTranslation()).ToQuat();
		FVector RotatedIKGoal = RotationOfIKChain.Inverse() * (IKTarget - TargetUpLeg.GetTranslation());
		// MocapSkeleton->GetJointWorldTransform(EMocapJoint::Spine3).InverseTransformPosition()

		FVector SoftenedIKGoal = RotatedIKGoal;
		// Soften IK targets
		float StretchFactor = 0.0f;
		
		SoftIK(TargetLeg.GetTranslation(),
			IKTarget,
			LegLength1 + LegLength2,
			IKSoftness,0,
			SoftenedIKGoal,
			StretchFactor);

		// Solve  IK
		float LegAngle;
		float KneeAngle;
		bool IKSolutionFound = CalcIK_2DTwoBoneAnalytic(
			false,
			LegLength1,
			LegLength2,
			SoftenedIKGoal.X, SoftenedIKGoal.Y,
			LegAngle,
			KneeAngle);

		OutArmRotationWS = RotationOfIKChain * FQuat::MakeFromEuler(FVector::RightVector * LegAngle);
		OutForeArmRotationLS = FQuat::MakeFromEuler(FVector::UpVector * KneeAngle);
		OutHandRotationWS = SourceFoot.GetRotation();
	}
};
