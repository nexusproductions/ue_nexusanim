﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "ARKitRow.h"
#include "ARKitCSVPlayer.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class NEXUSANIMRUNTIME_API UARKitCSVPlayer : public UActorComponent
{
	GENERATED_BODY()
	  
public:
	// Sets default values for this component's properties
	UARKitCSVPlayer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)//, meta=(RequiredAssetDataTags = "RowStructure=ARKitRowHandle"))
	UDataTable* ARKitData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PlaybackFPS;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Playing;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Loop;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int FrameCount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentFrame = 0;
	float FrameDeltaAccumulator = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FName> RowNames;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TMap<FName, float> FrameData;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable)
	void SetFrame(int32 FrameNumber);
	
	UFUNCTION(BlueprintCallable)
	void Init();
	
	UFUNCTION(BlueprintCallable)
	void UpdateFrameData(int32 Frame);
};
