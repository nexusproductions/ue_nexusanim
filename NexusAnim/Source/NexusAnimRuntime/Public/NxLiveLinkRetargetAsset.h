// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "LiveLinkRemapAsset.h"
#include "CoreMinimal.h"
#include "LiveLinkRetargetAsset.h"
 	
//#include "NxMappingEnum.h"
#include "NxRemappingRow.h"
#include "Roles/LiveLinkAnimationTypes.h"
#include "RetargetNexusSkeleton.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "Engine/SkeletalMesh.h"
#include "NxLiveLinkRetargetAsset.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogNxLLRetarget, Log, All);
/**
 * 
 */
class UAnimSequence;

USTRUCT(Blueprintable)
struct FNxRemappingRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
};

UCLASS(Blueprintable)
class NEXUSANIMRUNTIME_API UNxLiveLinkRetargetAsset : public ULiveLinkRetargetAsset
{
	GENERATED_UCLASS_BODY()
public:
	// Build OutPose from AnimationData if subject was from this type
	virtual void BuildPoseFromAnimationData(float DeltaTime, const FLiveLinkSkeletonStaticData* InSkeletonData, const FLiveLinkAnimationFrameData* InFrameData, FCompactPose& OutPose) override;
	void DebugDrawPose(FCompactPose& OutPose, FColor Color=FColor::Blue, float Thickness=1, bool DrawAxes=true, float AxesLength=2) const;
	void DebugDrawLiveLink(const FLiveLinkAnimationFrameData* FrameData, FColor Color=FColor::Green, float Thickness=1, bool DrawAxes=true, float AxesLength=2) const;
	void IKSpaceTransferLimbs(const FLiveLinkAnimationFrameData* FrameData, FCompactPose& OutPose, TArray<FTransform>& SegData);
	ENxXsensMapping GetParent(ENxXsensMapping XsensBone) const;
	bool HasParentXsens(ENxXsensMapping XsensBone) const;
	FTransform GetWorldLiveLinkTransform(ENxXsensMapping XsensBone, const FLiveLinkAnimationFrameData* FrameData, bool IdentityRotation=false) const;
	FTransform GetLocalLiveLinkTransform(ENxXsensMapping XsensBone, const FLiveLinkAnimationFrameData* FrameData, bool IdentityRotation=false) const;
	FCompactPoseBoneIndex XsensBoneToPoseIndex(ENxXsensMapping XsensBone, FCompactPose& OutPose) const;
	FTransform GetLocalPoseTransform(ENxXsensMapping XsensBone, FCompactPose& OutPose) const;
	FTransform GetLocalPoseTransform(FCompactPoseBoneIndex PoseBoneIndex, FCompactPose& OutPose) const;
	FTransform GetLocalPoseTransform(FName BoneName, FCompactPose& OutPose) const;
	FTransform GetWorldPoseTransform(ENxXsensMapping XsensBone, FCompactPose& OutPose) const;
	FTransform GetWorldPoseTransform(FCompactPoseBoneIndex PoseBoneIndex, FCompactPose& OutPose) const;

	// Build OutPose and OutCurve from the basic data. Called for every type of subjects
	virtual void BuildPoseAndCurveFromBaseData(float DeltaTime, const FLiveLinkBaseStaticData* InBaseStaticData, const FLiveLinkBaseFrameData* InBaseFrameData, FCompactPose& OutPose, FBlendedCurve& OutCurve) override;

	FName GetRemappedBoneName(const FName& BoneName) const;
	
#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

private:

	//Populate BoneNameMap and return the Transformed Bone names after the remapping
	TArray<FName> PopulateBoneNames(const FLiveLinkSkeletonStaticData* InSkeletonData);
	
	float CalculateVectorScale(FVector xsensVec, FVector unrealVec);
	FQuat IsRotationFromVecToVec(FVector a, FVector b);

	void CalculateTposeValues(FCompactPose OutPose, const FLiveLinkSkeletonStaticData& InSkeletonData, const FLiveLinkAnimationFrameData& InFrameData, FBlendedCurve& OutCurve);

	void OnSkeletalMeshChanged();

	void OnRemappingTableChanged();

	void AddBoneNamesToRemapTable();
	
public:
	UPROPERTY(EditAnywhere, DisplayName = "Remapping Target Table", BlueprintReadWrite, Category = "Live Link Mvn Remap")
	UDataTable* m_remapping_table;
	
	/** Map that stores all the information about bone remapping. This will be populated dynamically from the skeletal mesh. */
	UPROPERTY(EditAnywhere, DisplayName = "Remapping Assets", Category = "Live Link Mvn Remap")
	TMap<ENxXsensMapping, FNxRemappingRowHandle> m_remapping_rows;
	
	UPROPERTY(EditAnywhere, DisplayName = "Skeletal Mesh", BlueprintReadWrite, Category = "Live Link Mvn Remap")
	USkeletalMesh* m_skeletal_mesh;
public:
	void SetRemappingTable(UDataTable* DataTable);
	UDataTable* GetRemappingTable();

	void SetSkeletalMesh(USkeletalMesh* SkeletalMesh);

private:
	std::map<FName, ENxXsensMapping, FNameFastLess> m_remap_bones_names;
	
	TArray<FTransform> m_tposeWorld;

	TArray<FTransform> m_mvnToUnrealTpose;

	UPROPERTY(EditAnywhere, Category=Retarget)
	float ArmRatio = 1.0;
	UPROPERTY(EditAnywhere, Category=Retarget)
	float HipsRatio = 1.0;
	UPROPERTY(EditAnywhere, Category=Retarget)
	float FootHeightRatio = 1.0;
	UPROPERTY(EditAnywhere, Category=Retarget)
	float OriginalFootHeight;

	UPROPERTY()
	UKalmanVector* LeftHandKalman;
	UPROPERTY()
	UKalmanVector* RightHandKalman;
	UPROPERTY()
	UKalmanVector* LeftFootKalman;
	UPROPERTY()
	UKalmanVector* RightFootKalman;
	
	// Name mapping between source bone name and transformed bone name
	// (returned from GetRemappedBoneName)
	TMap<FName, FName> BoneNameMap;

	// Name mapping between source curve name and transformed curve name
	// (returned from GetRemappedCurveName)
	TMap<FName, FName> CurveNameMap;

	/** Blueprint.OnCompiled delegate handle */
	FDelegateHandle OnBlueprintCompiledDelegate;

	UPROPERTY(EditAnywhere, Category = "Reference Pose")
	UAnimSequence* TPoseAnimation;

	UPROPERTY()
	URetargetNexusSkeleton* RetargetNexusSkeleton;
	
	int m_retarget;

	bool m_doLog;
};
