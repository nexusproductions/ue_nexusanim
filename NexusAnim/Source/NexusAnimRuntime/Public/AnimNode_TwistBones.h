﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "QuaternionMath.h"
#include "Math/Axis.h"
#include "Animation/AnimNodeBase.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "AnimNode_TwistBones.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTwistBones, Log, All);

USTRUCT()
struct FTwistBonesCachedBoneData
{
	GENERATED_BODY()

	FTwistBonesCachedBoneData()
		: Bone(NAME_None)
		, RefSkeletonIndex(INDEX_NONE)
	{}
	
	FTwistBonesCachedBoneData(const FName& InBoneName, int32 InRefSkeletonIndex): Bone(InBoneName)
		, RefSkeletonIndex(InRefSkeletonIndex)
	{}
	
	/** The bone we refer to */
	UPROPERTY()
	FBoneReference Bone;

	/** Index of the bone in the reference skeleton */
	UPROPERTY()
	int32 RefSkeletonIndex;
};

USTRUCT(BlueprintType)
struct NEXUSANIMRUNTIME_API FAnimNode_TwistBones : public FAnimNode_SkeletalControlBase //FAnimNode_Base //SkeletalControlBase
{
	GENERATED_BODY()
	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink BasePose;
	*/
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	//TMap<FName, FQuat> BoneRotations;

public:
	FAnimNode_TwistBones();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext& Context) override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	//virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) override;
	virtual void OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance) override;
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;
    virtual bool IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) override;
	
	/** Name of root bone from which the spline extends **/
	UPROPERTY(EditAnywhere, Category = "Parameters")
	FBoneReference StartBone;

	/** Name of bone at the end of the spline chain. Bones after this will not be altered by the controller. */
	UPROPERTY(EditAnywhere, Category = "Parameters")
	FBoneReference EndBone;
	
	UPROPERTY(EditAnywhere, Category = "Parameters")
	bool RotationSourceIsParent = true;
	
	UPROPERTY(EditAnywhere, Category = "Parameters")
	TEnumAsByte<EAxis::Type> TwistAxis = EAxis::Type::Z;

	UPROPERTY(EditAnywhere, Category = "Parameters")
	float UpDirectionLerp = 0.25f;
	
	
	/** Axis of the controlled bone (ie the direction of the spline) to use as the direction for the curve. */
	//UPROPERTY(EditAnywhere, Category = "Parameters")
	//ESplineBoneAxis BoneAxis;
	
	/** Cached data for bones in the IK chain, from start to end */
	TArray<FTwistBonesCachedBoneData> CachedBoneReferences;

	/** Cached bone lengths. Same size as CachedBoneReferences */
	TArray<float> CachedBoneLengths;

	/** Cached bone offset rotations. Same size as CachedBoneReferences */
	TArray<FQuat> CachedOffsetRotations;
	/** Build bone references & reallocate transforms from the supplied ref skeleton */
	void GatherBoneReferences(const FReferenceSkeleton& RefSkeleton);
		
private:
	// FAnimNode_SkeletalControlBase interface
	virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones);
	// End of FAnimNode_SkeletalControlBase interface	
};


