// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class NexusAnimRuntime : ModuleRules
{
	public NexusAnimRuntime(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				//"NexusAnimRuntime/Public",
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				//"NexusAnimRuntime/Private",
				// ... add other private include paths required here ...
				
				"C:/Program Files/Epic Games/UE_4.26/Engine/Plugins/Animation/LiveLink/Source/LiveLink",
				//"C:/Program Files/Epic Games/UE_4.26/Engine/Source/ThirdParty/Eigen",
				"ThirdParty/Eigen",
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core", 
				"LiveLinkInterface",
				"LiveLinkMessageBusFramework",
				"LiveLink",
				"LiveLinkComponents"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"AnimGraphRuntime",
				"Eigen",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
