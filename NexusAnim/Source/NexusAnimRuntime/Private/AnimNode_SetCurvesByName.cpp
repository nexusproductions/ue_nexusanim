﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNode_SetCurvesByName.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"


FAnimNode_SetCurvesByName::FAnimNode_SetCurvesByName()
{
}
void FAnimNode_SetCurvesByName::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	BasePose.Initialize(Context);
}

void FAnimNode_SetCurvesByName::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
}


void FAnimNode_SetCurvesByName::Evaluate_AnyThread(FPoseContext& Output)
{
	// Evaluate the input
	BasePose.Evaluate(Output);
	for(TPair<FName, float> Curve : Curves)
	{
		USkeleton* Skeleton = Output.AnimInstanceProxy->GetSkeleton();
		SmartName::UID_Type NameUID = Skeleton->GetUIDByName(USkeleton::AnimCurveMappingName, Curve.Key);
		Output.Curve.Set(NameUID, Curve.Value);
	}
	
}

void FAnimNode_SetCurvesByName::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);
	
	DebugData.AddDebugItem(DebugLine);

	BasePose.GatherDebugData(DebugData);
}
