﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "NexusSkeletonTest.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY(LogNexusSkeletonTest);

// Sets default values
ANexusSkeletonTest::ANexusSkeletonTest()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ANexusSkeletonTest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANexusSkeletonTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ANexusSkeletonTest::InitSkeleton()
{
	NxSkeleton = NewObject<UNxSkeletonXsens>();
	
	NxSkeleton->Init(InitialXsensTransforms);
	XsensWorldTransforms = NxSkeleton->GetJointWorldTransforms();

	
	NxSkeleton2 = NewObject<UNxSkeletonXsens>();
	NxSkeleton2->Init(XsensWorldTransforms, EBoneSpace::World);
	// NxSkeleton->Init();
	// NxSkeleton->Init(InitialWorldTransforms, Parents);
}

void ANexusSkeletonTest::Draw()
{
	//UE_LOG(LogNexusSkeletonTest, Log, TEXT("Tick"));
	//DrawDebugLine(GetWorld(), FVector(), FVector::OneVector * 50, FColor::Blue,
	//		false, 0.5, 0, 1);
	if(!IsValid(NxSkeleton)) return;
	if(!IsValid(NxSkeleton2)) return;
	for(FTransform JointTransform : NxSkeleton->GetJointLocalTransforms())
	{
		DrawDebugLine(GetWorld(), FVector(), JointTransform.GetTranslation(),
			FColor::Purple, false, 1, 0, 0.1);
	}
	NxSkeleton->DrawDebug(GetWorld(), FColor::Red, 0.1, 1);
	NxSkeleton2->SetJointWorldTranslation(0, FVector(20, 0, 0));
	NxSkeleton2->DrawDebug(GetWorld(), FColor::Green, 0.1, 1);
	//NxSkeleton->SetJointLocalTranslation(1, NxSkeleton->GetJointLocalTranslation(1) * 1.5);
	//NxSkeleton->DrawDebug(GetWorld(), FColor::Green, 1, 1);
}

