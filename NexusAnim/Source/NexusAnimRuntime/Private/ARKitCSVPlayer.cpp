﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ARKitCSVPlayer.h"


// Sets default values for this component's properties
UARKitCSVPlayer::UARKitCSVPlayer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UARKitCSVPlayer::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UARKitCSVPlayer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	
	if(!(FrameCount > 0)) return;
	if(Playing)
	{
		FrameDeltaAccumulator += DeltaTime;
		if(FrameDeltaAccumulator > 1.0 / PlaybackFPS)
		{
			CurrentFrame++;
		}
		if(CurrentFrame >= FrameCount)
		{
			if(Loop) CurrentFrame = 0;
			else CurrentFrame = FrameCount - 1;
		}
	}
}

void UARKitCSVPlayer::SetFrame(int32 FrameNumber)
{
	if(FrameNumber >= FrameCount) FrameNumber = 0;
	CurrentFrame = FrameNumber;
}


void UARKitCSVPlayer::Init()
{
	TArray<FARKitRow> OutRows;
	RowNames = ARKitData->GetRowNames();
	RowNames.Sort();
	FrameCount = RowNames.Num();
}

void UARKitCSVPlayer::UpdateFrameData(int32 Frame)
{
	const FString ContextString;
	const FARKitRow* ARKitRow = ARKitData->FindRow<FARKitRow>(RowNames[CurrentFrame], ContextString);
	FrameData = ARKitRow->ToMap();
}

