﻿#include "RetargetNexusSkeleton.h"


void URetargetNexusSkeleton::UpdateMocapSkeletonRotations(TMap<FName, FQuat> JointRotations) const
{
	MocapSkeleton->SetJointLocalRotations(JointRotations);
}

void URetargetNexusSkeleton::UpdateMocapSkeletonTranslations(TMap<FName, FVector> JointTranslations) const
{
	MocapSkeleton->SetJointLocalTranslations(JointTranslations);
}

FName URetargetNexusSkeleton::GetRemappedJointName(int32 JointIndex) const
{
	return *MocapToFinalRemap.Find(static_cast<EMocapJoint>(JointIndex));
}

FName URetargetNexusSkeleton::GetRemappedJointName(const FName JointName) const
{
	return GetRemappedJointName(RetargetSkeleton->GetJointIndex(JointName));
}

void URetargetNexusSkeleton::RemapFinalSkeleton() const
{
	FinalSkeleton->SetJointLocalTranslation(
		FinalSkeleton->LocomotionRootIndex,
		RetargetSkeleton->GetJointLocalTransform(RetargetSkeleton->LocomotionRootIndex).GetTranslation());
		
	for(int i = 0; i < RetargetSkeleton->Length; i++)
	{
		const FName RemappedJointName = GetRemappedJointName(i);
		FinalSkeleton->SetJointLocalRotation(RemappedJointName, RetargetSkeleton->GetJointLocalTransform(i).GetRotation());
	}
}

void URetargetNexusSkeleton::RetargetMocapSkeleton() const
{
	// Place Hips location scaled by ratio
	RetargetSkeleton->SetJointWorldTranslation(
		RetargetSkeleton->LocomotionRootIndex,
		MocapSkeleton->GetJointWorldTransform(MocapSkeleton->LocomotionRootIndex).GetTranslation() * HipsRatio);
		
	// Put in base rotations
	TArray<FTransform> MocapTransforms = MocapSkeleton->GetJointLocalTransforms();
	for(int i = 0; i<MocapTransforms.Num(); i++)
	{
		RetargetSkeleton->SetJointLocalRotation(i, MocapTransforms[i].GetRotation());
	}

	RetargetLeftHand();
	RetargetRightHand();
	RetargetLeftFoot();
	RetargetRightFoot();
}

void URetargetNexusSkeleton::Setup()
{
	SetupMocapSkeleton();
	SetupFinalSkeleton();
	SetupRetargetSkeleton();
	RetargetArmLength1 = FinalSkeleton->GetJointLength(FinalSkeleton->GetJointIndex(MocapToFinalRemap[EMocapJoint::LeftForeArm]));
	RetargetArmLength2 = FinalSkeleton->GetJointLength(FinalSkeleton->GetJointIndex(MocapToFinalRemap[EMocapJoint::LeftHand]));
	GetArmRatio();
	RetargetLegLength1 = FinalSkeleton->GetJointLength(FinalSkeleton->GetJointIndex(MocapToFinalRemap[EMocapJoint::LeftLeg]));
	RetargetLegLength2 = FinalSkeleton->GetJointLength(FinalSkeleton->GetJointIndex(MocapToFinalRemap[EMocapJoint::LeftFoot]));
		
	SourceFootHeight = MocapSkeleton->GetJointWorldTransform(EMocapJoint::Reference).InverseTransformPosition(MocapSkeleton->GetJointWorldTransform(EMocapJoint::LeftFoot).GetTranslation()).Z;
	GetHipsRatio();
		
	GetFinalJointRotations();
}

void URetargetNexusSkeleton::Update()
{
	RetargetMocapSkeleton();
	RemapFinalSkeleton();
}

void URetargetNexusSkeleton::SetupMocapSkeleton()
{
	MocapSkeleton = NewObject<UNxSkeletonMocap>();
	TArray<FTransform> Transforms;
	Transforms.Reserve(EMocapJoint::NumJoints);
	// TMap<FName, FName> ParentsMap;
	for (int i=0; i<EMocapJoint::NumJoints; i++)
	{
		FName JointName = MocapSkeleton->GetJointName(i);
		// FName ParentName = MocapSkeleton->GetJointName(MocapSkeleton->JointParentsArray.Find(i));
		// ParentsMap.Add(JointName, ParentName);
		FTransform JointTransform = *MocapLocalTransforms.Find(JointName);
		// TPose for orthagonal skeleton
		JointTransform.SetRotation(FQuat::Identity);
		Transforms.Add(JointTransform);
	}
	MocapSkeleton->Init(Transforms);
	MocapSkeleton->StoreRestPose();
}

void URetargetNexusSkeleton::SetupFinalSkeleton()
{
	const int32 NumBones = SkeletalMesh->RefSkeleton.GetNum();
	TArray<FTransform> JointTransformArray = SkeletalMesh->RefSkeleton.GetRefBonePose();
	TMap<FName, FTransform> JointTransformsMap;
	TMap<FName, FName> ParentsMap;
	for(int32 i = 0; i < NumBones; i++)
	{
		FName JointName = SkeletalMesh->RefSkeleton.GetBoneName(i);
		FName ParentName = SkeletalMesh->RefSkeleton.GetBoneName(SkeletalMesh->RefSkeleton.GetParentIndex(i));
		JointTransformsMap.Add(JointName, JointTransformArray[i]);
		ParentsMap.Add(JointName, ParentName);				
	}
	FinalSkeleton = NewObject<UNxSkeletonBase>(); //JointTransformsMap, ParentsMap);
	FinalSkeleton->Init(JointTransformsMap, ParentsMap);
	if(TPoseAsset ==nullptr) return;
	if(TPoseAsset->GetNumPoses() == 0) return;
	TArray<FTransform> TPoseTransforms;
	TPoseAsset->GetFullPose(0, TPoseTransforms);
		
	//
	// FinalSkeleton = MakeShared<UNxSkeletonBase>(UNxSkeletonBase(JointTransformsMap, ParentsMap));
}

