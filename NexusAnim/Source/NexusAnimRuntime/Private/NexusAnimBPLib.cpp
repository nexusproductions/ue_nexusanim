// Fill out your copyright notice in the Description page of Project Settings.


#include "NexusAnimBPLib.h"

UNexusAnimBPLib::UNexusAnimBPLib(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

float UNexusAnimBPLib::ScaleFloat(const float A, const float B)
{
	return A * B;
}

void UNexusAnimBPLib::GetBoneTransformAtTimeFromAnimSequence(
	UAnimSequence* MyAnimSequence,
	float AnimTime,int BoneIdx, bool bUseRawDataOnly, FTransform& OutTransform)
{
	const TArray<FTrackToSkeletonMap> & TrackToSkeletonMap = bUseRawDataOnly ?
		MyAnimSequence->GetRawTrackToSkeletonMapTable() : MyAnimSequence->GetCompressedTrackToSkeletonMapTable();
  
	if ((TrackToSkeletonMap.Num() > 0) && (TrackToSkeletonMap[0].BoneTreeIndex == 0))
	{
		MyAnimSequence->GetBoneTransform(OutTransform, BoneIdx, AnimTime, bUseRawDataOnly);
	}
}
