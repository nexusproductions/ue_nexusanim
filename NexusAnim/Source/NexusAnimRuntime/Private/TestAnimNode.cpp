// Fill out your copyright notice in the Description page of Project Settings.

#include "TestAnimNode.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"



FTestAnimNode::FTestAnimNode()
{
}
void FTestAnimNode::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	BasePose.Initialize(Context);
}

void FTestAnimNode::CacheBones(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FTestAnimNode::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
}


void FTestAnimNode::Evaluate(FPoseContext& Output)
{
	// Evaluate the input
	BasePose.Evaluate(Output);
}

void FTestAnimNode::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);


	DebugData.AddDebugItem(DebugLine);

	BasePose.GatherDebugData(DebugData);
}

