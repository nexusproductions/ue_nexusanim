// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimNode_Retarget.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"

DEFINE_LOG_CATEGORY(LogRetarget)

FAnimNode_Retarget::FAnimNode_Retarget()
{
	for (FName XsensBoneName : XsensBoneNames) {
		if (!BoneMappings.Contains(XsensBoneName)) {
			BoneMappings.Add(XsensBoneName, XsensBoneName);
		}
	}
	

}
void FAnimNode_Retarget::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);
	//Need to build an array from the BoneMapping values which matches the SkeletalMesh bone array
	// OK, this makes an array using the skeleton index which I should be able to transform later
	LiveLinkTransformArray.Reserve(BoneMappings.Num());
	if (SourceMeshComponent.IsValid())
	{
		SourceMesh = SourceMeshComponent->SkeletalMesh;
		const FReferenceSkeleton ReferenceSkeleton = SourceMesh->RefSkeleton;
		for(const TPair<FName, FName> BoneMapping: BoneMappings)
		{
			FName BoneName = BoneMapping.Value;
			const int32 BoneIndex = ReferenceSkeleton.FindBoneIndex(BoneName);
			LiveLinkTransformArray.Insert(LiveLinkTransforms[BoneName], BoneIndex); 
		}
	}
	BasePose.Initialize(Context);
}

void FAnimNode_Retarget::CacheBones(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FAnimNode_Retarget::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
}


void FAnimNode_Retarget::Evaluate(FPoseContext& Output)
{
	// Evaluate the input
	BasePose.Evaluate(Output);

	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)
	FCompactPose& OutPose = Output.Pose;
	OutPose.ResetToRefPose();
	const FBoneContainer& RequiredBones = OutPose.GetBoneContainer();

	for (FCompactPoseBoneIndex PoseBoneIndex : OutPose.ForEachBoneIndex())
	{
		
		UE_LOG(LogRetarget, Log, TEXT(""))
		// LiveLinkTransformArray should match the skeleton indices
		// So get the skeleton index for this bone from the pose bone index
		const int32 SkeletonBoneIndex = RequiredBones.GetSkeletonIndex(PoseBoneIndex);
		if(LiveLinkTransformArray.IsValidIndex(SkeletonBoneIndex))
		{
			// This is expecting local transforms
			// I should expect world transforms and convert to local?
			OutPose[PoseBoneIndex] = LiveLinkTransformArray[SkeletonBoneIndex]; 
		}
		
		// const int32 MeshBoneIndex = RequiredBones.GetSkeletonToPoseBoneIndexArray()[SkeletonBoneIndex];
		
		/*
		const int32* Value = BoneMapToSource.Find(MeshBoneIndex);
		if (Value && SourceMeshTransformArray.IsValidIndex(*Value))
		{
			const int32 SourceBoneIndex = *Value;
			const int32 ParentIndex = CurrentMesh->RefSkeleton.GetParentIndex(SourceBoneIndex);
			const FCompactPoseBoneIndex MyParentIndex = RequiredBones.GetParentBoneIndex(PoseBoneIndex);
			// only apply if I also have parent, otherwise, it should apply the space bases
			if (SourceMeshTransformArray.IsValidIndex(ParentIndex) && MyParentIndex != INDEX_NONE)
			{
				const FTransform& ParentTransform = SourceMeshTransformArray[ParentIndex];
				const FTransform& ChildTransform = SourceMeshTransformArray[SourceBoneIndex];
				OutPose[PoseBoneIndex] = ChildTransform.GetRelativeTransform(ParentTransform);
			}
			else
			{
				OutPose[PoseBoneIndex] = SourceMeshTransformArray[SourceBoneIndex];
			}
		}
		*/
	}
}

void FAnimNode_Retarget::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);


	DebugData.AddDebugItem(DebugLine);

	BasePose.GatherDebugData(DebugData);
}

