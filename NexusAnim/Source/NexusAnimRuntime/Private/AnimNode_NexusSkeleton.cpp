﻿#include "AnimNode_NexusSkeleton.h"

DEFINE_LOG_CATEGORY(LogNexusSkeleton)

FAnimNode_NexusSkeleton::FAnimNode_NexusSkeleton()
{
	RetargetNexusSkeleton = NewObject<URetargetNexusSkeleton>();
}

void FAnimNode_NexusSkeleton::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	BasePose.Initialize(Context);
	if(IsValid(RetargetNexusSkeleton))
	{
		UE_LOG(LogNexusSkeleton, Log, TEXT("RetargetNexusSkeleton is nullptr"))		
		RetargetNexusSkeleton = NewObject<URetargetNexusSkeleton>();
		UE_LOG(LogNexusSkeleton, Log, TEXT("Now is nullptr? %s"), RetargetNexusSkeleton == nullptr)
	}
	
	if(IsValid(RetargetNexusSkeleton)) return;
	RetargetNexusSkeleton->Init(SkeletalMesh, TPoseAnimationAsset);
	RetargetNexusSkeleton->MocapLocalTransforms = InputTransforms;
	RetargetNexusSkeleton->Setup();
}

void FAnimNode_NexusSkeleton::CacheBones(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FAnimNode_NexusSkeleton::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
	RetargetNexusSkeleton->MocapLocalTransforms = InputTransforms;
	RetargetNexusSkeleton->Update();
}

void FAnimNode_NexusSkeleton::Evaluate_AnyThread(FPoseContext& Output)
{
	FAnimNode_Base::Evaluate_AnyThread(Output);
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)
	FCompactPose& OutPose = Output.Pose;
	//OutPose.ResetToRefPose();
	// OutPose.ResetToRefPose();
	const FBoneContainer& RequiredBones = OutPose.GetBoneContainer();
	TArray<int32> NSPoseIndices;
	for(FName JointName : RetargetNexusSkeleton->FinalSkeleton->JointNameArray)
	{
		FCompactPoseBoneIndex PoseIndex = static_cast<FCompactPoseBoneIndex>(RequiredBones.GetPoseBoneIndexForBoneName(JointName));
		if(OutPose.IsValidIndex(PoseIndex))
		{
			OutPose[PoseIndex] = RetargetNexusSkeleton->FinalSkeleton->GetJointLocalTransform(JointName);
		}
	}
}


void FAnimNode_NexusSkeleton::GatherDebugData(FNodeDebugData& DebugData)
{
	FAnimNode_Base::GatherDebugData(DebugData);
}
