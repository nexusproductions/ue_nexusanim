﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNode_TwistBones.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"

DEFINE_LOG_CATEGORY(LogTwistBones)

FAnimNode_TwistBones::FAnimNode_TwistBones()
{
}

void FAnimNode_TwistBones::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	ComponentPose.Initialize(Context);
}

void FAnimNode_TwistBones::CacheBones(const FAnimationCacheBonesContext& Context)
{
	ComponentPose.CacheBones(Context);
}

void FAnimNode_TwistBones::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	ComponentPose.Update(Context);
}

void FAnimNode_TwistBones::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
	// Evaluate the input
	//ComponentPose.Evaluate(Output);
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(EvaluateSkeletalControl_AnyThread)
	const FBoneContainer& RequiredBones = Output.Pose.GetPose().GetBoneContainer();
	
	FQuat Rotation;
	if(RotationSourceIsParent)
	{
		const FCompactPoseBoneIndex BoneIndex = StartBone.GetCompactPoseIndex(RequiredBones);
		Rotation = Output.Pose.GetComponentSpaceTransform(BoneIndex).GetRotation();
	}else{
		const FCompactPoseBoneIndex BoneIndex = EndBone.GetCompactPoseIndex(RequiredBones);
		Rotation = Output.Pose.GetComponentSpaceTransform(BoneIndex).GetRotation();
	}
	const FVector UpDirection = FQuat::Slerp(FQuat::Identity, Rotation, UpDirectionLerp) * FVector::UpVector;
	const FQuat Space = FRotationMatrix::MakeFromXZ(Rotation * FVector::ForwardVector, UpDirection).ToQuat();
	
	FQuat Twist;
	FQuat Swing;
	if (RotationSourceIsParent)
	{
		const FQuat rotationInSpace = Space.Inverse() * Rotation;
		Twist = QuaternionMath::Twist(rotationInSpace, TwistAxis);
		Swing = QuaternionMath::Swing(rotationInSpace, Twist);
	}
	else // If it's the bottom child (like the hand) we just use the direct rotation
	{
		Twist = QuaternionMath::Twist(Rotation, TwistAxis);
		Swing = QuaternionMath::Swing(Rotation, Twist);
	}
	const int TwistDistribution = CachedBoneReferences.Num();
	const float TwistAmount = 1.0f / TwistDistribution;
	const FQuat DistributedTwist = FQuat::Slerp(FQuat::Identity, Twist, TwistAmount);
	
	for(FTwistBonesCachedBoneData Bone : CachedBoneReferences)
	{
		FCompactPoseBoneIndex BoneIndex = Bone.Bone.GetCompactPoseIndex(RequiredBones);
		FTransform BoneTransform = Output.Pose.GetComponentSpaceTransform(BoneIndex);
		// If the rotation source is the top parent, then the swing has to take place in the space of it's parent.
		if (RotationSourceIsParent)
		{
			BoneTransform.SetRotation(Space * DistributedTwist * Swing);
			OutBoneTransforms.Emplace(BoneIndex, BoneTransform);
		}
		else
		{
			BoneTransform.SetRotation(DistributedTwist * Swing);
			OutBoneTransforms.Emplace(BoneIndex, BoneTransform);
		}
	}
/*
	for (TPair<FName, FTransform> BoneTransform: BoneRotations)
	{
		const int32 PoseIndex = RequiredBones.GetPoseBoneIndexForBoneName(BoneTransform.Key);
		
		Output.Pose[static_cast<FCompactPoseBoneIndex>(PoseIndex)] = BoneTransform.Value;
		
		//Output.Pose.GetComponentSpaceTransform()
	}
*/	
}

void FAnimNode_TwistBones::GatherDebugData(FNodeDebugData& DebugData)
{
	const FString DebugLine = DebugData.GetNodeName(this);
	
	DebugData.AddDebugItem(DebugLine);

	ComponentPose.GatherDebugData(DebugData);
}

bool FAnimNode_TwistBones::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) 
{
	// If any bone references are valid, evaluate.
	
	if (CachedBoneReferences.Num() > 0)
	{
		for (FTwistBonesCachedBoneData& CachedBoneData : CachedBoneReferences)
		{
			if (CachedBoneData.Bone.IsValidToEvaluate(RequiredBones))
			{
				return true;
			}
		}
	}
	return false;
}

/*
void FAnimNode_TwistBones::EvaluateComponentSpace_AnyThread(FComponentSpacePoseContext& Output)
{
	FBoneContainer BoneContainer = Output.Pose.GetPose().GetBoneContainer();
	// BoneTransforms.KeySort()
	for(TPair<FName, FQuat> RotationMap : BoneRotations)
	{
		FBoneReference BoneReference = FBoneReference(RotationMap.Key);
		if(!BoneReference.IsValidToEvaluate(BoneContainer)) continue;
		// Output.Pose.GetComponentSpaceTransform(BoneReference.GetCompactPoseIndex(BoneContainer));
		FCompactPoseBoneIndex PoseBoneIndex = BoneReference.GetCompactPoseIndex(BoneContainer);
		FTransform BoneTransform = Output.Pose.GetComponentSpaceTransform(PoseBoneIndex);
		BoneTransform.SetRotation(RotationMap.Value);
	}
}*/

void FAnimNode_TwistBones::OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance)
{
	if (InAnimInstance->GetSkelMeshComponent() && InAnimInstance->GetSkelMeshComponent()->SkeletalMesh)
	{
		GatherBoneReferences(InAnimInstance->GetSkelMeshComponent()->SkeletalMesh->RefSkeleton);
	}
}

void FAnimNode_TwistBones::InitializeBoneReferences(const FBoneContainer& RequiredBones) 
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(InitializeBoneReferences)
	StartBone.Initialize(RequiredBones);
	EndBone.Initialize(RequiredBones);

	GatherBoneReferences(RequiredBones.GetReferenceSkeleton());

	for (FTwistBonesCachedBoneData& CachedBoneData : CachedBoneReferences)
	{
		CachedBoneData.Bone.Initialize(RequiredBones);
	}
}

void FAnimNode_TwistBones::GatherBoneReferences(const FReferenceSkeleton& RefSkeleton)
{
	CachedBoneReferences.Reset();
	int32 StartIndex = RefSkeleton.FindBoneIndex(StartBone.BoneName);
	int32 EndIndex = RefSkeleton.FindBoneIndex(EndBone.BoneName);
	if (StartIndex != INDEX_NONE && EndIndex != INDEX_NONE)
	{
		// walk up hierarchy towards root from end to start
		int32 BoneIndex = EndIndex;
		while (BoneIndex != StartIndex)
		{
			// we hit the root, so clear the cached bones - we have an invalid chain
			if (BoneIndex == INDEX_NONE)
			{
				CachedBoneReferences.Reset();
				break;
			}

			FName BoneName = RefSkeleton.GetBoneName(BoneIndex);
			CachedBoneReferences.EmplaceAt(0, BoneName, BoneIndex);
			FTwistBonesCachedBoneData* CachedBone = CachedBoneReferences.GetData(); 
			BoneIndex = RefSkeleton.GetParentIndex(BoneIndex);
		}

		if (CachedBoneReferences.Num())
		{
			FName BoneName = RefSkeleton.GetBoneName(StartIndex);
			CachedBoneReferences.EmplaceAt(0, BoneName, StartIndex);
		}
	}
}
