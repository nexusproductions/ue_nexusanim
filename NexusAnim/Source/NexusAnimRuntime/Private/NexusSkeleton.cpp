﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "NexusSkeleton.h"

#include "../../../../Developer/RiderLink/Source/RD/thirdparty/spdlog/include/spdlog/fmt/bundled/format.h"
DEFINE_LOG_CATEGORY(LogNxSkeleton)

void UNxSkeletonBase::DrawDebug(UWorld* World, FColor Color = FColor::Cyan, float Thickness = 1, float LifeTime = -1, bool DrawAxes=true, float AxesLength=1)
{
	// UE_LOG(LogNxSkeleton, Log, TEXT("DrawDebug: %d Bones"), GetLength());
	DrawDebugLine(World, FVector::ZeroVector, FVector::OneVector*100, Color, false, -1, 0, Thickness);
	for(int i =0; i<GetLength(); i++)
	{
		FTransform JointTransform = GetJointWorldTransform(i);
		DrawDebugLine(World,
			JointTransform.GetTranslation(),
			GetJointWorldTransform(GetParentIndex(i)).GetTranslation(),
			Color, false,LifeTime, 0, Thickness);
	
		if(true) //(DrawAxes)
		{
			DrawDebugLine(World,
				JointTransform.GetTranslation(),
				JointTransform.GetTranslation() + JointTransform.GetRotation().GetRightVector() * AxesLength * JointTransform.GetScale3D().X,
				FColor::Red, false, LifeTime, 0, Thickness * 0.2);
			DrawDebugLine(World,
				JointTransform.GetTranslation(),
				JointTransform.GetTranslation() + JointTransform.GetRotation().GetForwardVector() * AxesLength * JointTransform.GetScale3D().Y,
				FColor::Green, false, LifeTime, 0, Thickness * 0.2);
			DrawDebugLine(World,
				JointTransform.GetTranslation(),
				JointTransform.GetTranslation() + JointTransform.GetRotation().GetUpVector() * AxesLength * JointTransform.GetScale3D().Z,
				FColor::Blue, false, LifeTime, 0, Thickness * 0.2);
		}
	}
}

void UNxSkeletonMocap::Init(TMap<FName, FTransform> JointTransforms)
{
	if(JointTransforms.Num()!=GetLength()) return;
	JointTransformArray.Empty();
	JointTransformArray.Reserve(Length);
		
	for(FName JointName : JointNameArray)
	{
		if(!JointTransforms.Contains(JointName)) continue;
		SetJointLocalTransform(JointName, *JointTransforms.Find(JointName));
	}
}

void UNxSkeletonMocap::Init(TArray<FTransform> JointTransforms)
{
	//UE_LOG(LogNxSkeleton, Log, TEXT("Init: %d"), JointTransforms.Num());
	if(JointTransforms.Num()!=GetLength()) return;
		
	//UE_LOG(LogNxSkeleton, Log, TEXT("Transform Count OK"));
	JointTransformArray.Empty();
	JointTransformArray.Reserve(GetLength());
	for(int i = 0; i < GetLength(); i++)
	{
		JointTransformArray.Add(JointTransforms[i]);
	}
}
void UNxSkeletonXsens::Init(TArray<FTransform> JointTransforms, EBoneSpace BoneSpace)
{
    InitXsens();
	SetLength(JointTransforms.Num());
	
	//UE_LOG(LogNxSkeleton, Log, TEXT("Init: %d"), GetLength());
	//if(JointTransforms.Num()!=GetLength()) return;
	for(int i = 0; i < GetLength(); i++)
	{
		//UE_LOG(LogNxSkeleton, Log, TEXT("%s"), *JointTransforms[i].ToString());
	}
			
    
	if(BoneSpace==Local)
	{
		//UE_LOG(LogNxSkeleton, Log, TEXT("Local"));
		SetJointTransformArray(JointTransforms);
	}
	else if(BoneSpace==World)
	{
		//UE_LOG(LogNxSkeleton, Log, TEXT("World"));
		TArray<FTransform> LocalJointTransforms;
		LocalJointTransforms.Reserve(GetLength());
		for(int i = 0; i < GetLength(); i++)
		{
			int32 ParentIndex = GetParentIndex(i);
			if(ParentIndex<0)
				LocalJointTransforms.Add(FTransform(JointTransforms[i].ToMatrixNoScale()));
			else
			{
				LocalJointTransforms.Add(
					FTransform(JointTransforms[i].ToMatrixNoScale() *
							JointTransforms[ParentIndex].ToMatrixNoScale().Inverse()));
				//LocalJointTransforms.Add(JointTransforms[i] * JointTransforms[ParentIndex].Inverse());
				
			}
		}
		SetJointTransformArray(LocalJointTransforms);
	}
	
	/*
	TArray<FTransform> LocalJointTransforms;
	LocalJointTransforms.Reserve(GetLength());
	for(int i =0; i<GetLength(); i++)
	{
		int32 ParentIndex = GetParentIndex(i);
		FTransform ParentTransform;
		FTransform JointTransform = JointTransforms[i];
		if(ParentIndex>=0) ParentTransform = JointTransforms[ParentIndex];
		LocalJointTransforms.Add(ParentTransform.Inverse()*JointTransform);
		UE_LOG(LogNxSkeleton, Log, TEXT("%d: %s"), i, *LocalJointTransforms[i].GetTranslation().ToString());
	}
	
	SetJointTransformArray(LocalJointTransforms);
	*/
	/*
	UE_LOG(LogNxSkeleton, Log, TEXT("Have set num joints: %d"), JointTransforms.Num());
	UE_LOG(LogNxSkeleton, Log, TEXT("Transform Count OK"));
	JointTransformArray.Empty();
	JointTransformArray.Reserve(GetLength());
	for(int i = 0; i < GetLength(); i++)
	{
		UE_LOG(LogNxSkeleton, Log, TEXT("%d: %s"), i, *JointTransforms[i].ToString())
		JointTransformArray.Add(JointTransforms[i]);
	}
	*/
}
