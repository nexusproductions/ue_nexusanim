﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Kalman.h"

UKalmanFloat::UKalmanFloat() 
{
}

UKalmanFloat::UKalmanFloat(const FObjectInitializer& ObjectInitializer): UObject(ObjectInitializer)
{
}

float UKalmanFloat::Update(const float Value)
{
	const double K = (P + Q) / (P + Q + R);
	P = K * R;
	X += (Value - X) * K;
	return X;
}

UKalmanVector::UKalmanVector()
{
}

UKalmanVector::UKalmanVector(const FObjectInitializer& ObjectInitializer): UObject(ObjectInitializer)
{
}

FVector UKalmanVector::Update(const FVector Value)
{
	const double K = (P + Q) / (P + Q + R);
	P = K * R;
	Xx += (Value.X - Xx) * K;
	Xy += (Value.Y - Xy) * K;
	Xz += (Value.Z - Xz) * K;
	return FVector(Xx, Xy, Xz);
}
