﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimNode_SetBonesByName.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"



FAnimNode_SetBonesByName::FAnimNode_SetBonesByName()
{
}
void FAnimNode_SetBonesByName::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	BasePose.Initialize(Context);
}

void FAnimNode_SetBonesByName::CacheBones(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FAnimNode_SetBonesByName::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
}


void FAnimNode_SetBonesByName::Evaluate_AnyThread(FPoseContext& Output)
{
	// Evaluate the input
	BasePose.Evaluate(Output);
	const FBoneContainer& RequiredBones = Output.Pose.GetBoneContainer();
	for (TPair<FName, FTransform> BoneTransform: BoneTransforms)
	{
		const int32 PoseIndex = RequiredBones.GetPoseBoneIndexForBoneName(BoneTransform.Key);
		
		Output.Pose[static_cast<FCompactPoseBoneIndex>(PoseIndex)] = BoneTransform.Value;
		
		//Output.Pose.GetComponentSpaceTransform()
	}
	
}

void FAnimNode_SetBonesByName::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);


	DebugData.AddDebugItem(DebugLine);

	BasePose.GatherDebugData(DebugData);
}

bool FAnimNode_SetBonesByName::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones)
{
	return BoneTransforms.Num() > 0;
}

void FAnimNode_SetBonesByName::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output,
                                                                TArray<FBoneTransform>& OutBoneTransforms)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(EvaluateSkeletalControl_AnyThread)
	FAnimNode_SkeletalControlBase::EvaluateSkeletalControl_AnyThread(Output, OutBoneTransforms);
	FBoneContainer BoneContainer = Output.Pose.GetPose().GetBoneContainer();
	// BoneTransforms.KeySort()
	for(TPair<FName, FTransform> BoneTransform : BoneTransforms)
	{
		FBoneReference BoneReference = FBoneReference(BoneTransform.Key);
		if(!BoneReference.IsValidToEvaluate(BoneContainer)) continue;
		// Output.Pose.GetComponentSpaceTransform(BoneReference.GetCompactPoseIndex(BoneContainer));
		//Output.Pose.SetComponentSpaceTransform(BoneReference.GetCompactPoseIndex(BoneContainer), BoneTransform.Value);
	}
}
