// Fill out your copyright notice in the Description page of Project Settings.


#include "NxLiveLinkRetargetAsset.h"

#include <complex>

#include "Retarget.h"
#include <mftransform.h>
#include "DrawDebugHelpers.h"

#include "SkeletonDefinition.h"


DEFINE_LOG_CATEGORY(LogNxLLRetarget)

UNxLiveLinkRetargetAsset::UNxLiveLinkRetargetAsset(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	m_remap_bones_names{ {"Root", ENxXsensMapping::Root}, {"Pelvis", ENxXsensMapping::Pelvis}, {"L5", ENxXsensMapping::L5}, {"L3", ENxXsensMapping::L3}, {"T12", ENxXsensMapping::T12}, {"T8", ENxXsensMapping::T8}, {"Neck", ENxXsensMapping::Neck}, {"Head", ENxXsensMapping::Head},
						{"RightShoulder", ENxXsensMapping::RightShoulder}, {"RightUpperArm", ENxXsensMapping::RightUpperArm},{"RightForeArm", ENxXsensMapping::RightForeArm},{"RightHand", ENxXsensMapping::RightHand},
						{"LeftShoulder", ENxXsensMapping::LeftShoulder}, {"LeftUpperArm", ENxXsensMapping::LeftUpperArm},{"LeftForeArm", ENxXsensMapping::LeftForeArm},{"LeftHand", ENxXsensMapping::LeftHand},
						{"RightUpperLeg", ENxXsensMapping::RightUpperLeg},{"RightLowerLeg", ENxXsensMapping::RightLowerLeg},{"RightFoot", ENxXsensMapping::RightFoot},{"RightToe", ENxXsensMapping::RightToe},
						{"LeftUpperLeg", ENxXsensMapping::LeftUpperLeg},{"LeftLowerLeg", ENxXsensMapping::LeftLowerLeg},{"LeftFoot", ENxXsensMapping::LeftFoot},{"LeftToe", ENxXsensMapping::LeftToe},
						{"Prop1", ENxXsensMapping::Prop1},{"Prop2", ENxXsensMapping::Prop2},{"Prop3", ENxXsensMapping::Prop3},{"Prop4", ENxXsensMapping::Prop4},
						{"LeftCarpus", ENxXsensMapping::LeftCarpus},{"LeftFirstMC", ENxXsensMapping::LeftFirstMC},{"LeftFirstPP", ENxXsensMapping::LeftFirstPP},{"LeftFirstDP", ENxXsensMapping::LeftFirstDP},
						{"LeftSecondMC", ENxXsensMapping::LeftSecondMC},{"LeftSecondPP", ENxXsensMapping::LeftSecondPP},{"LeftSecondMP", ENxXsensMapping::LeftSecondMP,},{"LeftSecondDP", ENxXsensMapping::LeftSecondDP},
						{"LeftThirdMC", ENxXsensMapping::LeftThirdMC},{"LeftThirdPP", ENxXsensMapping::LeftThirdPP},{"LeftThirdMP", ENxXsensMapping::LeftThirdMP},{"LeftThirdDP", ENxXsensMapping::LeftThirdDP},
						{"LeftFourthMC", ENxXsensMapping::LeftFourthMC},{"LeftFourthPP", ENxXsensMapping::LeftFourthPP},{"LeftFourthMP", ENxXsensMapping::LeftFourthMP},{"LeftFourthDP", ENxXsensMapping::LeftFourthDP},
						{"LeftFifthMC", ENxXsensMapping::LeftFifthMC},{"LeftFifthPP", ENxXsensMapping::LeftFifthPP},{"LeftFifthMP", ENxXsensMapping::LeftFifthMP},{"LeftFifthDP", ENxXsensMapping::LeftFifthDP},
						{"RightCarpus", ENxXsensMapping::RightCarpus},{"RightFirstMC", ENxXsensMapping::RightFirstMC},{"RightFirstPP", ENxXsensMapping::RightFirstPP},{"RightFirstDP", ENxXsensMapping::RightFirstDP},
						{"RightSecondMC", ENxXsensMapping::RightSecondMC},{"RightSecondPP", ENxXsensMapping::RightSecondPP},{"RightSecondMP", ENxXsensMapping::RightSecondMP},{"RightSecondDP", ENxXsensMapping::RightSecondDP},
						{"RightThirdMC", ENxXsensMapping::RightThirdMC},{"RightThirdPP", ENxXsensMapping::RightThirdPP},{"RightThirdMP", ENxXsensMapping::RightThirdMP},{"RightThirdDP", ENxXsensMapping::RightThirdDP},
						{"RightFourthMC", ENxXsensMapping::RightFourthMC},{"RightFourthPP", ENxXsensMapping::RightFourthPP},{"RightFourthMP", ENxXsensMapping::RightFourthMP},{"RightFourthDP", ENxXsensMapping::RightFourthDP},
						{"RightFifthMC", ENxXsensMapping::RightFifthMC},{"RightFifthPP", ENxXsensMapping::RightFifthPP},{"RightFifthMP", ENxXsensMapping::RightFifthMP},{"RightFifthDP", ENxXsensMapping::RightFifthDP}, },
	m_retarget(100),
	m_doLog(false)
{
}

void UNxLiveLinkRetargetAsset::BuildPoseFromAnimationData(float DeltaTime,
	const FLiveLinkSkeletonStaticData* InSkeletonData, const FLiveLinkAnimationFrameData* InFrameData,
	FCompactPose& OutPose)
{
	Super::BuildPoseFromAnimationData(DeltaTime, InSkeletonData, InFrameData, OutPose);

	bool logValid = false;
	std::map<int, int> parentOverride;

	auto TransformedBoneNames = PopulateBoneNames(InSkeletonData);

	FVector uniformScale = OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[0].GetScale3D();

	if(m_retarget++ >= 100 || (InFrameData->Transforms.Num() != m_mvnToUnrealTpose.Num()))
	{
		FBlendedCurve OutCurve;
		CalculateTposeValues(OutPose, *InSkeletonData, *InFrameData, OutCurve);
		m_retarget = 0;
	}

	TArray<FTransform> SegData;
	for(int32 i = 0; i < InFrameData->Transforms.Num(); ++i)
	{
		FName BoneName = TransformedBoneNames[i];

		FTransform BoneTransform = InFrameData->Transforms[i];
		int parent = 0;
		SegData.Add(BoneTransform);
		const FName MapBoneName = *BoneNameMap.FindKey(BoneName);

		//set translation and rotation for the pelvis
		if(MapBoneName == "Pelvis")
		{
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			//UE_LOG(LogNxLLRetarget, Log, TEXT("%d Bone index = %d"), i, boneIndex);
			if(boneIndex >= 0)
			{
				//scale the pelvis to the correct height with the Xsens data and Unreal skeleton
				float scale = CalculateVectorScale(BoneTransform.GetScale3D(), m_tposeWorld[boneIndex].GetTranslation());
				if(isinf(scale))
					scale = CalculateVectorScale(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetTranslation(), m_tposeWorld[boneIndex].GetTranslation());
				//Calculate the pelvis rotation using the mvn and tpose rotation
				SegData[i].SetRotation(BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation());
				//scale the position so the pelvis is at the correct height
				SegData[i].SetTranslation(BoneTransform.GetTranslation() * scale);
				SegData[i].SetScale3D(uniformScale);
				//UE_LOG(LogNxLLRetarget, Log, TEXT("final dpos (%f,%f,%f)"), SegData[i].GetTranslation()[0], SegData[i].GetTranslation()[1], SegData[i].GetTranslation()[2]);
				BoneTransform = SegData[i];
			}
		}
		else if(i > 23 && i < 29 && MapBoneName.ToString().Contains("Prop"))
		{
			//UE_LOG(LogNxLLRetarget, Log, TEXT("Prop"));
			//for all props
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			if(boneIndex >= 0)
			{
				//find the parent bone
				auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(boneIndex);
				FName parentBoneName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(parentBoneIndex);

				const FName* TargetBoneName = BoneNameMap.FindKey(parentBoneName);
				// if the parent is not in the BoneNameMap (retargeted) find the parent of the parent
				while(!TargetBoneName && parentBoneIndex >= 0)
				{
					parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(parentBoneIndex);
					if(parentBoneIndex >= 0)
					{
						parentBoneName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(parentBoneIndex);
						TargetBoneName = BoneNameMap.FindKey(parentBoneName);
					}
				}
				if(parentBoneIndex >= 0)
				{
					parent = NxSkeletonDefinition::XsensBoneNames.Find(*TargetBoneName);
					FQuat drot = BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation();
					drot = SegData[parent].GetRotation().Inverse() * drot;
					BoneTransform.SetRotation(drot);
					BoneTransform.SetTranslation(m_mvnToUnrealTpose[i].GetTranslation());
					BoneTransform.SetScale3D(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetScale3D());
				}
			}
		}
		else if(i > 0)
		{
			//if an Xsens bone is not mapped to a bone from the current character find a parent that is for the child to use
			if(BoneName == "None")
			{
				parent = NxSkeletonDefinition::XsensParentIndex[i];
				int cur = parent;
				FName parentBoneName = *BoneNameMap.Find(NxSkeletonDefinition::XsensBoneNames[parent]);
				while(parentBoneName == "None")
				{
					cur = parent;
					parent = NxSkeletonDefinition::XsensParentIndex[parent];
					parentBoneName = *BoneNameMap.Find(NxSkeletonDefinition::XsensBoneNames[parent]);
				}
				auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(parentBoneName);

				parentOverride[i] = cur;
			}
			else if(BoneName != "p1" && BoneName != "p2" && BoneName != "p3" && BoneName != "p4" && m_mvnToUnrealTpose.Num() > i)
			{
				//xsens parent
				parent = NxSkeletonDefinition::XsensParentIndex[i];// -1] + 1;
				//character bone index
				auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
				if(boneIndex >= 0)
				{
					//character parent bone index
					auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(boneIndex);
					//UE_LOG(LogNxLLRetarget, Log, TEXT("%d Bone index = %d, parent = %d, parentBoneIndex = %d"), i, boneIndex, parent, parentBoneIndex);

					//if parent is not mapped override it with a parent that is
					auto it = parentOverride.find(parent);
					if(it != parentOverride.end())
					{
						parent = it->second;
						//UE_LOG(LogNxLLRetarget, Log, TEXT("%d Overriding parent index with %d"), i, parent);
					}
					/*
					UE_LOG(LogNxLLRetarget, Log, TEXT("Xsens Tpose pos (%f,%f,%f), parent Tpose pos (%f,%f,%f)"),
						BoneTransform.GetScale3D()[0], BoneTransform.GetScale3D()[1], BoneTransform.GetScale3D()[2],
						InFrameData->Transforms[parent].GetScale3D()[0], InFrameData->Transforms[parent].GetScale3D()[1], InFrameData->Transforms[parent].GetScale3D()[2]);
					*/
					//combine the tpose and mvn rotation
					FQuat drot = BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation();
					SegData[i].SetRotation(drot);
					// remove the rotation of the parent from the current segment rotation before applying
					drot = SegData[parent].GetRotation().Inverse() * drot;

					BoneTransform.SetRotation(drot);
					BoneTransform.SetTranslation(m_mvnToUnrealTpose[i].GetTranslation());
					BoneTransform.SetScale3D(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetScale3D());
				}
				else
				{
					//UE_LOG(LogNxLLRetarget, Log, TEXT("%d Bone index = %d, parent = %d"), i, boneIndex, parent);
				}
			}
		}
		else
		{
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			//UE_LOG(LogNxLLRetarget, Log, TEXT("%d Bone index = %d"), i, boneIndex);
			if(boneIndex >= 0 && m_tposeWorld.Num() > 0)
			{
				/*
				UE_LOG(LogNxLLRetarget, Log, TEXT("Tpose pos (%f,%f,%f) ori (%f,%f,%f,%f)"),
					m_tposeWorld[boneIndex].GetTranslation()[0], m_tposeWorld[boneIndex].GetTranslation()[1], m_tposeWorld[boneIndex].GetTranslation()[2],
					m_tposeWorld[boneIndex].GetRotation().W, m_tposeWorld[boneIndex].GetRotation().X, m_tposeWorld[boneIndex].GetRotation().Y, m_tposeWorld[boneIndex].GetRotation().Z);
				*/
			}
		}

		int32 MeshIndex = OutPose.GetBoneContainer().GetPoseBoneIndexForBoneName(BoneName);

		if(MeshIndex != INDEX_NONE)
		{
			FCompactPoseBoneIndex CPIndex = OutPose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(MeshIndex));
			if(CPIndex != INDEX_NONE)
			{
				OutPose[CPIndex] = BoneTransform;
			}
		}
	}
	
	//DebugDrawLiveLink(InFrameData, FColor::Black, 0);
	//DebugDrawPose(OutPose, FColor::Orange, 1);
	
	IKSpaceTransferLimbs(InFrameData, OutPose, SegData);

	const TArray<FName>& SourceCurveNames = InSkeletonData->PropertyNames;
	TArray<FName, TMemStackAllocator<>> TransformedCurveNames;
	TransformedCurveNames.Reserve(SourceCurveNames.Num());

	for(const FName& SrcCurveName : SourceCurveNames)
	{
		FName* TargetCurveName = CurveNameMap.Find(SrcCurveName);
		if(TargetCurveName == nullptr)
		{
			FName NewName = SrcCurveName;
			TransformedCurveNames.Add(NewName);
			CurveNameMap.Add(SrcCurveName, NewName);
		}
		else
		{
			TransformedCurveNames.Add(*TargetCurveName);
		}
	}

	if(logValid)
		m_doLog = false;
}

void UNxLiveLinkRetargetAsset::DebugDrawPose(FCompactPose& OutPose, FColor Color, float Thickness, bool DrawAxes, float AxesLength) const
{
	//Pose transforms are local space
	
	for (int i = 0; i < OutPose.GetBoneContainer().GetNumBones(); i++)
	{
		FCompactPoseBoneIndex BoneIndex = static_cast<FCompactPoseBoneIndex>(i);
		if (!OutPose.IsValidIndex(BoneIndex)) continue;
		FTransform BoneTransform = GetWorldPoseTransform(BoneIndex, OutPose);
		FTransform ParentTransform = GetWorldPoseTransform(OutPose.GetParentBoneIndex(BoneIndex), OutPose);
				
		DrawDebugLine(GetWorld(), BoneTransform.GetTranslation(), ParentTransform.GetTranslation(),
			Color, false, 0, 0, Thickness);
		if(DrawAxes)
		{
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetRightVector() * AxesLength * BoneTransform.GetScale3D().X,
				FColor::Red, false, 0, 0, Thickness * 0.2);
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetForwardVector() * AxesLength * BoneTransform.GetScale3D().Y,
				FColor::Green, false, 0, 0, Thickness * 0.2);
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetUpVector() * AxesLength * BoneTransform.GetScale3D().Z,
				FColor::Blue, false, 0, 0, Thickness * 0.2);
		}
	}
}

void UNxLiveLinkRetargetAsset::DebugDrawLiveLink(const FLiveLinkAnimationFrameData* FrameData, const FColor Color,
	const float Thickness, bool DrawAxes, const float AxesLength) const
{
	//Live Link frame data is in world space
	for (int i = 0; i < FrameData->Transforms.Num(); i++)
	{
		FTransform BoneTransform = GetWorldLiveLinkTransform(static_cast<ENxXsensMapping>(i), FrameData);
		int32 ParentIndex = NxSkeletonDefinition::XsensParentIndex[i];
		FTransform ParentTransform = GetWorldLiveLinkTransform(static_cast<ENxXsensMapping>(ParentIndex), FrameData);
		
		
		DrawDebugLine(GetWorld(),
			BoneTransform.GetTranslation(),
			ParentTransform.GetTranslation(),
			Color, false, 0, 0, Thickness);

		if(DrawAxes)
		{
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetRightVector() * AxesLength,
				FColor::Red, false, 0, 0, Thickness * 0.2);
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetForwardVector() * AxesLength,
				FColor::Green, false, 0, 0, Thickness  * 0.2);
			DrawDebugLine(GetWorld(),
				BoneTransform.GetTranslation(),
				BoneTransform.GetTranslation() + BoneTransform.GetRotation().GetUpVector() * AxesLength,
				FColor::Blue, false, 0, 0, Thickness * 0.2);
		}
	}
}

void UNxLiveLinkRetargetAsset::IKSpaceTransferLimbs(const FLiveLinkAnimationFrameData* FrameData, FCompactPose& OutPose, TArray<FTransform>& SegData)
{
/*
	FTransform SourceChest = GetWorldLiveLinkTransform(ENxXsensMapping::T8, FrameData);
	FTransform SourceRightShoulder = GetWorldLiveLinkTransform(ENxXsensMapping::RightShoulder, FrameData);
	FTransform SourceRightUpperArm = GetWorldLiveLinkTransform(ENxXsensMapping::RightUpperArm, FrameData);
	FTransform SourceRightForeArm = GetWorldLiveLinkTransform(ENxXsensMapping::RightForeArm, FrameData);
	FTransform SourceRightHand = GetWorldLiveLinkTransform(ENxXsensMapping::RightHand, FrameData);

	FTransform TargetChest = GetWorldPoseTransform(ENxXsensMapping::T8, OutPose);
	FTransform TargetRightShoulder = GetWorldPoseTransform(ENxXsensMapping::RightShoulder, OutPose);
	FTransform TargetRightUpperArm = GetWorldPoseTransform(ENxXsensMapping::RightUpperArm, OutPose);
	FTransform TargetRightForeArm = GetWorldPoseTransform(ENxXsensMapping::RightForeArm, OutPose);
	FTransform TargetRightHand = GetWorldPoseTransform(ENxXsensMapping::RightHand, OutPose);

	FVector RightHandPositionRelativeToSource = SourceChest.ToMatrixNoScale().InverseTransformPosition(SourceRightHand.GetTranslation());
	FVector ScaledRightHandPosition = RightHandPositionRelativeToSource;// * ArmRatio;
	FVector RotatedRightHandPosition = m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::L3)].GetRotation().Inverse().RotateVector(ScaledRightHandPosition);
	FVector RightHandIKTarget = TargetChest.ToMatrixNoScale().TransformPosition(
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::L3)].GetRotation().Inverse().RotateVector(
			SourceChest.ToMatrixNoScale().InverseTransformPosition(
				SourceRightHand.GetTranslation()) * ArmRatio));

	FTransform SourceLeftHand = GetWorldLiveLinkTransform(ENxXsensMapping::LeftHand, FrameData);
	FVector LeftHandPositionRelativeToSource = SourceChest.ToMatrixNoScale().InverseTransformPosition(SourceLeftHand.GetTranslation());
	FVector ScaledLeftHandPosition = LeftHandPositionRelativeToSource;// * ArmRatio;
	FVector RotatedLeftHandPosition = m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::L3)].GetRotation().Inverse().RotateVector(ScaledLeftHandPosition);
	
    
    //TArray<FTransform> IntermediateTransforms = FrameData->Transforms;
	std::map<FName, ENxXsensMapping, FNameFastLess>::iterator it = m_remap_bones_names.begin();
*/
	// Create Skeleton from OutPose
	UNxSkeletonBase* PoseSkeleton = NewObject<UNxSkeletonBase>();
	TArray<int32> PoseToSkeletonBoneIndexArray = OutPose.GetBoneContainer().GetPoseToSkeletonBoneIndexArray();
	TMap<FName, FTransform> PoseBoneTransforms;
	TMap<FName, FName> PoseParentsMap;
	
	for(int32 i = 0; i < OutPose.GetNumBones(); i++){
	    int32 BoneIndex = PoseToSkeletonBoneIndexArray[i];
	    FName BoneName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(BoneIndex);
	    int32 PoseParentIndex = OutPose.GetBoneContainer().GetParentBoneIndex(i);
	    if(!(PoseParentIndex < 0)){
            int32 ParentIndex = PoseToSkeletonBoneIndexArray[PoseParentIndex];
            FName ParentName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(ParentIndex);
            PoseParentsMap.Add(BoneName, ParentName);
	    }
	    FTransform BoneTransform = OutPose.GetBones()[i];
    	PoseBoneTransforms.Add(BoneName, BoneTransform);
	}
	
	PoseSkeleton->Init(PoseBoneTransforms, PoseParentsMap);
	PoseSkeleton->DrawDebug(GetWorld(), FColor::Red, 0.25, -1, false, 1);
	
	// Create Skeleton from live link data
	UNxSkeletonXsens* MocapSkeleton = NewObject<UNxSkeletonXsens>();	
	MocapSkeleton->Init(FrameData->Transforms, World);
	MocapSkeleton->DrawDebug(GetWorld(),FColor::Black, 1, -1, false, 1);
	
	TArray<FTransform> MocapLocalTransforms;
	MocapLocalTransforms.Reserve(FrameData->Transforms.Num());
	
	for (int32 i = 0; i < FrameData->Transforms.Num(); i++)
	{
		// int32 ParentIndex = MocapSkeleton->GetParentIndex(i);
		FTransform LocalTransform = MocapSkeleton->GetJointLocalTransform(i);
		LocalTransform.SetTranslation(LocalTransform.GetTranslation()*LocalTransform.GetScale3D());
		LocalTransform.SetScale3D(FVector::OneVector);
		MocapLocalTransforms.Add(LocalTransform);
	}

	// Create skeleton using Live Link Bones then scale to match remapped OutPose bone lengths
	// So we have a skeleton with the same rotation as the live link
	UNxSkeletonXsens* IntermediateSkeletonXsens = NewObject<UNxSkeletonXsens>();
	IntermediateSkeletonXsens->Init(MocapLocalTransforms);
	
	TMap<FName, FTransform> RemappedTransforms;
	RemappedTransforms.Reserve(MocapSkeleton->GetLength());
	TArray<FName> RemappedNames;
	RemappedNames.Reserve(MocapSkeleton->GetLength());
	TArray<FTransform> ScaledTransforms = MocapSkeleton->GetJointLocalTransforms();
	
	for(int32 i = 0; i < MocapSkeleton->GetLength(); i++)
	{
		FName MocapName = MocapSkeleton->GetJointName(i);
		FName RemappedName = GetRemappedBoneName(MocapName);
		RemappedNames.Add(RemappedName);
		FName MocapParentName = MocapSkeleton->GetParentName(MocapName);
		FName RemappedParentName = GetRemappedBoneName(MocapParentName);
		while (RemappedParentName.IsNone())
		{
			MocapParentName = MocapSkeleton->GetParentName(MocapParentName);
			if(MocapParentName.IsNone()) break;
			RemappedParentName = GetRemappedBoneName(MocapParentName);
		}

		FTransform PoseWorldTransform = PoseSkeleton->GetJointWorldTransform(RemappedName);
		FTransform PoseParentWorldTransform = PoseSkeleton->GetJointWorldTransform(RemappedParentName);
		float PoseLength = FTransform(PoseWorldTransform.ToMatrixNoScale() * PoseParentWorldTransform.ToMatrixNoScale().Inverse()).GetTranslation().Size();
		float MocapLength = MocapSkeleton->GetJointLocalTranslation(i).Size();
		float Ratio = 1.0;
		if(MocapLength != 0) Ratio = PoseLength / MocapLength;
		if(RemappedName.IsNone()) Ratio = 0;
		ScaledTransforms[i].ScaleTranslation(Ratio);
	}
	
	UNxSkeletonBase* IntermediateRemappedSkeleton = NewObject<UNxSkeletonBase>();
	IntermediateRemappedSkeleton->Init(ScaledTransforms, RemappedNames, MocapSkeleton->GetJointParentsArray(), Local);
	IntermediateRemappedSkeleton->SetJointWorldTranslation(
		static_cast<int32>(ENxXsensMapping::Pelvis),
		GetWorldPoseTransform(ENxXsensMapping::Pelvis, OutPose).GetTranslation());
	IntermediateRemappedSkeleton->DrawDebug(GetWorld(), FColor::Blue, 1, -1, true, 2);

	// Retarget 
	FQuat RightUpperArmRotationWS;
	FQuat RightForeArmRotationLS;
	FQuat RightHandRotationWS;
	/*
		MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::T8)),
		MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightShoulder)),
		MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightUpperArm)),
		MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightForeArm)),
		MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightHand)),
	*/
	bool IKSolutionFoundRightHand = Retarget::RetargetHand(
		GetWorldLiveLinkTransform(ENxXsensMapping::T8, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::RightShoulder, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::RightUpperArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::RightForeArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::RightHand, FrameData),
		
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::T8)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightShoulder)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightUpperArm)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightForeArm)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightHand)),
		
		RightUpperArmRotationWS, RightForeArmRotationLS, RightHandRotationWS,
		FQuat::Identity, FQuat::Identity,
		RightHandKalman, ArmRatio, FVector::UpVector, false, 0.01, GetWorld(), FColor::Red);
	
	if(!IKSolutionFoundRightHand) UE_LOG(LogNxLLRetarget, Log, TEXT("Failed to find IK solution for Right Hand"));
	
	//UE_LOG(LogNxLLRetarget, Log, TEXT("\nUpperArmQ: %s\nUpperArmE: %s"), *RightUpperArmRotationWS.ToString(), *RightUpperArmRotationWS.Euler().ToString());

	// Set rotations into intermediate skeleton
	IntermediateRemappedSkeleton->SetJointWorldRotation(static_cast<int32>(ENxXsensMapping::RightUpperArm), RightUpperArmRotationWS);
	IntermediateRemappedSkeleton->SetJointLocalRotation(static_cast<int32>(ENxXsensMapping::RightForeArm), RightForeArmRotationLS);
	IntermediateRemappedSkeleton->SetJointWorldRotation(static_cast<int32>(ENxXsensMapping::RightHand), RightHandRotationWS);


	
	//OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)]
	/*
	auto RightUpperArmParentIndex = OutPose.GetParentBoneIndex(XsensBoneToPoseIndex(ENxXsensMapping::RightUpperArm, OutPose));
	FTransform RightUpperArmParentTransform = GetWorldPoseTransform(RightUpperArmParentIndex, OutPose);

	// Testing just a straight map from the input livelink data and multiplying it by the cached pose offset rotation
	// Just as is done above as far as I can see, but results in the wrong rotation.
	FQuat RightUpperArmRotationLS = MocapSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightUpperArm)).GetRotation() *
		m_mvnToUnrealTpose[XsensBoneToPoseIndex(ENxXsensMapping::RightUpperArm, OutPose).GetInt()].GetRotation();
	RightUpperArmRotationLS = MocapSkeleton->GetJointLocalTransform(
		static_cast<int32>(ENxXsensMapping::RightShoulder)).GetRotation().Inverse() * RightUpperArmRotationLS;
	*/
	
	// Left Hand
	FQuat LeftUpperArmRotationWS;
	FQuat LeftForeArmRotationLS;
	FQuat LeftHandRotationWS;

	bool IKSolutionFoundLeftHand = Retarget::RetargetHand(
		GetWorldLiveLinkTransform(ENxXsensMapping::T8, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftShoulder, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftUpperArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftForeArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftHand, FrameData),
		
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::T8)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::LeftShoulder)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::LeftUpperArm)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::LeftForeArm)),
		IntermediateRemappedSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::LeftHand)),
		
		LeftUpperArmRotationWS, LeftForeArmRotationLS, LeftHandRotationWS,
		FQuat::Identity, FQuat::Identity,
		LeftHandKalman, ArmRatio, FVector::UpVector, false, 0.01, GetWorld(), FColor::Red);
	
	if(!IKSolutionFoundLeftHand) UE_LOG(LogNxLLRetarget, Log, TEXT("Failed to find IK solution for Left Hand"));
	
	//UE_LOG(LogNxLLRetarget, Log, TEXT("\nUpperArmQ: %s\nUpperArmE: %s"), *LeftUpperArmRotationWS.ToString(), *LeftUpperArmRotationWS.Euler().ToString());

	// Set rotations into intermediate skeleton
	IntermediateRemappedSkeleton->SetJointWorldRotation(static_cast<int32>(ENxXsensMapping::LeftUpperArm), LeftUpperArmRotationWS);
	IntermediateRemappedSkeleton->SetJointLocalRotation(static_cast<int32>(ENxXsensMapping::LeftForeArm), LeftForeArmRotationLS);
	IntermediateRemappedSkeleton->SetJointWorldRotation(static_cast<int32>(ENxXsensMapping::LeftHand), LeftHandRotationWS);

	IntermediateRemappedSkeleton->DrawDebug(GetWorld(), FColor::Green, 1, -1, true, 2);

	for(int32 i = 2; i < IntermediateRemappedSkeleton->GetLength(); i++)
	{
		FQuat TposeRot = m_mvnToUnrealTpose[i].GetRotation();
		FQuat IntermediateRot = IntermediateRemappedSkeleton->GetJointWorldTransform(i).GetRotation();
		FName JointName = IntermediateRemappedSkeleton->GetJointName(i);
		PoseSkeleton->SetJointWorldRotation(JointName, IntermediateRot*TposeRot);
	}
	PoseSkeleton->DrawDebug(GetWorld(), FColor::Cyan, 1, -1, true, 1);

	for(int32 i = 0; i<PoseSkeleton->GetLength(); i++)
	{
		OutPose[static_cast<FCompactPoseBoneIndex>(i)].SetRotation(PoseSkeleton->GetJointLocalTransform(i).GetRotation());
	}
	
	/*
	// FTransform BoneTransform = FrameData->Transforms[static_cast<int32>(ENxXsensMapping::RightUpperArm)];
	FCompactPoseBoneIndex RightUpperArmPoseIndex = XsensBoneToPoseIndex(ENxXsensMapping::RightUpperArm, OutPose);
	FQuat drot = RightUpperArmRotationWS * m_mvnToUnrealTpose[RightUpperArmPoseIndex.GetInt()].GetRotation();
	SegData[RightUpperArmPoseIndex.GetInt()].SetRotation(drot);
	
	FCompactPoseBoneIndex RightShoulderPoseIndex = XsensBoneToPoseIndex(ENxXsensMapping::RightShoulder, OutPose);
	// remove the rotation of the parent from the current segment rotation before applying
	drot = SegData[RightShoulderPoseIndex.GetInt()].GetRotation().Inverse() * drot;
	OutPose[RightUpperArmPoseIndex].SetRotation(drot);
	*/
	// FQuat parentdrot = RightUpperArmRotationWS * m_mvnToUnrealTpose[PoseIndex].GetRotation();
	// drot = MocapSkeleton->GetJointLocalTransform(static_cast<int32>(ENxXsensMapping::RightShoulder)).GetRotation().Inverse() * drot;
	// BoneTransform.SetRotation(drot);
	// Set Right Upper Arm LS
	//OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightUpperArm, OutPose)].SetRotation(RightUpperArmRotationLS);

	// Set Right Fore Arm LS
	/*
	OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].SetRotation(
		GetWorldLiveLinkTransform(ENxXsensMapping::RightForeArm, FrameData).GetRotation() *
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::RightForeArm)].GetRotation()
		);
	*/
	//OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].SetRotation(OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].GetRotation());
	FTransform PoseRightForeArm = PoseSkeleton->GetJointLocalTransform(GetRemappedBoneName(NxSkeletonDefinition::XsensBoneNames[static_cast<int32>(ENxXsensMapping::RightForeArm)]));
	
	// UE_LOG(LogNxLLRetarget, Log, TEXT("%d"), XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose).GetInt());
	//FTransform PoseRightForeArm = OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)];
	/*
	DrawDebugLine(GetWorld(), FVector::ZeroVector, PoseRightForeArm.GetTranslation(),
		FColor::Black, false, -1, 0, 10);
	
	DrawDebugLine(GetWorld(),
		PoseRightForeArm.GetTranslation(),
		PoseRightForeArm.GetTranslation() + PoseRightForeArm.GetRotation().GetRightVector() * 10,
		FColor::Red, false, -1, 0, 0);
	DrawDebugLine(GetWorld(),
		PoseRightForeArm.GetTranslation(),
		PoseRightForeArm.GetTranslation() + PoseRightForeArm.GetRotation().GetForwardVector() * 10,
		FColor::Green, false, -1, 0, 0);
	DrawDebugLine(GetWorld(),
		PoseRightForeArm.GetTranslation(),
		PoseRightForeArm.GetTranslation() + PoseRightForeArm.GetRotation().GetUpVector() * 10,
		FColor::Blue, false, -1, 0,  0);
	*/
	//OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].SetRotation(RightForeArmRotationLS *
	//	m_mvnToUnrealTpose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose).GetInt()].GetRotation());
	/*
	OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].SetRotation(
		PoseSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightForeArm)).GetRotation());
	*/
	/*
	OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightForeArm, OutPose)].SetRotation(
		PoseSkeleton->GetJointWorldTransform(static_cast<int32>(ENxXsensMapping::RightForeArm)).GetRotation());
	*/
	auto RightHandParentIndex = OutPose.GetParentBoneIndex(XsensBoneToPoseIndex(ENxXsensMapping::RightHand, OutPose));
	/*
	FQuat RightHandRotationLS = GetWorldPoseTransform(RightHandParentIndex, OutPose).InverseTransformRotation(RightHandRotationWS) *
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::RightHand)].GetRotation().Inverse();
	*/
	FQuat RightHandRotationLS = RightHandRotationWS *
			m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::RightHand)].GetRotation().Inverse();
	// Set Right Hand LS
	//OutPose[XsensBoneToPoseIndex(ENxXsensMapping::RightHand, OutPose)].SetRotation(RightHandRotationLS);

	/*
	 *
	FQuat LeftUpperArmRotationWS;
	FQuat LeftForeArmRotationLS;
	FQuat LeftHandRotationWS;
	Retarget::RetargetHand(GetWorldLiveLinkTransform(ENxXsensMapping::T8, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftShoulder, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftUpperArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftForeArm, FrameData),
		GetWorldLiveLinkTransform(ENxXsensMapping::LeftHand, FrameData),
		GetWorldPoseTransform(ENxXsensMapping::T8, OutPose),
		GetWorldPoseTransform(ENxXsensMapping::LeftShoulder, OutPose),
		GetWorldPoseTransform(ENxXsensMapping::LeftUpperArm, OutPose),
		GetWorldPoseTransform(ENxXsensMapping::LeftForeArm, OutPose),
		GetWorldPoseTransform(ENxXsensMapping::LeftHand, OutPose),
		LeftUpperArmRotationWS, LeftForeArmRotationLS, LeftHandRotationWS,
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::LeftShoulder)].GetRotation(),
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::L3)].GetRotation(),
		LeftHandKalman, ArmRatio, false, 0.01, GetWorld(), FColor::Green);
	
	auto LeftUpperArmParentIndex = OutPose.GetParentBoneIndex(XsensBoneToPoseIndex(ENxXsensMapping::LeftUpperArm, OutPose));
	FTransform LeftUpperArmParentTransform = GetWorldPoseTransform(LeftUpperArmParentIndex, OutPose);
	FQuat LeftUpperArmRotationLS = LeftUpperArmParentTransform.InverseTransformRotation(LeftUpperArmRotationWS) *
		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::LeftUpperArm)].GetRotation();
	// OutPose[XsensBoneToPoseIndex(ENxXsensMapping::LeftUpperArm, OutPose)].SetRotation(
//		m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::LeftForeArm)].GetRotation() * LeftUpperArmRotationLS);
	
	// OutPose[XsensBoneToPoseIndex(ENxXsensMapping::LeftForeArm, OutPose)].SetRotation(LeftForeArmRotationLS);
	
	auto LeftHandParentIndex = OutPose.GetParentBoneIndex(XsensBoneToPoseIndex(ENxXsensMapping::LeftHand, OutPose));
	FQuat LeftHandRotationLS = m_mvnToUnrealTpose[static_cast<int32>(ENxXsensMapping::LeftForeArm)].GetRotation() *
		GetWorldPoseTransform(LeftHandParentIndex, OutPose).InverseTransformRotation(LeftHandRotationWS);
	// OutPose[XsensBoneToPoseIndex(ENxXsensMapping::LeftHand, OutPose)].SetRotation(LeftHandRotationLS);
	*/
}

ENxXsensMapping UNxLiveLinkRetargetAsset::GetParent(ENxXsensMapping XsensBone) const
{
	const int32 Index = static_cast<int32>(XsensBone);
	if(!NxSkeletonDefinition::XsensParentIndex.IsValidIndex(Index)) return ENxXsensMapping::Root;
	return static_cast<ENxXsensMapping>(NxSkeletonDefinition::XsensParentIndex[Index]);
}

bool UNxLiveLinkRetargetAsset::HasParentXsens(ENxXsensMapping XsensBone) const
{
	return GetParent(XsensBone) != ENxXsensMapping::Root;
}

FTransform UNxLiveLinkRetargetAsset::GetLocalLiveLinkTransform(const ENxXsensMapping XsensBone,
                                                               const FLiveLinkAnimationFrameData* FrameData,
                                                               bool IdentityRotation) const
{
	FTransform WorldTransform = GetWorldLiveLinkTransform(XsensBone, FrameData, IdentityRotation);
	if(!HasParentXsens(XsensBone)) return WorldTransform;
	return WorldTransform * GetWorldLiveLinkTransform(GetParent(XsensBone), FrameData, IdentityRotation).Inverse();
}

FTransform UNxLiveLinkRetargetAsset::GetWorldLiveLinkTransform(ENxXsensMapping XsensBone,
	const FLiveLinkAnimationFrameData* FrameData,
	bool IdentityRotation) const
{
	//need to swap world and local for live link dats
	int32 Index = static_cast<int32>(XsensBone);
	if (!FrameData->Transforms.IsValidIndex(Index)) return FTransform::Identity;
	FTransform Transform = FrameData->Transforms[Index];
	if (IdentityRotation) Transform.SetRotation(FQuat::Identity);
	return Transform;
}

FCompactPoseBoneIndex UNxLiveLinkRetargetAsset::XsensBoneToPoseIndex(ENxXsensMapping XsensBone, FCompactPose& OutPose) const
{
	const int32 BoneIndex = static_cast<int32>(XsensBone);
	if(!NxSkeletonDefinition::XsensBoneNames.IsValidIndex(BoneIndex)) return FCompactPoseBoneIndex(-1);
	const FName BoneName = NxSkeletonDefinition::XsensBoneNames[BoneIndex];
	const FCompactPoseBoneIndex PoseIndex = static_cast<FCompactPoseBoneIndex>(
		OutPose.GetBoneContainer().GetPoseBoneIndexForBoneName(GetRemappedBoneName(BoneName)));
	return PoseIndex;
}

FTransform UNxLiveLinkRetargetAsset::GetLocalPoseTransform(ENxXsensMapping XsensBone, FCompactPose& OutPose) const
{
	const FCompactPoseBoneIndex PoseIndex = XsensBoneToPoseIndex(XsensBone, OutPose);
	if(!OutPose.IsValidIndex(PoseIndex)) return FTransform::Identity;
	return OutPose[PoseIndex];
}

FTransform UNxLiveLinkRetargetAsset::GetLocalPoseTransform(FCompactPoseBoneIndex PoseBoneIndex, FCompactPose& OutPose) const
{
	if(!OutPose.IsValidIndex(PoseBoneIndex)) return FTransform::Identity;
	return OutPose[PoseBoneIndex];
}

FTransform UNxLiveLinkRetargetAsset::GetLocalPoseTransform(FName BoneName, FCompactPose& OutPose) const
{
	int32 BoneIndex = OutPose.GetBoneContainer().GetPoseBoneIndexForBoneName(BoneName);
	if(BoneIndex < 0) return FTransform();

	return GetLocalPoseTransform(static_cast<FCompactPoseBoneIndex>(BoneIndex), OutPose);
}

FTransform UNxLiveLinkRetargetAsset::GetWorldPoseTransform(ENxXsensMapping XsensBone, FCompactPose& OutPose) const
{
	FCompactPoseBoneIndex BoneIndex = XsensBoneToPoseIndex(XsensBone, OutPose);
	return GetWorldPoseTransform(BoneIndex, OutPose);
	if(!OutPose.IsValidIndex(BoneIndex)) return FTransform::Identity;
	FCompactPoseBoneIndex ParentIndex = OutPose.GetBoneContainer().GetParentBoneIndex(BoneIndex);
	if(!OutPose.IsValidIndex(ParentIndex)) return GetLocalPoseTransform(XsensBone, OutPose);;
	return GetLocalPoseTransform(XsensBone, OutPose) * GetWorldPoseTransform(ParentIndex, OutPose);
}

FTransform UNxLiveLinkRetargetAsset::GetWorldPoseTransform(FCompactPoseBoneIndex PoseBoneIndex, FCompactPose& OutPose) const
{
	if (!OutPose.IsValidIndex(PoseBoneIndex))
	{
		return FTransform::Identity;
	}
	FTransform BoneTransform = OutPose[PoseBoneIndex];
	//BoneTransform = GetWorldPoseTransform(BoneIndex, OutPose);
	FCompactPoseBoneIndex CurrentIndex = PoseBoneIndex;
	while (!CurrentIndex.IsRootBone())
	{
		FCompactPoseBoneIndex ParentIndex = OutPose.GetBoneContainer().GetParentBoneIndex(CurrentIndex);
		if (!OutPose.IsValidIndex(ParentIndex))
		{
			break;
		}
		BoneTransform = BoneTransform * OutPose[ParentIndex];
			
		CurrentIndex = ParentIndex;
	}
	return BoneTransform;
}

void UNxLiveLinkRetargetAsset::BuildPoseAndCurveFromBaseData(float DeltaTime,
	const FLiveLinkBaseStaticData* InBaseStaticData, const FLiveLinkBaseFrameData* InBaseFrameData,
	FCompactPose& OutPose, FBlendedCurve& OutCurve)
{
	Super::BuildPoseAndCurveFromBaseData(DeltaTime, InBaseStaticData, InBaseFrameData, OutPose, OutCurve);
}

FName UNxLiveLinkRetargetAsset::GetRemappedBoneName(const FName& BoneName) const
{
	auto it = m_remap_bones_names.find(BoneName);

	if(it != m_remap_bones_names.end())
	{
		auto it_row = m_remapping_rows.Find(it->second);
		if(it_row)
			return it_row->RemapId;
	}

	return BoneName;
}
/*
FName UNxLiveLinkRetargetAsset::GetReversedRemappedBoneName(const ENxXsensMapping XsensBoneName) const {
    transformed_bone_names[XsensBoneNames]
    auto it = m_remap_bones_names.find(BoneName);
    
    	if(it != m_remap_bones_names.end())
    	{
    		auto it_row = m_remapping_rows.Find(it->second);
    		if(it_row)
    			return it_row->RemapId;
    	}
    
    	return BoneName;
}
*/

#if WITH_EDITOR
void UNxLiveLinkRetargetAsset::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	auto property_name = PropertyChangedEvent.GetPropertyName();

	//Only notify for these 2 properties
	if(property_name == "m_skeletal_mesh")
		OnSkeletalMeshChanged();
	else if(property_name == "m_remapping_table")
		OnRemappingTableChanged();
}
#endif

TArray<FName> UNxLiveLinkRetargetAsset::PopulateBoneNames(const FLiveLinkSkeletonStaticData* InSkeletonData)
{
	const auto& source_bone_names = InSkeletonData->GetBoneNames();

	TArray<FName> transformed_bone_names;
	transformed_bone_names.Reserve(source_bone_names.Num() + 4);  // Why + 4?

	for(const auto& bone_name : source_bone_names)
	{
		FName* TargetBoneName = BoneNameMap.Find(bone_name);
		if(TargetBoneName == nullptr)
		{
			FName new_name = GetRemappedBoneName(bone_name);
			transformed_bone_names.Add(new_name);
			BoneNameMap.Add(bone_name, new_name);
		}
		else
			transformed_bone_names.Add(*TargetBoneName);
	}

	return transformed_bone_names;
}

float UNxLiveLinkRetargetAsset::CalculateVectorScale(FVector xsensVec, FVector unrealVec)
{
	float xsensLength = xsensVec.Size();
	float unrealLength = unrealVec.Size();

	return (unrealLength / xsensLength);
}

inline FQuat UNxLiveLinkRetargetAsset::IsRotationFromVecToVec(FVector a, FVector b)
{
	a.Normalize();
	b.Normalize();

	return FQuat::FindBetweenNormals(a, b);
}

void UNxLiveLinkRetargetAsset::CalculateTposeValues(FCompactPose OutPose,
	const FLiveLinkSkeletonStaticData& InSkeletonData, const FLiveLinkAnimationFrameData& InFrameData,
	FBlendedCurve& OutCurve)
{
	bool logValid = false;
	std::map<int, int> parentOverride;

	auto TransformedBoneNames = PopulateBoneNames(&InSkeletonData);

	//get reference pose values
	TArray<FTransform> TPose = OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose();
	if(TPoseAnimation)
	{
		UE_LOG(LogNxLLRetarget, Log, TEXT("TPoseAnimation %s"), *TPoseAnimation->GetName());
		//when upgrading from UE4.23 to 4.26 api changed for GetAnimationPose
		//TPoseAnimation->GetAnimationPose(OutPose, OutCurve, FAnimExtractContext(0, true));
        FStackCustomAttributes attributes;
        FAnimationPoseData outAnimationPoseData(OutPose, OutCurve, attributes);
        TPoseAnimation->GetAnimationPose(outAnimationPoseData, FAnimExtractContext( 0, true ));
		TPose = OutPose.GetBones();
	}
	FVector uniformScale = OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[0].GetScale3D();
	//calculate the character tpose rotation and position in world space
	if(m_tposeWorld.Num() != 0 && m_tposeWorld.Num() != TPose.Num())
		return;
	m_tposeWorld = TPose;
	for (int32 i = 0; i < TPose.Num(); ++i)
	{
		if (m_tposeWorld.IsValidIndex(i))
		{
			int parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(i);
			if (m_tposeWorld.IsValidIndex(parentBoneIndex))
			{
				m_tposeWorld[i].SetRotation(m_tposeWorld[parentBoneIndex].GetRotation() * TPose[i].GetRotation());
				m_tposeWorld[i].SetTranslation(m_tposeWorld[parentBoneIndex].GetRotation().RotateVector(TPose[i].GetTranslation() * uniformScale) + m_tposeWorld[parentBoneIndex].GetTranslation());
			}
		}
	}
	
	// Calculate the ratio of each bone for use when creating the intermediate skeleton
	
	m_tposeWorld[static_cast<int32>(ENxXsensMapping::RightShoulder)];

	FName RightShoulderName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::RightShoulder)];
	auto RightShoulderIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RightShoulderName);
	FName RightUpperArmName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::RightUpperArm)];
	auto RightUpperArmIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RightUpperArmName);
	FName RightForeArmName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::RightForeArm)];
	auto RightForeArmIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RightForeArmName);
	FName RightHandName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::RightHand)];
	auto RightHandIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RightHandName);
	FName LeftUpperArmName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::LeftUpperArm)];
	auto LeftUpperArmIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(LeftUpperArmName);
	FName LeftHandName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::LeftHand)];
	auto LeftHandIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(LeftHandName);
	FName ChestName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::L3)];
	auto ChestIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(ChestName);
	FName PelvisName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::Pelvis)];
	auto PelvisIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(PelvisName);
	FName RootName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::Root)];
	auto RootIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RootName);
	FName RightFootName = TransformedBoneNames[static_cast<int32>(ENxXsensMapping::RightFoot)];
	auto RightFootIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(RightFootName);
	
	float TargetArmLength = FVector::Dist(
		m_tposeWorld[RightUpperArmIndex].GetTranslation(),
		m_tposeWorld[RightForeArmIndex].GetTranslation());
	TargetArmLength += FVector::Dist(
		m_tposeWorld[RightForeArmIndex].GetTranslation(),
		m_tposeWorld[RightHandIndex].GetTranslation());
	TargetArmLength += FVector::Dist(
		m_tposeWorld[RightUpperArmIndex].GetTranslation(),
		m_tposeWorld[RightShoulderIndex].GetTranslation());
	TargetArmLength += FMath::Abs((m_tposeWorld[ChestIndex].GetTranslation() - m_tposeWorld[RightShoulderIndex].GetTranslation()).Y);
	//TargetArmLength += FMath::Abs(m_tposeWorld[ChestIndex].GetTranslation().Y - m_tposeWorld[RightUpperArmIndex].GetTranslation().Y);
	
	float SourceArmLength = FVector::Dist(
		GetWorldLiveLinkTransform(ENxXsensMapping::RightHand, &InFrameData, true).GetTranslation(),
		GetWorldLiveLinkTransform(ENxXsensMapping::RightForeArm, &InFrameData, true).GetTranslation());
	SourceArmLength += FVector::Dist(
		GetWorldLiveLinkTransform(ENxXsensMapping::RightForeArm, &InFrameData, true).GetTranslation(), 
		GetWorldLiveLinkTransform(ENxXsensMapping::RightUpperArm, &InFrameData, true).GetTranslation());
	SourceArmLength += FVector::Dist(
		GetWorldLiveLinkTransform(ENxXsensMapping::RightUpperArm, &InFrameData, true).GetTranslation(), 
		GetWorldLiveLinkTransform(ENxXsensMapping::RightShoulder, &InFrameData, true).GetTranslation());
	SourceArmLength += FMath::Abs((
		GetWorldLiveLinkTransform(ENxXsensMapping::T8, &InFrameData, true).GetTranslation() - 
		GetWorldLiveLinkTransform(ENxXsensMapping::RightShoulder, &InFrameData, true).GetTranslation()).Y);
	//SourceArmLength += FMath::Abs(GetWorldLiveLinkTransform(ENxXsensMapping::L3, &InFrameData, true).GetTranslation().Y - GetWorldLiveLinkTransform(ENxXsensMapping::RightUpperArm, &InFrameData, true).GetTranslation().Y);
	ArmRatio = TargetArmLength / SourceArmLength;
	// UE_LOG(LogNxLLRetarget, Log, TEXT("Calculated Arm Ratio: %f = %f/%f"), ArmRatio, TargetArmLength, SourceArmLength);
	//ArmRatio = 1;
	// m_tposeWorld[static_cast<int32>(ENxXsensMapping::RightForeArm)];

	float SourceHipHeight = GetLocalLiveLinkTransform(ENxXsensMapping::Pelvis, &InFrameData).GetTranslation().Z; 
	float TargetHipHeight = (m_tposeWorld[PelvisIndex] * m_tposeWorld[RootIndex].Inverse()).GetTranslation().Z;
	HipsRatio = TargetHipHeight / SourceHipHeight;
	// UE_LOG(LogNxLLRetarget, Log, TEXT("Calculated Hips Ratio: %f = %f/%f"), HipsRatio, TargetHipHeight, SourceHipHeight);
	OriginalFootHeight = (GetWorldLiveLinkTransform(ENxXsensMapping::RightFoot, &InFrameData, true) * GetWorldLiveLinkTransform(ENxXsensMapping::Root, &InFrameData, true).Inverse() ).GetTranslation().Z; 
	float TargetFootHeight = (m_tposeWorld[RootIndex].Inverse() * m_tposeWorld[RightFootIndex]).GetTranslation().Z;
	FootHeightRatio = TargetFootHeight / OriginalFootHeight;
	// UE_LOG(LogNxLLRetarget, Log, TEXT("Calculated FootHeight Ratio: %f = %f/%f"), FootHeightRatio, TargetFootHeight, OriginalFootHeight);
	m_mvnToUnrealTpose.SetNum(InFrameData.Transforms.Num());
	for(int32 i = 0; i < InFrameData.Transforms.Num(); ++i)
	{
		FName BoneName = TransformedBoneNames[i];
		auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
		if(m_tposeWorld.IsValidIndex(boneIndex))
		{
			auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(boneIndex);
			m_mvnToUnrealTpose[i].SetRotation(m_tposeWorld[boneIndex].GetRotation());

			FVector xsensVec;
			FVector tposeVec;

			if(i > 0 && m_tposeWorld.IsValidIndex(parentBoneIndex))
			{
				m_mvnToUnrealTpose[i].SetTranslation(TPose[boneIndex].GetTranslation());
			}
			else
			{
				tposeVec = m_tposeWorld[boneIndex].GetTranslation();
				m_mvnToUnrealTpose[i].SetTranslation(tposeVec);
			}
			/*
			UE_LOG(LogNxLLRetarget, Log,
				TEXT("m_mvnToUnrealTpose (%f,%f,%f)"),
				m_mvnToUnrealTpose[i].GetTranslation()[0],
				m_mvnToUnrealTpose[i].GetTranslation()[1],
				m_mvnToUnrealTpose[i].GetTranslation()[2]);
			*/
		}
	}
}

void UNxLiveLinkRetargetAsset::OnSkeletalMeshChanged()
{
	if(m_remapping_table)
	{
		//clean the table and the rows
		if(m_skeletal_mesh == nullptr)
		{
			m_remapping_rows.Reset();
			m_remapping_table->EmptyTable();

			return;
		}

		AddBoneNamesToRemapTable();
	}
}

void UNxLiveLinkRetargetAsset::OnRemappingTableChanged()
{
	if(m_skeletal_mesh == nullptr)
	{
		m_remapping_rows.Reset();
		m_remapping_table->EmptyTable();
		return;
	}

	if(m_remapping_table != nullptr)
	{
		AddBoneNamesToRemapTable();
	}
	else
	{
		m_remapping_rows.Reset();
		return;
	}
}

void UNxLiveLinkRetargetAsset::AddBoneNamesToRemapTable()
{
	if(m_skeletal_mesh)
	{
		TArray<FName> bone_names;

		m_skeletal_mesh->RefSkeleton.GetNum();

		for(int i = 0; i < m_skeletal_mesh->RefSkeleton.GetNum(); ++i)
			bone_names.Add(m_skeletal_mesh->RefSkeleton.GetBoneName(i));

		auto row = FNxRemappingRow();

		for(auto& bone : bone_names)
			m_remapping_table->AddRow(bone, row);

		//Link each rows to the remapping table
		FNxRemappingRowHandle row_handle;
		row_handle.DataTable = m_remapping_table;

		for(int i = static_cast<int>(ENxXsensMapping::Root); i <= static_cast<int>(ENxXsensMapping::RightFifthDP); ++i)
			m_remapping_rows.Add(static_cast<ENxXsensMapping>(i), row_handle);

		//special case for Root
		m_remapping_table->AddRow("Reference", FNxRemappingRow());
		m_remapping_rows[ENxXsensMapping::Root].RemapId = "Reference";
	}
}

void UNxLiveLinkRetargetAsset::SetRemappingTable(UDataTable* DataTable)
{
	m_remapping_table = DataTable;
	OnRemappingTableChanged();
}

UDataTable* UNxLiveLinkRetargetAsset::GetRemappingTable()
{
	return m_remapping_table;
}

void UNxLiveLinkRetargetAsset::SetSkeletalMesh(USkeletalMesh* SkeletalMesh)
{
	m_skeletal_mesh = SkeletalMesh;
	OnSkeletalMeshChanged();
}

