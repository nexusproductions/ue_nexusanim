﻿#include "QuaternionMath.h"

FQuat QuaternionMath::xFlip = FQuat::MakeFromEuler(FVector(0, -90, 0));
FQuat QuaternionMath::yFlip = FQuat::MakeFromEuler(FVector(90, 0, 0));