﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "RBFComponent.h"

DEFINE_LOG_CATEGORY(LogRBFComponent)

// 
// Sets default values for this component's properties
URBFComponent::URBFComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//UE_LOG(LogRBFComponent, Log, TEXT("Initialising Rbf"))
	//Rbf = NewObject<URBF>();
	//UE_LOG(LogRBFComponent, Log, TEXT("Done!"))
	//UE_LOG(LogRBFComponent, Log, TEXT("Rbf pointer has name: %s"), *(Rbf->GetFName().ToString()))
	// ...
}



// Called when the game starts
void URBFComponent::BeginPlay()
{
	Super::BeginPlay();
	SetupRBF();
	// ...
	
}


// Called every frame
void URBFComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UpdateRBF();
	// ...
}

float KalmanQ = 0.000001f;
float KalmanR = 0.00001f;

void URBFComponent::PruneInDataToChannels(TSet<FName> Channels, TMap<FName, float> InputChannels, TMap<FName, float>& PrunedInputChannels) {
	PrunedInputChannels.Reserve(Channels.Num());
	for (FName ChannelName : Channels) {
		PrunedInputChannels.Add(ChannelName, 0.0f);
		if (InputChannels.Contains(ChannelName))
			PrunedInputChannels.Add(ChannelName, InputChannels[ChannelName]);
	}
}

void URBFComponent::CalibrateShape(FName ShapeName, TMap<FName, float> InputChannels, TArray<FNamedCurve> OutputChannels, FRBFShape& OutShape)
{
	OutShape.Name = ShapeName;
	OutShape.InputChannels = CurvesMapToNamedChannelArray(InputChannels);
	OutShape.OutputChannels = OutputChannels;
}

void URBFComponent::SetupRBF()
{
	UE_LOG(LogRBFComponent, VeryVerbose, TEXT("=SetupRBF="));
	// Setup RBF (just do this once)
	// Check we have any shapes
	if (!(CalibrationShapes.Num() > 0)) {
		//UE_LOG(LogRBFComponent, Log, TEXT("No calibration shapes defined (%d). Cancelling RBF setup."), CalibrationShapes.Num())
		return;
	}
	TArray<FRBFShape> CalibrationShapesArray;
	CalibrationShapesArray.Reserve(CalibrationShapes.Num());
	for (TPair<FName, FRBFShape> CalibrationShape : CalibrationShapes) {
		CalibrationShapesArray.Add(CalibrationShape.Value);
	}

	// Bit dirty, get a shape name from the CalibrationShapes map
	TSet<FName> ShapeNames;
	CalibrationShapes.GetKeys(ShapeNames);
	
	FName AnyShape;
	for (FName ShapeName : ShapeNames) {

		AnyShape = ShapeName;
		//break;
	}

	int numShapes = CalibrationShapesArray.Num();
	int numInputs = CalibrationShapesArray[0].InputChannels.Num();
	int numOutputs = CalibrationShapesArray[0].OutputChannels.Num();

	UE_LOG(LogRBFComponent, Verbose, TEXT("Number of Shapes: %d"), numShapes);
	UE_LOG(LogRBFComponent, Verbose, TEXT("Number of Inputs: %d"), numInputs);
	UE_LOG(LogRBFComponent, Verbose, TEXT("Number of Outputs: %d"), numOutputs);


	// Assert we've got a valid array of matching shapes
	for (FRBFShape CalibrationShape : CalibrationShapesArray) {
		if (!CalibrationShape.InputChannels.Num() == numInputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration inputs count mismatch"));
			return;
		}
		if (!CalibrationShape.OutputChannels.Num() == numOutputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration outputs count mismatch"));
			return;
		}
	}
	FNMatrix sampleArray = FNMatrix(numShapes, numInputs);
	UE_LOG(LogRBFComponent, Verbose, TEXT("Constructed sampleArray? r:%d c:%d"), sampleArray.getRows(), sampleArray.getColumns());
	FNMatrix outputArray = FNMatrix(numShapes, numOutputs);
	UE_LOG(LogRBFComponent, Verbose, TEXT("Constructed outputArray? r:%d c:%d"), outputArray.getRows(), outputArray.getColumns());
	if (CalibrationShapesArray.Num() != numShapes) {
		UE_LOG(LogRBFComponent, Warning, TEXT("Calibration shapes count mismatch"));
		return;
	}
	
	for (int i = 0; i < numShapes; i++) {
		if (CalibrationShapesArray[i].InputChannels.Num() != numInputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration shape inputs mismatch"));
			return;
		}
		for (int j = 0; j < numInputs; j++) {
			
			sampleArray(i, j) = CalibrationShapesArray[i].InputChannels[j].Weight;
		}
		if (CalibrationShapesArray[i].OutputChannels.Num() != numOutputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration shape outputs mismatch"));
			return;
		}
		for (int j = 0; j < numOutputs; j++) {
			outputArray(i, j) = CalibrationShapesArray[i].OutputChannels[j].Weight;
		}
	}
	Rbf.SmoothingType = SmoothingType;
	if (SmoothingRadius <= 0) SmoothingRadius = 0.2;
	Rbf.smoothingRadius = SmoothingRadius;
	//UE_LOG(LogRBFComponent, Log, TEXT("Rbf pointer has name: %s"), *(Rbf.GetFName().ToString()))
	Rbf.Init(sampleArray, outputArray);
	if (numOutputs == 0) {
		UE_LOG(LogRBFComponent, Warning, TEXT("numOuputs is zero and this is maybe causing something or whatever?"));
		return;
	}
	RBFOutputs.Init(0.0, numOutputs);

	//KalmanFilters.Init(KalmanFilter(), numOutputs);
	UE_LOG(LogRBFComponent, Log, TEXT("RBF Setup OK"));
	isSetup = true;
}

void URBFComponent::UpdateRBF()
{
	UE_LOG(LogRBFComponent, Verbose, TEXT("=UpdateRBF="));
	if (!isSetup) {
		// UE_LOG(LogRBFComponent, Warning, TEXT("RBF is not setup"));
		SetupRBF();
		return;
		
	}
	
	if (CalibrationShapes.Num() < 1) {
		//UE_LOG(LogRBFComponent, Warning, TEXT("No calibration shapes defined!"))
		return;
	}

	// Bit dirty, get a shape name from the CalibrationShapes map
	TSet<FName> ShapeNames;
	CalibrationShapes.GetKeys(ShapeNames);
	FName AnyShape;
	for (FName ShapeName : ShapeNames) {
		AnyShape = ShapeName;
		break;
	}
	UE_LOG(LogRBFComponent, Verbose, TEXT("Looking at any shape, happens to be: %s"), *AnyShape.ToString())
	int numShapes = CalibrationShapes.Num();
	UE_LOG(LogRBFComponent, Verbose, TEXT("numShapes: %d"), numShapes);
	int numInputs = CalibrationShapes[AnyShape].InputChannels.Num();
	UE_LOG(LogRBFComponent, Verbose, TEXT("numInputs: %d"), numInputs);
	int numOutputs = CalibrationShapes[AnyShape].OutputChannels.Num();
	UE_LOG(LogRBFComponent, Verbose, TEXT("numOutputs: %d"), numOutputs);

	if (!(PositiveBlendShapeOutputs.Num() == numOutputs) && (NegativeBlendShapeOutputs.Num() == numOutputs)) {
		UE_LOG(LogRBFComponent, Warning, TEXT("Positive and Negative outputs lengths does not match number of outputs"));
		return;
	}

	// Assert we've got a valid array of matching shapes
	for (TPair<FName, FRBFShape> CalibrationShape : CalibrationShapes) {
		if (CalibrationShape.Value.InputChannels.Num() != numInputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration shape input numbers does not match number of inputs"));
			return;
		}
		if (CalibrationShape.Value.OutputChannels.Num() != numOutputs) {
			UE_LOG(LogRBFComponent, Warning, TEXT("Calibration shape output numbers does not match number of outputs"));
			return;
		}
	}

	// Should be run every tick
	// Put correct InChannels values into an array in the correct order
	//for (TPair<FName, FRBFShape> CalibrationShape : CalibrationShapes) {
			
	TArray<double> currentSample;
	currentSample.Reserve(numInputs);
	//currentSample.Init(0.0f, numInputs);
	// Make sure these stay in the same order
	for (FNamedCurve InputChannel : CalibrationShapes[AnyShape].InputChannels) {
		// Assert that we're getting the expected input
		if (!InData.Contains(InputChannel.Name)) {
			UE_LOG(LogRBFComponent, Warning, TEXT("InData does not contain channel: %s"), *InputChannel.Name.ToString());
			return;
		}
		//check(InData.Contains(InputChannel.Name));
		UE_LOG(LogRBFComponent, Verbose, TEXT("Adding to currentSample input channel named: %s"), *InputChannel.Name.ToString())
		currentSample.Add(InData[InputChannel.Name]);
	}
	UE_LOG(LogRBFComponent, Verbose, TEXT("currentSample initialised at size: %d"), currentSample.Num())
	
	RBFOutputs = Rbf.Interpolate(currentSample);
	for (double Output : RBFOutputs) {
		UE_LOG(LogRBFComponent, Verbose, TEXT("RBFOutput: %f"), Output)
	}

	UE_LOG(LogRBFComponent, Verbose, TEXT("Mapping to positive/negative outputs"))
	//if(KalmanFilters.Num()!=numOutputs) KalmanFilters.Init(KalmanFilter(), numOutputs);
	if (RBFOutputs.Num() != numOutputs) {
		UE_LOG(LogRBFComponent, Warning, TEXT("RBFOutputs (%d) does not match numOutputs (%d)"), RBFOutputs.Num(), numOutputs);
		return;
	}

	if (numOutputs != PositiveBlendShapeOutputs.Num()) {
		UE_LOG(LogRBFComponent, Warning, TEXT("PositiveBlendShapeOutput count doesn't match number of outputs"));
		return;
	}

	if (numOutputs != NegativeBlendShapeOutputs.Num()) {
		UE_LOG(LogRBFComponent, Warning, TEXT("NegativeBlendShapeOutput count doesn't match number of outputs"));
		return;
	}

	for (int i = 0; i < numOutputs; i++) {	
		float BlendShapeWeight = RBFOutputs[i]; // KalmanFilters[i].Update(RBFOutputs[i], KalmanQ, KalmanR);
		//UE_LOG(LogRBFComponent, Verbose, TEXT("Kalman Filter value. before: %f after:%f"), RBFOutputs[i], BlendShapeWeight)
		if (BlendShapeWeight > 0) {
			UE_LOG(LogRBFComponent, Verbose, TEXT("Value is positive"))
			if (PositiveBlendShapeOutputs[i] != "") {
				UE_LOG(LogRBFComponent, Verbose, TEXT("We have a positive output channel to map to"))
				OutData.Add(PositiveBlendShapeOutputs[i], BlendShapeWeight);
			}
		}
		else {
			UE_LOG(LogRBFComponent, Verbose, TEXT("Value is negative or zero"))
				if (NegativeBlendShapeOutputs[i] != "") {
					UE_LOG(LogRBFComponent, Verbose, TEXT("We have a negative output channel to map to"))
					OutData.Add(NegativeBlendShapeOutputs[i], -BlendShapeWeight);
				}
		}
		//OutChannels.Add(FNamedChannel(CalibrationShapes[0].OutputChannels[i].Name, OutData[i]));
	}

	for (TPair<FName, double> OutDatum : OutData) {
		UE_LOG(LogRBFComponent, Verbose, TEXT("OutData: %s = %f"), *OutDatum.Key.ToString(), OutDatum.Value)
	}

	/*
		// Make sure we haven't been passed a populated array for some reason
		OutChannels.Empty();

		// Put data into output array
		for (int i = 0; i < numOutputs; i++) {
			OutChannels.Add(FNamedChannel(CalibrationShapes[0].OutputChannels[i].Name, OutData[i]));
		}
		*/
	//}
}

TArray<FNamedCurve> URBFComponent::CurvesMapToNamedChannelArray(TMap<FName, float> Curves)
{
	TArray<FNamedCurve> result = TArray<FNamedCurve>();
	for (TPair<FName, float> Curve : Curves) {
		result.Add(FNamedCurve(Curve.Key, Curve.Value));
	}
	return result;
}

TMap<FName, float> URBFComponent::NamedChannelArrayToCurvesMap(TArray<FNamedCurve> Curves)
{
	TMap<FName, float> result;
	for (FNamedCurve Curve : Curves) {
		result.Add(Curve.Name, Curve.Weight);
	}
	return result;
}

