﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNode_SetBoneRotationsByName.h"
#include "NexusAnimRuntime.h"
#include "Animation/AnimInstanceProxy.h"


FAnimNode_SetBoneRotationsByName::FAnimNode_SetBoneRotationsByName()
{
}
void FAnimNode_SetBoneRotationsByName::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);

	BasePose.Initialize(Context);
}

void FAnimNode_SetBoneRotationsByName::CacheBones(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FAnimNode_SetBoneRotationsByName::Update(const FAnimationUpdateContext& Context)
{
	GetEvaluateGraphExposedInputs().Execute(Context);
	BasePose.Update(Context);
}


void FAnimNode_SetBoneRotationsByName::Evaluate_AnyThread(FPoseContext& Output)
{
	// Evaluate the input
	BasePose.Evaluate(Output);
	const FBoneContainer& RequiredBones = Output.Pose.GetBoneContainer();
	for (TPair<FName, FTransform> BoneTransform: BoneRotations)
	{
		const int32 PoseIndex = RequiredBones.GetPoseBoneIndexForBoneName(BoneTransform.Key);
		
		Output.Pose[static_cast<FCompactPoseBoneIndex>(PoseIndex)] = BoneTransform.Value;
		
		//Output.Pose.GetComponentSpaceTransform()
	}
	
}

void FAnimNode_SetBoneRotationsByName::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);
	
	DebugData.AddDebugItem(DebugLine);

	BasePose.GatherDebugData(DebugData);
}

void FAnimNode_SetBoneRotationsByName::EvaluateComponentSpace_AnyThread(FComponentSpacePoseContext& Output)
{
	FBoneContainer BoneContainer = Output.Pose.GetPose().GetBoneContainer();
	// BoneTransforms.KeySort()
	for(TPair<FName, FQuat> RotationMap : BoneRotations)
	{
		FBoneReference BoneReference = FBoneReference(RotationMap.Key);
		if(!BoneReference.IsValidToEvaluate(BoneContainer)) continue;
		// Output.Pose.GetComponentSpaceTransform(BoneReference.GetCompactPoseIndex(BoneContainer));
		FCompactPoseBoneIndex PoseBoneIndex = BoneReference.GetCompactPoseIndex(BoneContainer);
		FTransform BoneTransform = Output.Pose.GetComponentSpaceTransform(PoseBoneIndex);
		BoneTransform.SetRotation(RotationMap.Value);
	}
}
