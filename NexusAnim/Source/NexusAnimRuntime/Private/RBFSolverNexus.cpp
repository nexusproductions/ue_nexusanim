﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "RBFSolverNexus.h"

// NB good value for SMOOTHING RADIUS = 0.2

DEFINE_LOG_CATEGORY(LogRBF)


void URBFSolver::Init(FNMatrix sampleArray, FNMatrix outputValues) {
	UE_LOG(LogRBF, Verbose, TEXT("=RBF.Init="))
	_sampleArray = sampleArray;

	UE_LOG(LogRBF, Verbose, TEXT("_sampleArray: %s"), *_sampleArray.ToString())
	int sampleCount = _sampleArray.getRows();
	int columnCount = _sampleArray.getColumns();
	if (sampleCount == 0 || columnCount == 0) {
		UE_LOG(LogRBF, Warning, TEXT("sampleArray empty? %d:%d"), sampleCount, columnCount)
		return;
	}
	
	_featureMatrix = FNMatrix(sampleCount, columnCount);
	_featureNormals.Init(0.0, columnCount);
	for (int i = 0; i < columnCount; i++) {
		double columnTotal = 0;
		for (int j = 0; j < sampleCount; j++) {
			columnTotal += FMath::Abs(_sampleArray(j, i) * _sampleArray(j, i));
		}
		columnTotal = FMath::Sqrt(columnTotal);

		if (columnTotal != 0) {
			for (int j = 0; j < sampleCount; j++) {
				_featureMatrix(j, i) = (sampleArray(j, i)) / columnTotal;
			}
		}	
		_featureNormals[i] = columnTotal;
	}
	
	UE_LOG(LogRBF, Verbose, TEXT("_featureMatrix: %s"), *_featureMatrix.ToString())
	for (int i = 0; i < _featureNormals.Num(); i++) {
		UE_LOG(LogRBF, Verbose, TEXT("_featureNormals[%d]: %f"), i, _featureNormals[i])
	}

	_distances = FNMatrix(sampleCount, sampleCount);
	_distancesLength = 0;
	for (int i = 0; i < sampleCount; i++) {
		for (int j = 0; j < sampleCount; j++) {
			TArray<double> vector1;
			vector1.Init(0.0, columnCount);
			TArray<double> vector2;
			vector2.Init(0.0, columnCount);

			for (int k = 0; k < columnCount; k++) {
				UE_LOG(LogRBF, Verbose, TEXT("i:%d j:%d k:%d _featureMatrix(i, k): %f _featureMatrix(j, k): %f"), i, j, k, _featureMatrix(i, k), _featureMatrix(j, k))
				vector1[k] = _featureMatrix(i, k);
				vector2[k] = _featureMatrix(j, k);
			}
			_distances(i, j) = Distance(vector1, vector2);
			_distancesLength += _distances(i, j) * _distances(i, j);
		}
	}
	// Normalize distances (In this context it means dividing over the euclidean length of the entire matrix)
	// https://eigen.tuxfamily.org/dox/group__TutorialReductionsVisitorsBroadcasting.html
	// https://eigen.tuxfamily.org/dox/classEigen_1_1MatrixBase.html#ac8da566526419f9742a6c471bbd87e0a
	// https://github.com/chadmv/cmt/blob/master/src/linearRegressionSolver.cpp

	_distancesLength = FMath::Sqrt(_distancesLength);
	
	for (int i = 0; i < sampleCount; i++) {
		for (int j = 0; j < sampleCount; j++) {
			_distances(i, j) /= _distancesLength;
		}
	}
	switch (SmoothingType) {
	case ERBFSmoothingType::None:
		break;
	case ERBFSmoothingType::InverseMultiQuadraticBiharmonic:
		_distances = InverseMultiQuadraticBiharmonic(_distances, smoothingRadius);
		break;
	case ERBFSmoothingType::BeckertWendlandC2Basis:
		_distances = BeckertWendlandC2Basis(_distances, smoothingRadius);
		break;
	default:
		break;
	}
	
	UE_LOG(LogRBF, Verbose, TEXT("_distances: %s"), *_distances.ToString())
	RowMatrix distancesMatrix = MakeRowMatrixFromnxMatrix(_distances);
	
	UE_LOG(LogRBF, Verbose, TEXT("distancesMatrix: %s"), *RowMatrixToString(distancesMatrix))
	RowMatrix outputMatrix = MakeRowMatrixFromnxMatrix(outputValues);
	UE_LOG(LogRBF, Verbose, TEXT("outputMatrix: %s"), *RowMatrixToString(outputMatrix))
	// I don't understand why I'm doing this. The distances matrix is identical across the diagonal anyway.
	RowMatrix transposeSampleMatrix = distancesMatrix.transpose();
	UE_LOG(LogRBF, Verbose, TEXT("transposeSampleMatrix: %s"), *RowMatrixToString(transposeSampleMatrix))

	//MatrixXd pseudoInversed = Eigen::CompleteOrthogonalDecomposition<Eigen::MatrixXd>((transposeSampleMatrix * distancesMatrix));
	RowMatrix pseudoInversed = PseudoInverse(transposeSampleMatrix * distancesMatrix, DBL_EPSILON);
	UE_LOG(LogRBF, Verbose, TEXT("pseudoInversed: %s"), *RowMatrixToString(pseudoInversed))
	MatrixXd thetaMatrix = (pseudoInversed * transposeSampleMatrix);
	UE_LOG(LogRBF, Verbose, TEXT("thetaMatrix: %s"), *RowMatrixToString(thetaMatrix))
	thetaMatrix = (thetaMatrix * outputMatrix).transpose();
	UE_LOG(LogRBF, Verbose, TEXT("thetaMatrix: %s"), *RowMatrixToString(thetaMatrix))
	
	// IOFormat matrixFormat(StreamPrecision, 0, ", ", ";\n", "", "", "[", "]");
	
	_theta = MakenxMatrixFromRowMatrix(thetaMatrix);
	UE_LOG(LogRBF, Verbose, TEXT("_theta: %s"), *_theta.ToString())
	
	//double* thetaData = thetaMatrix.data();
	
	//Eigen::Matrix<double, transposedSampleMatrix.;
	//Smoothing here when implemented
	// nxMatrix matrix =  Matrix.Multiply(Matrix.PseudoInvercse(Matrix.Multiply(transposedSampleMatrix, _distances)), transposedSampleMatrix);

	//_theta = Matrix.Transpose(Matrix.Multiply(matrix, outputValues));

	//_distances(i, j) = 
}

RowMatrix URBFSolver::PseudoInverse(const RowMatrix& a, double epsilon) {

	Eigen::JacobiSVD<MatrixXd> svd(a, Eigen::ComputeThinU | Eigen::ComputeThinV);
	
	double tolerance = epsilon * FMath::Max(a.cols(), a.rows()) * svd.singularValues().array().abs()(0);
	
	return svd.matrixV() *
		(svd.singularValues().array().abs() > tolerance)
		.select(svd.singularValues().array().inverse(), 0)
		.matrix()
		.asDiagonal() *
		svd.matrixU().adjoint();
}

RowMatrix URBFSolver::MakeRowMatrixFromnxMatrix(FNMatrix inMatrix) {
	RowMatrix outMatrix = RowMatrix();
	outMatrix.resize(inMatrix.getRows(), inMatrix.getColumns());
	for (int i = 0; i < inMatrix.getRows(); i++) {
		for (int j = 0; j < inMatrix.getColumns(); j++) {
			outMatrix(i, j) = inMatrix(i, j);
		}
	}
	return outMatrix;
}


FNMatrix URBFSolver::MakenxMatrixFromRowMatrix(RowMatrix inMatrix) {
	FNMatrix outMatrix = FNMatrix(inMatrix.rows(), inMatrix.cols());
	
	for (int i = 0; i < inMatrix.rows(); i++) {
		for (int j = 0; j < inMatrix.cols(); j++) {
			outMatrix(i, j) = inMatrix(i, j);
		}
	}
	return outMatrix;
}

double URBFSolver::Distance(TArray<double> vectorA, TArray<double> vectorB)
{
	UE_LOG(LogRBF, VeryVerbose, TEXT("=Distance="))
	if (vectorA.Num() != vectorB.Num()) {
		UE_LOG(LogRBF, Warning, TEXT("Distance cannot be calculated because size of arrays does not match: A:%d B:%d"), vectorA.Num(), vectorB.Num());
		return 0;
	}
	double accumulator = 0.0;
	for (int i = 0; i < vectorA.Num(); i++) {
		double value = vectorA[i] - vectorB[i];
		UE_LOG(LogRBF, Verbose, TEXT("vectorA[%d]: %f | vectorB[%d]: %f | distance: %f"), i, vectorA[i], i, vectorB[i], value)
		UE_LOG(LogRBF, Verbose, TEXT("%f-%f=%f"), vectorA[i], vectorB[i], vectorA[i]-vectorB[i])
		accumulator += value * value;
	}
	UE_LOG(LogRBF, Verbose, TEXT("Square distance: %f"), accumulator)
	return FMath::Sqrt(accumulator);
}

TArray<double> URBFSolver::Interpolate(TArray<double> newSample) 
{
	UE_LOG(LogRBF, Verbose, TEXT("=Interpolate="))
	UE_LOG(LogRBF, Verbose, TEXT("sampleArray: %s"), &_sampleArray)
	int sampleCount = _sampleArray.getRows();
	UE_LOG(LogRBF, Verbose, TEXT("sampleCount: %d"), sampleCount)
	int columnCount = _sampleArray.getColumns();
	UE_LOG(LogRBF, Verbose, TEXT("columnCount: %d"), columnCount)
	int newSampleCount = newSample.Num();
	UE_LOG(LogRBF, Verbose, TEXT("newSample size: %d"), newSampleCount)

	TArray<double> inputDistance;
	inputDistance.Init(0.0, sampleCount);
	// Normalize the input sample
	for (int i = 0; i < columnCount; i++) {
		UE_LOG(LogRBF, Verbose, TEXT("newSample[%d]: %f"), i, newSample[i])
		if (_featureNormals[i] != 0) {
			newSample[i] /= _featureNormals[i];
			UE_LOG(LogRBF, Verbose, TEXT("(_featureNormal[%d] is not 0, newSample[%d] is now: %f"), i, i, newSample[i])
		}
	}
	// distance from the input to each sample
	for (int i = 0; i < sampleCount; i++) {
		
		TArray<double> sample;
		sample.Init(0.0, columnCount);
		for (int k = 0; k < columnCount; k++) {
			sample[k] = _featureMatrix(i, k);
		}
		
		inputDistance[i] = Distance(newSample, sample);
		inputDistance[i] /= _distancesLength;
		UE_LOG(LogRBF, Verbose, TEXT("Distance returned: %f"), inputDistance[i])
	}
	
	//TArray<double> outputValues; // Matrix.Dot(_theta, inputDistance)
	switch (SmoothingType) {
	case ERBFSmoothingType::None:
		break;
	case ERBFSmoothingType::InverseMultiQuadraticBiharmonic:
		inputDistance = InverseMultiQuadraticBiharmonic(FNMatrix(inputDistance), smoothingRadius).matrix;
		break;
	case ERBFSmoothingType::BeckertWendlandC2Basis:
		inputDistance = BeckertWendlandC2Basis(FNMatrix(inputDistance), smoothingRadius).matrix;
		break;
	default:
		break;
	}
	
	// Switch inputs array over to Eigen Vector type
	VectorXd inputs;
	inputs.resize(inputDistance.Num());
	for (int i = 0; i < inputDistance.Num(); i++) {
		inputs[i] = inputDistance[i];
	}
	

	UE_LOG(LogRBF, Verbose, TEXT("inputs: %s"), *RowMatrixToString(RowMatrix(inputs)))
	//= VectorXd(inputDistance.GetData());
	//outputCount
	VectorXd outputs;
	RowMatrix thetaMatrix = MakeRowMatrixFromnxMatrix(_theta);
	UE_LOG(LogRBF, Verbose, TEXT("theta: %s"), *RowMatrixToString(thetaMatrix))
	outputs.resize(thetaMatrix.rows());
	
	for (int i = 0; i < thetaMatrix.rows(); i++) {
		UE_LOG(LogRBF, Verbose, TEXT("thetaMatrix.row(%d).dot(inputs) = %f"), i, thetaMatrix.row(i).dot(inputs))
		UE_LOG(LogRBF, Verbose, TEXT("inputs.dot(thetaMatrix.col(%d)) = %f"), i, inputs.dot(thetaMatrix.row(i)))
		outputs[i] = inputs.dot(thetaMatrix.row(i));
	}
	//= MakeRowMatrixFromnxMatrix(_theta).dot(inputs);
	TArray<double> outputsT;
	for (double x : outputs) {
		outputsT.Add(x);
	}
	return outputsT;
	
}


FNMatrix URBFSolver::InverseMultiQuadraticBiharmonic(FNMatrix matrix, double radius)
{
	int rowCount = matrix.getRows();
	int columnCount = matrix.getColumns();

	FNMatrix returnMatrix = FNMatrix(rowCount, columnCount);
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			returnMatrix(i, j) = 1.0 / FMath::Sqrt((matrix(i, j) * matrix(i, j)) + (radius * radius));
		}
	}

	return returnMatrix;
}

FNMatrix URBFSolver::BeckertWendlandC2Basis(FNMatrix matrix, double radius)
{
	if (radius < 0.00001) return matrix;
	int rowCount = matrix.getRows();
	int columnCount = matrix.getColumns();

	FNMatrix returnMatrix = FNMatrix(rowCount, columnCount);
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			double v = matrix(i, j) / radius;
			double first = (1.0 - v > 0.0) ? FMath::Pow(1.0 - v, 4) : 0.0;
			double second = 4.0 * v + 1.0;
			returnMatrix(i, j) = first * second;
		}
	}

	return returnMatrix;
}
