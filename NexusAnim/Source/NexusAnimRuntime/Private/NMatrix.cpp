﻿#include "NMatrix.h"

FNMatrix::FNMatrix()
{
	rows = 0;
	columns = 0;
	//matrix = TArray<double>();
};
FNMatrix::FNMatrix(size_t numrows, size_t numcols)
{
	rows = numrows;
	columns = numcols;
	matrix.Init(0.0, numrows * numcols);
}

FNMatrix::FNMatrix(TArray<double> vector) {
	rows = 1;
	columns = vector.Num();
	matrix = vector;
}