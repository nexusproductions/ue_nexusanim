// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class NexusAnimEditor
	: ModuleRules
{
	public NexusAnimEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				//"NexusAnimEditor/Public",
				// ... add public include paths required here ...

			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				//"NexusAnimEditor/Private",
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"NexusAnimRuntime",
				
				"DetailCustomizations",
				"LiveLinkInterface",
				"BlueprintGraph",
				"AnimGraph",
				"Persona",
				"AssetTools",
				"UnrealEd",
				"ToolMenus",
				"LiveLinkComponents",
				"LiveLinkEditor",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore", 
				"AnimGraph", 
				"BlueprintGraph",
				"LiveLinkInterface",
				"LiveLinkMessageBusFramework",
				"LiveLink",
				"LiveLinkComponents",
				
				
				"WorkspaceMenuStructure",
				"EditorStyle",
				"PropertyEditor",
				"SlateCore",
				"Slate",
				"InputCore",
				"AssetTools",
				"UnrealEd",
				"ToolMenus",
				"LiveLinkComponents",
				"LiveLinkEditor",
				
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
