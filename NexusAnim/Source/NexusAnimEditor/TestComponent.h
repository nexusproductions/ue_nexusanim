﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TestComponent.generated.h"


UCLASS(BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class NEXUSANIMEDITOR_API UTestComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTestComponent();
	UPROPERTY(EditAnywhere)
	UPoseAsset* PoseAsset;
	UPROPERTY(EditAnywhere)
	FColor PoseDebugColor = FColor::Black;
	UPROPERTY(EditAnywhere)
	bool DrawAxes = false;
	UPROPERTY(EditAnywhere)
	float AxesLength = 1.0f;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	void UpdatePose();

	UFUNCTION(BlueprintCallable, Category="Test")
	void DebugDrawPose() const;
	};
