﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_SetBoneRotationsByName.h"
#include "NexusAnimEditor.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

USetBoneRotationsByNameAnimGraphNode::USetBoneRotationsByNameAnimGraphNode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor USetBoneRotationsByNameAnimGraphNode::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText USetBoneRotationsByNameAnimGraphNode::GetTooltipText() const
{
	return LOCTEXT("Set Bone Rotations By Name", "Set Bone Rotations By Name");
}

FText USetBoneRotationsByNameAnimGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("Set Bone Rotations By Name", "Set Bone Rotations By Name");
}

FString USetBoneRotationsByNameAnimGraphNode::GetNodeCategory() const
{
	return TEXT("Set Bone Rotations By Name");
}

#undef LOCTEXT_NAMESPACE

