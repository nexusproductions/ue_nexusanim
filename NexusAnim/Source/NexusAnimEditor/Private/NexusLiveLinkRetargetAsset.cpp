// Fill out your copyright notice in the Description page of Project Settings.


#include "NexusLiveLinkRetargetAsset.h"

#include "Roles/LiveLinkAnimationTypes.h"
#include "LiveLinkTypes.h"

#include <map>


UNexusLiveLinkRetargetAsset::UNexusLiveLinkRetargetAsset(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	m_remap_bones_names{ {"Root", EXsensBoneMapping::Root}, {"Pelvis", EXsensBoneMapping::Pelvis}, {"L5", EXsensBoneMapping::L5}, {"L3", EXsensBoneMapping::L3}, {"T12", EXsensBoneMapping::T12}, {"T8", EXsensBoneMapping::T8}, {"Neck", EXsensBoneMapping::Neck}, {"Head", EXsensBoneMapping::Head},
						{"RightShoulder", EXsensBoneMapping::RightShoulder}, {"RightUpperArm", EXsensBoneMapping::RightUpperArm},{"RightForeArm", EXsensBoneMapping::RightForeArm},{"RightHand", EXsensBoneMapping::RightHand},
						{"LeftShoulder", EXsensBoneMapping::LeftShoulder}, {"LeftUpperArm", EXsensBoneMapping::LeftUpperArm},{"LeftForeArm", EXsensBoneMapping::LeftForeArm},{"LeftHand", EXsensBoneMapping::LeftHand},
						{"RightUpperLeg", EXsensBoneMapping::RightUpperLeg},{"RightLowerLeg", EXsensBoneMapping::RightLowerLeg},{"RightFoot", EXsensBoneMapping::RightFoot},{"RightToe", EXsensBoneMapping::RightToe},
						{"LeftUpperLeg", EXsensBoneMapping::LeftUpperLeg},{"LeftLowerLeg", EXsensBoneMapping::LeftLowerLeg},{"LeftFoot", EXsensBoneMapping::LeftFoot},{"LeftToe", EXsensBoneMapping::LeftToe},
						{"Prop1", EXsensBoneMapping::Prop1},{"Prop2", EXsensBoneMapping::Prop2},{"Prop3", EXsensBoneMapping::Prop3},{"Prop4", EXsensBoneMapping::Prop4},
						{"LeftCarpus", EXsensBoneMapping::LeftCarpus},{"LeftFirstMC", EXsensBoneMapping::LeftFirstMC},{"LeftFirstPP", EXsensBoneMapping::LeftFirstPP},{"LeftFirstDP", EXsensBoneMapping::LeftFirstDP},
						{"LeftSecondMC", EXsensBoneMapping::LeftSecondMC},{"LeftSecondPP", EXsensBoneMapping::LeftSecondPP},{"LeftSecondMP", EXsensBoneMapping::LeftSecondMP,},{"LeftSecondDP", EXsensBoneMapping::LeftSecondDP},
						{"LeftThirdMC", EXsensBoneMapping::LeftThirdMC},{"LeftThirdPP", EXsensBoneMapping::LeftThirdPP},{"LeftThirdMP", EXsensBoneMapping::LeftThirdMP},{"LeftThirdDP", EXsensBoneMapping::LeftThirdDP},
						{"LeftFourthMC", EXsensBoneMapping::LeftFourthMC},{"LeftFourthPP", EXsensBoneMapping::LeftFourthPP},{"LeftFourthMP", EXsensBoneMapping::LeftFourthMP},{"LeftFourthDP", EXsensBoneMapping::LeftFourthDP},
						{"LeftFifthMC", EXsensBoneMapping::LeftFifthMC},{"LeftFifthPP", EXsensBoneMapping::LeftFifthPP},{"LeftFifthMP", EXsensBoneMapping::LeftFifthMP},{"LeftFifthDP", EXsensBoneMapping::LeftFifthDP},
						{"RightCarpus", EXsensBoneMapping::RightCarpus},{"RightFirstMC", EXsensBoneMapping::RightFirstMC},{"RightFirstPP", EXsensBoneMapping::RightFirstPP},{"RightFirstDP", EXsensBoneMapping::RightFirstDP},
						{"RightSecondMC", EXsensBoneMapping::RightSecondMC},{"RightSecondPP", EXsensBoneMapping::RightSecondPP},{"RightSecondMP", EXsensBoneMapping::RightSecondMP},{"RightSecondDP", EXsensBoneMapping::RightSecondDP},
						{"RightThirdMC", EXsensBoneMapping::RightThirdMC},{"RightThirdPP", EXsensBoneMapping::RightThirdPP},{"RightThirdMP", EXsensBoneMapping::RightThirdMP},{"RightThirdDP", EXsensBoneMapping::RightThirdDP},
						{"RightFourthMC", EXsensBoneMapping::RightFourthMC},{"RightFourthPP", EXsensBoneMapping::RightFourthPP},{"RightFourthMP", EXsensBoneMapping::RightFourthMP},{"RightFourthDP", EXsensBoneMapping::RightFourthDP},
						{"RightFifthMC", EXsensBoneMapping::RightFifthMC},{"RightFifthPP", EXsensBoneMapping::RightFifthPP},{"RightFifthMP", EXsensBoneMapping::RightFifthMP},{"RightFifthDP", EXsensBoneMapping::RightFifthDP}, },
	m_retarget(100),
	m_doLog(false)
{
}

FName UNexusLiveLinkRetargetAsset::GetRemappedBoneName(const FName& BoneName) const
{
	auto it = m_remap_bones_names.find(BoneName);

	if (it != m_remap_bones_names.end())
	{
	/*	auto it_row = m_remapping_rows.Find(it->second);
		if (it_row)
			return it_row->RemapId;*/
	}

	return BoneName;
}

TArray<FName> UNexusLiveLinkRetargetAsset::RemapBoneNames(const FLiveLinkSkeletonStaticData* InSkeletonData)
{
	const auto& inBoneNames = InSkeletonData->GetBoneNames();
	TArray<FName> remappedBoneNames;
	remappedBoneNames.Reserve(inBoneNames.Num()); // The MVN implementation adds 4 to this, for props perhaps? Let's see if we get errors

	for (const auto& boneName : inBoneNames){
		FName* targetBoneName = BoneNameMap.Find(boneName);
		if (targetBoneName == nullptr) {
			FName newName = GetRemappedBoneName(boneName);
			remappedBoneNames.Add(newName);
			BoneNameMap.Add(boneName, newName);
		}
		else
			remappedBoneNames.Add(*targetBoneName);
	}
	return remappedBoneNames;
}

int32 UNexusLiveLinkRetargetAsset::GetSourceBoneIndex(const FCompactPoseBoneIndex& Index)
{
    return CompactPoseBoneIndices.Find(Index);
}

FTransform UNexusLiveLinkRetargetAsset::GetWorldPose(const FCompactPose& Pose, const FCompactPoseBoneIndex& Index, bool UsePoseCorrection)
{
    if (Index == INDEX_NONE)
    {
        return FTransform::Identity;
    }

    FTransform PoseCorrection = FTransform::Identity;

    if (UsePoseCorrection)
    {
        const auto SourceBoneIndex = GetSourceBoneIndex(Index);

        if (SourceBoneIndex != INDEX_NONE)
        {
            PoseCorrection = FTransform(TargetLocalPoseCorrections[SourceBoneIndex]);
        }
    }

    return (PoseCorrection * Pose[Index] * GetWorldPose(Pose, Pose.GetParentBoneIndex(Index), UsePoseCorrection));
}

FTransform UNexusLiveLinkRetargetAsset::GetWorldReferencePose(const FCompactPose& Pose, const FCompactPoseBoneIndex& Index, bool UsePoseCorrection)
{
    if (Index == INDEX_NONE)
    {
        return FTransform::Identity;
    }

    FTransform PoseCorrection = FTransform::Identity;

    if (UsePoseCorrection)
    {
        const auto SourceBoneIndex = GetSourceBoneIndex(Index);

        if (SourceBoneIndex != INDEX_NONE)
        {
            PoseCorrection = FTransform(TargetLocalPoseCorrections[SourceBoneIndex]);
        }
    }

    return (PoseCorrection * Pose.GetRefPose(Index) * GetWorldReferencePose(Pose, Pose.GetParentBoneIndex(Index), UsePoseCorrection));
}


void UNexusLiveLinkRetargetAsset::BuildPoseFromAnimationData(
	float DeltaTime,
	const FLiveLinkSkeletonStaticData* InSkeletonData,
	const FLiveLinkAnimationFrameData* InFrameData,
	FCompactPose& OutPose)
{
	
	InSkeletonData->BoneParents;
/*
	MYLOG("*********************************************");
	MYLOG("Building for %p", this);

	bool logValid = false;
	std::map<int, int> parentOverride;

	auto TransformedBoneNames = PopulateBoneNames(InSkeletonData);

	FVector uniformScale = OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[0].GetScale3D();

	if (m_retarget++ >= 100 || (InFrameData->Transforms.Num() != m_mvnToUnrealTpose.Num()))
	{
		FBlendedCurve OutCurve;
		calculateTposeValues(OutPose, *InSkeletonData, *InFrameData, OutCurve);
		m_retarget = 0;
	}

	TArray<FTransform> SegData;
	for (int32 i = 0; i < InFrameData->Transforms.Num(); ++i)
	{
		FName BoneName = TransformedBoneNames[i];

		FTransform BoneTransform = InFrameData->Transforms[i];
		int parent = 0;
		SegData.Add(BoneTransform);
		const FName MapBoneName = *BoneNameMap.FindKey(BoneName);

		//set translation and rotation for the pelvis
		if (MapBoneName == "Pelvis")
		{
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			MYLOG("%d Bone index = %d", i, boneIndex);
			if (boneIndex >= 0)
			{
				//scale the pelvis to the correct height with the Xsens data and Unreal skeleton
				float scale = calculateVectorScale(BoneTransform.GetScale3D(), m_tposeWorld[boneIndex].GetTranslation());
				if (isinf(scale))
					scale = calculateVectorScale(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetTranslation(), m_tposeWorld[boneIndex].GetTranslation());
				//Calculate the pelvis rotation using the mvn and tpose rotation
				SegData[i].SetRotation(BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation());
				//scale the position so the pelvis is at the correct height
				SegData[i].SetTranslation(BoneTransform.GetTranslation() * scale);
				SegData[i].SetScale3D(uniformScale);
				MYLOG("final dpos (%f,%f,%f)", SegData[i].GetTranslation()[0], SegData[i].GetTranslation()[1], SegData[i].GetTranslation()[2]);
				BoneTransform = SegData[i];
			}
		}
		else if (i > 23 && i < 29 && MapBoneName.ToString().Contains("Prop"))
		{
			MYLOG("Prop");
			//for all props
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			if (boneIndex >= 0)
			{
				//find the parent bone
				auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(boneIndex);
				FName parentBoneName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(parentBoneIndex);

				const FName* TargetBoneName = BoneNameMap.FindKey(parentBoneName);
				// if the parent is not in the BoneNameMap (retargeted) find the parent of the parent
				while (!TargetBoneName && parentBoneIndex >= 0)
				{
					parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(parentBoneIndex);
					if (parentBoneIndex >= 0)
					{
						parentBoneName = OutPose.GetBoneContainer().GetReferenceSkeleton().GetBoneName(parentBoneIndex);
						TargetBoneName = BoneNameMap.FindKey(parentBoneName);
					}
				}
				if (parentBoneIndex >= 0)
				{
					parent = SegmentInformation::SegmentBoneNames.Find(*TargetBoneName);
					FQuat drot = BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation();
					drot = SegData[parent].GetRotation().Inverse() * drot;
					BoneTransform.SetRotation(drot);
					BoneTransform.SetTranslation(m_mvnToUnrealTpose[i].GetTranslation());
					BoneTransform.SetScale3D(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetScale3D());
				}
			}
		}
		else if (i > 0)
		{
			//if an Xsens bone is not mapped to a bone from the current character find a parent that is for the child to use
			if (BoneName == "None")
			{
				parent = SegmentInformation::parentIndex[i];
				int cur = parent;
				FName parentBoneName = *BoneNameMap.Find(SegmentInformation::SegmentBoneNames[parent]);
				while (parentBoneName == "None")
				{
					cur = parent;
					parent = SegmentInformation::parentIndex[parent];
					parentBoneName = *BoneNameMap.Find(SegmentInformation::SegmentBoneNames[parent]);
				}
				auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(parentBoneName);

				parentOverride[i] = cur;
			}
			else if (BoneName != "p1" && BoneName != "p2" && BoneName != "p3" && BoneName != "p4" && m_mvnToUnrealTpose.Num() > i)
			{
				//xsens parent
				parent = SegmentInformation::parentIndex[i];// -1] + 1;
				//character bone index
				auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
				if (boneIndex >= 0)
				{
					//character parent bone index
					auto parentBoneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(boneIndex);
					MYLOG("%d Bone index = %d, parent = %d, parentBoneIndex = %d", i, boneIndex, parent, parentBoneIndex);

					//if parent is not mapped override it with a parent that is
					auto it = parentOverride.find(parent);
					if (it != parentOverride.end())
					{
						parent = it->second;
						MYLOG("%d Overriding parent index with %d", i, parent);
					}

					MYLOG("Xsens Tpose pos (%f,%f,%f), parent Tpose pos (%f,%f,%f)",
						BoneTransform.GetScale3D()[0], BoneTransform.GetScale3D()[1], BoneTransform.GetScale3D()[2],
						InFrameData->Transforms[parent].GetScale3D()[0], InFrameData->Transforms[parent].GetScale3D()[1], InFrameData->Transforms[parent].GetScale3D()[2]);

					//combine the tpose and mvn rotation
					FQuat drot = BoneTransform.GetRotation() * m_mvnToUnrealTpose[i].GetRotation();
					SegData[i].SetRotation(drot);
					// remove the rotation of the parent from the current segment rotation before applying
					drot = SegData[parent].GetRotation().Inverse() * drot;

					BoneTransform.SetRotation(drot);
					BoneTransform.SetTranslation(m_mvnToUnrealTpose[i].GetTranslation());
					BoneTransform.SetScale3D(OutPose.GetBoneContainer().GetReferenceSkeleton().GetRefBonePose()[boneIndex].GetScale3D());
				}
				else
				{
					MYLOG("%d Bone index = %d, parent = %d", i, boneIndex, parent);
				}
			}
		}
		else
		{
			auto boneIndex = OutPose.GetBoneContainer().GetReferenceSkeleton().FindBoneIndex(BoneName);
			MYLOG("%d Bone index = %d", i, boneIndex);
			if (boneIndex >= 0 && m_tposeWorld.Num() > 0)
			{
				MYLOG("Tpose pos (%f,%f,%f) ori (%f,%f,%f,%f)",
					m_tposeWorld[boneIndex].GetTranslation()[0], m_tposeWorld[boneIndex].GetTranslation()[1], m_tposeWorld[boneIndex].GetTranslation()[2],
					m_tposeWorld[boneIndex].GetRotation().W, m_tposeWorld[boneIndex].GetRotation().X, m_tposeWorld[boneIndex].GetRotation().Y, m_tposeWorld[boneIndex].GetRotation().Z);
			}
		}

		int32 MeshIndex = OutPose.GetBoneContainer().GetPoseBoneIndexForBoneName(BoneName);

		if (MeshIndex != INDEX_NONE)
		{
			FCompactPoseBoneIndex CPIndex = OutPose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(MeshIndex));
			if (CPIndex != INDEX_NONE)
			{
				OutPose[CPIndex] = BoneTransform;
			}
		}
	}
	const TArray<FName>& SourceCurveNames = InSkeletonData->PropertyNames;
	TArray<FName, TMemStackAllocator<>> TransformedCurveNames;
	TransformedCurveNames.Reserve(SourceCurveNames.Num());

	for (const FName& SrcCurveName : SourceCurveNames)
	{
		FName* TargetCurveName = CurveNameMap.Find(SrcCurveName);
		if (TargetCurveName == nullptr)
		{
			FName NewName = SrcCurveName;
			TransformedCurveNames.Add(NewName);
			CurveNameMap.Add(SrcCurveName, NewName);
		}
		else
		{
			TransformedCurveNames.Add(*TargetCurveName);
		}
	}

	if (logValid)
		m_doLog = false;
	*/
}

