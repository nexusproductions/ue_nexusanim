// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_Test.h"
#include "NexusAnimEditor.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

UTestAnimGraphNode::UTestAnimGraphNode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor UTestAnimGraphNode::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UTestAnimGraphNode::GetTooltipText() const
{
	return LOCTEXT("TestAnimGraphNode", "TestAnimGraphNode");
}

FText UTestAnimGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("TestAnimGraphNode", "TestAnimGraphNode");
}

FString UTestAnimGraphNode::GetNodeCategory() const
{
	return TEXT("TestAnimGraphNode");
}

#undef LOCTEXT_NAMESPACE

