﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_SetCurvesByName.h"
#include "NexusAnimEditor.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

USetCurvesByNameAnimGraphNode::USetCurvesByNameAnimGraphNode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor USetCurvesByNameAnimGraphNode::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText USetCurvesByNameAnimGraphNode::GetTooltipText() const
{
	return LOCTEXT("Set Curves By Name", "Set Curves By Name");
}

FText USetCurvesByNameAnimGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("Set Curves By Name", "Set Curves By Name");
}

FString USetCurvesByNameAnimGraphNode::GetNodeCategory() const
{
	return TEXT("Set Curves By Name");
}

#undef LOCTEXT_NAMESPACE

