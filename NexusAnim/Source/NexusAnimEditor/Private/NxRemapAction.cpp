﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "NxRemapAction.h"


// Copyright 2020 Nexus Technologies B.V., Inc. All Rights Reserved.

#include "NxRemapAction.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "Toolkits/AssetEditorManager.h"
#include "EditorStyle.h"
#include "Animation/AnimationAsset.h"
#include "Animation/AnimBlueprint.h"
#include "PhysicsEngine/PhysicsAsset.h"
#include "SSkeletonWidget.h"
#include "Editor/MainFrame/Public/Interfaces/IMainFrameModule.h"
#include "Widgets/Notifications/SNotificationList.h"
#include "Framework/Notifications/NotificationManager.h"
#include "AssetToolsModule.h"
#include "Factories/NxLiveLinkRetargetAssetFactory.h"
#include "Components/RichTextBlock.h"
#include "Factories/NXRemapDataTableAssetFactory.h"
#include "ToolMenuSection.h"
#include "ContentBrowserModule.h"
#include "IContentBrowserSingleton.h"
#include "AssetRegistryModule.h"

#define LOCTEXT_NAMESPACE "NexusRemapAction"

// This is UE 4.23 way of doing things, which no longer works in UE 4.24
//void FNxRemapAction::GetActions(const  TArray<UObject*>& InObjects, FMenuBuilder& MenuBuilder) 
//{ 
//	FAssetToolsModule& AssetToolsModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>(TEXT("AssetTools"));
//	ParentAction->GetActions(InObjects, MenuBuilder);
//
//	FAssetTypeActions_Base::GetActions(InObjects, MenuBuilder);
//	auto Skeletons = GetTypedWeakObjectPtrs<USkeleton>(InObjects);
//	if (Skeletons.Num() == 1)
//	{
//		// create Nexus submenu
//		MenuBuilder.AddSubMenu(
//			LOCTEXT("CreateSkeletonNexusSubmenu", "Nexus"),
//			LOCTEXT("CreateSkeletonNexusSubmenu_ToolTip", "Remapping Functions"),
//			FNewMenuDelegate::CreateSP(this, &FNxRemapAction::NexusSubmenu, Skeletons),
//			false,
//			FSlateIcon()
//		);
//	}
//}

/// Helpers.

// Saves an asset.
void SaveAsset(FString* PackageName, UObject* Asset)
{
	FString PackageFileName = FPackageName::LongPackageNameToFilename(*PackageName, FPackageName::GetAssetPackageExtension());
	UPackage *AssetPackage = CreatePackage(**PackageName);
	GEditor->SavePackage(AssetPackage, Asset, EObjectFlags::RF_Public | EObjectFlags::RF_Standalone, *PackageFileName, GError, nullptr, false, true, SAVE_NoError);
	// Update UE4.25
	//UPackage::SavePackage(AssetPackage, Asset, EObjectFlags::RF_Public | EObjectFlags::RF_Standalone, *PackageFileName, GError, nullptr, true, true, SAVE_NoError);
}

// Finds all derived classes based on base class name.
TSet< FName > FindAllDerivedSubclasses(FName BaseClassName)
{
	// Load the asset registry module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked< FAssetRegistryModule >(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	// Use the asset registry to get the set of all class names deriving from Base
	TSet< FName > DerivedNames;
	{
		TArray< FName > BaseNames;
		BaseNames.Add(BaseClassName);

		const TSet< FName > Excluded;
		AssetRegistry.GetDerivedClassNames(BaseNames, Excluded, DerivedNames);
		return DerivedNames;
	}
}

void FilterAssetPicker(FAssetPickerConfig &AssetPickerConfig)
{
	AssetPickerConfig.bAllowNullSelection = false;
	AssetPickerConfig.InitialAssetViewType = EAssetViewType::Tile;
	AssetPickerConfig.Filter.bRecursivePaths = true;
	AssetPickerConfig.bCanShowFolders = true;
	AssetPickerConfig.bCanShowClasses = true;

	TArray<FName> ClassNames;
	ClassNames.Add(UNexusLiveLinkRetargetAsset::StaticClass()->GetFName());
	TSet<FName> DerivedClassNames;
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked< FAssetRegistryModule >(FName("AssetRegistry"));
	//AssetRegistryModule.Get().GetDerivedClassNames(ClassNames, TSet<FName>(), DerivedClassNames);
	DerivedClassNames.Add(UBlueprint::StaticClass()->GetFName());

	for (auto ClassName : DerivedClassNames)
		AssetPickerConfig.Filter.ClassNames.Add(ClassName);
}

void FNxRemapAction::GetActions(const TArray<UObject*>& InObjects, FToolMenuSection& Section)
{
	FAssetToolsModule& AssetToolsModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>(TEXT("AssetTools"));
	ParentAction->GetActions(InObjects, Section);

	FAssetTypeActions_Base::GetActions(InObjects, Section);
	auto Skeletons = GetTypedWeakObjectPtrs<USkeleton>(InObjects);
	if (Skeletons.Num() == 1)
	{
		// create Nexus submenu
		Section.AddSubMenu(
			"CreateSkeletonNexusSubmenu",
			LOCTEXT("CreateSkeletonNexusSubmenu", "Nexus"),
			LOCTEXT("CreateSkeletonNexusSubmenu_ToolTip", "Remapping Functions"),
			FNewMenuDelegate::CreateSP(this, &FNxRemapAction::NexusSubmenu, Skeletons),
			false,
			FSlateIcon()
		);
	}	
}

void FNxRemapAction::NexusSubmenu(FMenuBuilder& MenuBuilder, TArray<TWeakObjectPtr<USkeleton>> Skeletons) const
{
	if (Skeletons.Num() == 1)
	{
		MenuBuilder.BeginSection("NexusRemapping", LOCTEXT("NexusRemapHeading", "Remapping"));
		{
			MenuBuilder.AddMenuEntry(
				LOCTEXT("Skeleton_Remap", "Create Remap Asset"),
				LOCTEXT("Skeleton_RemapTooltip", "Create Retarget Asset and use this skeleton for Remapping."),
				FSlateIcon(),
				FUIAction(
					FExecuteAction::CreateSP(const_cast<FNxRemapAction*>(this), &FNxRemapAction::RemapAssetAction, Skeletons, true),
					FCanExecuteAction()
				)
			);
			MenuBuilder.AddMenuEntry(
				LOCTEXT("Skeleton_Remap_With_Path", "Create Remap Asset In Folder"),
				LOCTEXT("Skeleton_RemapTooltip_With_Path", "Create Retarget Asset and use this skeleton for Remapping. Allows user to select a path to save the retarget asset to."),
				FSlateIcon(),
				FUIAction(
					FExecuteAction::CreateSP(const_cast<FNxRemapAction*>(this), &FNxRemapAction::RemapAssetAction, Skeletons, false),
					FCanExecuteAction()
				)
			);

			// In case we would want a window selector instead.
			//MenuBuilder.AddMenuEntry(
			//	LOCTEXT("Skeleton_Remap_Modify", "Modify Existing Remap Asset"),
			//	LOCTEXT("Skeleton_Remap_Modify_ToolTip", "Select existing Remap Asset and set skeleton mesh. Creates a new remap data table in case none is set. "),
			//	FSlateIcon(),
			//	FUIAction(
			//		FExecuteAction::CreateSP(const_cast<FNxRemapAction*>(this), &FNxRemapAction::ModifyExisting_Window, Skeletons),
			//		FCanExecuteAction()
			//	)
			//); 


		}
		MenuBuilder.EndSection();
	}
}

void FNxRemapAction::RemapAssetAction(TArray<TWeakObjectPtr<USkeleton>> Skeletons, bool InPlace)
{
	// Determine the starting path. Try to use the most recently used directory
	FString PackageName;
	FString AssetName;
	FString SkeletonPackagePath = FPackageName::GetLongPackagePath(Skeletons[0]->GetOutermost()->GetPathName());
	const FString SkeletonName = FPackageName::GetShortFName(Skeletons[0]->GetOutermost()->GetName()).ToString();
	UNxLiveLinkRetargetAssetFactory* RetargetFactory = NewObject<UNxLiveLinkRetargetAssetFactory>();
	const FAssetToolsModule& AssetToolsModule = FAssetToolsModule::GetModule();


	UDataTable* DataTable;
	UBlueprint* NewNxLiveLinkRetargetAssetBP;

	// Create new Asset remapping bluerprint.
	if (!InPlace)
	{
		NewNxLiveLinkRetargetAssetBP = static_cast<UBlueprint*>(AssetToolsModule.Get().CreateAssetWithDialog(RetargetFactory->GetSupportedClass(),
			RetargetFactory));
		if (NewNxLiveLinkRetargetAssetBP)
			SkeletonPackagePath = FPackageName::GetLongPackagePath(NewNxLiveLinkRetargetAssetBP->GetOutermost()->GetPathName());
	}
	else
	{
		CreateUniqueAssetName(SkeletonPackagePath / RetargetFactory->GetDefaultNewAssetName(), TEXT(""), PackageName, AssetName);
		NewNxLiveLinkRetargetAssetBP = static_cast<UBlueprint*>(AssetToolsModule.Get().CreateAsset(SkeletonName + "_NxRemap" /*AssetName*/, SkeletonPackagePath,
			RetargetFactory->GetSupportedClass(), RetargetFactory));
		//UBlueprint* NewLiveLinkMvnRetargetAssetBP = (UBlueprint*)AssetToolsModule.Get().CreateAssetWithDialog(RetargetFactory->GetSupportedClass(), RetargetFactory);
	}

	if (NewNxLiveLinkRetargetAssetBP == nullptr)
		return;

	// Set remap asset as selected.
	{
		TArray<UObject*> ObjectsToSync;
		ObjectsToSync.Add(NewNxLiveLinkRetargetAssetBP);
		GEditor->SyncBrowserToObjects(ObjectsToSync);
	}

	// Create Data Table
	{
		UNxRemapDataTableAssetFactory* DataTableFactory = NewObject<UNxRemapDataTableAssetFactory>();
		CreateUniqueAssetName(SkeletonPackagePath / DataTableFactory->GetDefaultNewAssetName(), TEXT(""), PackageName, AssetName);
		DataTable = (UDataTable *)AssetToolsModule.Get().CreateAsset(SkeletonName + "_NxRemap_DataTable" /*AssetName*/, SkeletonPackagePath, DataTableFactory->GetSupportedClass(), DataTableFactory);

	}

	//ObjectsToSync.Add(DataTable);

	// Assign default values to the new remap asset that we've created.
	{
		USkeletalMesh* SkeletalMesh = Skeletons[0]->FindCompatibleMesh();

		UNxLiveLinkRetargetAsset* NewNxLiveLinkRetargetAsset = CastChecked<UNxLiveLinkRetargetAsset>(NewNxLiveLinkRetargetAssetBP->GeneratedClass->GetDefaultObject());
		NewNxLiveLinkRetargetAsset->SetRemappingTable(DataTable);
		NewNxLiveLinkRetargetAsset->SetSkeletalMesh(SkeletalMesh);
	}
	 
	// Save assets.
	{
		PackageName = SkeletonPackagePath / SkeletonName + "_NxRemap_DataTable";
		SaveAsset(&PackageName, DataTable);

		PackageName = SkeletonPackagePath / SkeletonName + "_NxRemap";
		SaveAsset(&PackageName, NewNxLiveLinkRetargetAssetBP);
	}
	UAssetEditorSubsystem* AssetEditorSubsystem = GEditor->GetEditorSubsystem<UAssetEditorSubsystem>();
	AssetEditorSubsystem->OpenEditorForAsset(NewNxLiveLinkRetargetAssetBP);
	//FAssetEditorManager::Get().OpenEditorForAsset(NewLiveLinkMvnRetargetAssetBP);
};

#undef LOCTEXT_NAMESPACE
