﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimGraphNode_NexusSkeleton.h"


#define LOCTEXT_NAMESPACE "A3Nodes"

UAnimGraphNode_NexusSkeleton::UAnimGraphNode_NexusSkeleton(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor UAnimGraphNode_NexusSkeleton::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UAnimGraphNode_NexusSkeleton::GetTooltipText() const
{
	return LOCTEXT("NexusSkeletonAnimGraphNode", "NexusSkeletonAnimGraphNode");
}

FText UAnimGraphNode_NexusSkeleton::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("NexusSkeletonAnimGraphNode", "NexusSkeletonAnimGraphNode");
}

FString UAnimGraphNode_NexusSkeleton::GetNodeCategory() const
{
	return TEXT("NexusSkeletonAnimGraphNode");
}

#undef LOCTEXT_NAMESPACE

