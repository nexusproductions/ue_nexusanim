﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_SetBonesByName.h"
#include "NexusAnimEditor.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

UAnimGraphNode_SetBonesByName::UAnimGraphNode_SetBonesByName(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor UAnimGraphNode_SetBonesByName::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UAnimGraphNode_SetBonesByName::GetTooltipText() const
{
	return LOCTEXT("SetBonesByNameAnimGraphNode", "SetBonesByNameAnimGraphNode");
}

FText UAnimGraphNode_SetBonesByName::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("SetBonesByNameAnimGraphNode", "SetBonesByNameAnimGraphNode");
}

FString UAnimGraphNode_SetBonesByName::GetNodeCategory() const
{
	return TEXT("SetBonesByNameAnimGraphNode");
}

#undef LOCTEXT_NAMESPACE

