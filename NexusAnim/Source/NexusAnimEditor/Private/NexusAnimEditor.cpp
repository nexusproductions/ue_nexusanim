// Copyright Epic Games, Inc. All Rights Reserved.

#include "NexusAnimEditor.h"
#include "NxLiveLinkRetargetAsset.h"
#include "AssetToolsModule.h"
#include "Modules/ModuleManager.h"
#include "PropertyEditorModule.h"
#include "PropertyEditorDelegates.h"
#include "IAssetTools.h"
#include "ToolMenus.h"
#include "NexusLiveLinkRetargetAssetCustomisation.h"
#include "NxRemapAction.h"

#define LOCTEXT_NAMESPACE "FNexusAnimEditorModule"

void FNexusAnimEditorModule::StartupModule()
{
	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked< FPropertyEditorModule >(TEXT("PropertyEditor"));
	//This is no longer needed as we customize the map directly.
	//PropertyModule.RegisterCustomPropertyTypeLayout(FRemappingRowHandle::StaticStruct()->GetFName(), FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FRemappingRowCustomization::MakeInstance));
	PropertyModule.RegisterCustomClassLayout(UNxLiveLinkRetargetAsset::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&NexusLiveLinkRetargetAssetCustomisation::MakeInstance));

	// Registering our custom Asset Action.
	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

	TSharedRef<FNxRemapAction> RemapAction = MakeShareable(new FNxRemapAction());
	// We need to grab an original Action menu and set it to our override, so that we don't completely override it.
	RemapAction->SetParent(AssetTools.GetAssetTypeActionsForClass(USkeleton::StaticClass()).Pin());
	AssetTools.RegisterAssetTypeActions(RemapAction);
	CreatedAssetTypeActions.Add(RemapAction);

}

void FNexusAnimEditorModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
	if (FModuleManager::Get().IsModuleLoaded("AssetTools"))
	{
		// Unregister skeleton assets actions from the AssetTools
		IAssetTools& AssetTools = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools").Get();
		for (int32 i = 0; i < CreatedAssetTypeActions.Num(); ++i)
		{
			AssetTools.UnregisterAssetTypeActions(CreatedAssetTypeActions[i].ToSharedRef());
		}
	}

	CreatedAssetTypeActions.Empty();
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FNexusAnimEditorModule, NexusAnimEditor)