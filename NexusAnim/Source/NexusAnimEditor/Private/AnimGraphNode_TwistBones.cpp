﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_TwistBones.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

UAnimGraphNode_TwistBones::UAnimGraphNode_TwistBones(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

FLinearColor UAnimGraphNode_TwistBones::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UAnimGraphNode_TwistBones::GetTooltipText() const
{
	return LOCTEXT("TwistBones", "Twist Bones");
}

FText UAnimGraphNode_TwistBones::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("TwistBones", "Twist Bones");
}

FText UAnimGraphNode_TwistBones::GetControllerDescription() const
{
	return LOCTEXT("TwistBones", "Twist Bones");
}

#undef LOCTEXT_NAMESPACE

