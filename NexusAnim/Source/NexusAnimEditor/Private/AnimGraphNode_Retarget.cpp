// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimGraphNode_Retarget.h"



#include "NexusAnimEditor.h"

#define LOCTEXT_NAMESPACE "A3Nodes"

UAnimGraphNode_Retarget::UAnimGraphNode_Retarget(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

FLinearColor UAnimGraphNode_Retarget::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UAnimGraphNode_Retarget::GetTooltipText() const
{
	return LOCTEXT("RetargetAnimGraphNode", "RetargetAnimGraphNode");
}

FText UAnimGraphNode_Retarget::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("RetargetAnimGraphNode", "RetargetAnimGraphNode");
}

FString UAnimGraphNode_Retarget::GetNodeCategory() const
{
	return TEXT("RetargetAnimGraphNode");
}

#undef LOCTEXT_NAMESPACE

