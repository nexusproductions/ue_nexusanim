﻿// Copyright 2020 Xsens Technologies B.V., Inc. All Rights Reserved.

#include "NexusLiveLinkRetargetAssetCustomisation.h"
#include "DetailCategoryBuilder.h"
#include "DetailLayoutBuilder.h"
#include "IDetailChildrenBuilder.h"
#include "IDetailPropertyRow.h"

#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SComboBox.h"
#include "PropertyCustomizationHelpers.h"
#include "NxLiveLinkRetargetAsset.h"
#include "PropertyHandle.h"
#include "NxMapBuilder.h"


#define LOCTEXT_NAMESPACE "NexusLiveLinkRetargetAssetCustomisation"
static FComboBoxStyle* DefaultComboBoxStyle = nullptr;


TSharedRef<IDetailCustomization> NexusLiveLinkRetargetAssetCustomisation::MakeInstance()
{
	return MakeShareable(new NexusLiveLinkRetargetAssetCustomisation);
}

void NexusLiveLinkRetargetAssetCustomisation::CustomizeDetails(IDetailLayoutBuilder & DetailBuilder)
{
	// Create a category so this is displayed early in the properties
	DetailBuilder.EditCategory("Default");
	DetailBuilder.EditCategory("Reference Pose");
	IDetailCategoryBuilder& LiveLinkCategory = DetailBuilder.EditCategory("Live Link Mvn Remap");
	
	TSharedPtr<IPropertyHandle> RecipesProperty = DetailBuilder.GetProperty((GET_MEMBER_NAME_CHECKED(UNxLiveLinkRetargetAsset, m_remapping_rows)));
	TSharedRef<FNxDetailMapBuilder> MapPropertyBuilder = MakeShareable(new FNxDetailMapBuilder(RecipesProperty.ToSharedRef(), this, true, false, false));
	
	// Re-add this property so it is displayed above the remapping table
	LiveLinkCategory.AddProperty(DetailBuilder.GetProperty((GET_MEMBER_NAME_CHECKED(UNxLiveLinkRetargetAsset, m_remapping_table))));
	LiveLinkCategory.AddCustomBuilder(MapPropertyBuilder, false);

	// Generate default combo box style
	DefaultComboBoxStyle = new FComboBoxStyle(FCoreStyle::Get().GetWidgetStyle<FComboBoxStyle>("ComboBox"));
}

void NexusLiveLinkRetargetAssetCustomisation::GenerateChildWidget(IDetailChildrenBuilder& ChildrenBuilder, TSharedRef<IPropertyHandle> ItemProperty)
{
	TSharedPtr<IPropertyHandle> ChildHandle = ItemProperty->GetKeyHandle();
	uint8 xsensRemapId;
	ItemProperty->GetKeyHandle()->GetValue(xsensRemapId);

	// Remapping table that is stored in FRemappingRowHandle parent class.
	UDataTable* dataTable;
	ItemProperty->GetChildHandle(GET_MEMBER_NAME_CHECKED(FNxRemappingRowHandle, DataTable))->GetValue((UObject*&)dataTable);
	UpdateRowNames(dataTable);

	TSharedPtr<IPropertyHandle> RemapIdHandle = ItemProperty->GetChildHandle(GET_MEMBER_NAME_CHECKED(FNxRemappingRowHandle, RemapId));

	// Generate our UI properties.
	ChildrenBuilder.AddCustomRow(LOCTEXT("Remapping", "Remapping Row"))
	.NameContent()
	[
		SNew(STextBlock).ToolTipText(LOCTEXT("RemapFrom", "XSens bone name to remap from.")).Text(GETENUM_DISPLAYNAME("ENxXsensMapping", static_cast<ENxXsensMapping>(xsensRemapId)))
	]
	.ValueContent()
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot()
		[
			SNew(SComboBox<TSharedPtr<FString>>)
			.ComboBoxStyle(DefaultComboBoxStyle)
			.ToolTipText(LOCTEXT("RemapTo", "Choose the bone to remap to."))
			.OptionsSource(&m_row_names)
			.OnGenerateWidget_Lambda ( [](TSharedPtr<FString> InItem) {
				return SNew(STextBlock).Margin(FMargin(4, 2))
				.Text(FText::FromString(*InItem));
			})
			.OnSelectionChanged_Lambda([=](TSharedPtr<FString> Selection, ESelectInfo::Type){
				if (RemapIdHandle->IsValidHandle()) {
					RemapIdHandle->SetValue(*Selection);
				}
			})
			.ContentPadding(FMargin(4,2))
			[
				SNew(STextBlock)
				.Font(IDetailLayoutBuilder::GetDetailFont())
				.Text_Lambda( [=]() -> FText
				{
					if (RemapIdHandle->IsValidHandle())
					{
						FName val;
						RemapIdHandle->GetValue(val);
						return FText::FromName(val);
					}

					return FText::GetEmpty();
				} )
			]
		]
	];
}

//This perhaps has a better place as part of FRemappingRowHandle
void NexusLiveLinkRetargetAssetCustomisation::UpdateRowNames(const UDataTable* DataTable)
{
	TArray<FString> RowNames{ "None" };
	m_row_names.Empty();
	if (DataTable != nullptr)
	{
		for (TMap<FName, uint8*>::TConstIterator Iterator(DataTable->GetRowMap()); Iterator; ++Iterator)
		{
			FString RowString = Iterator.Key().ToString();
			RowNames.Add(RowString);
		}
	}

	for (const FString& RowString : RowNames)
	{
		TSharedRef<FString> RowNameItem = MakeShareable(new FString(RowString));
		m_row_names.Add(RowNameItem);
	}
}

#undef LOCTEXT_NAMESPACE