﻿#pragma once
#include "NexusLiveLinkRetargetAsset.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "NxRemappingRow.h"

#include "Factories/Factory.h"
#include "NxRemapDataTableAssetFactory.generated.h"

UCLASS()
class NEXUSANIMEDITOR_API UNxRemapDataTableAssetFactory : public UFactory
{
	GENERATED_BODY()

public:
	UNxRemapDataTableAssetFactory(const FObjectInitializer& ObjectInitializer);
	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;

	UDataTable* DataTable;
};