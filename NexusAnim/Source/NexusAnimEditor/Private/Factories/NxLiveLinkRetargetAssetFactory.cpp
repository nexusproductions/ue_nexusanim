﻿#include "NxLiveLinkRetargetAssetFactory.h"
#include "NxLiveLinkRetargetAsset.h"
#include "UObject/Object.h"
#include "UnrealEd.h"
#include "Kismet2/KismetEditorUtilities.h"
#include "KismetCompilerModule.h"

UNxLiveLinkRetargetAssetFactory::UNxLiveLinkRetargetAssetFactory(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UNxLiveLinkRetargetAsset::StaticClass();
}

UObject* UNxLiveLinkRetargetAssetFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	if (MvnRetargetAsset != nullptr)
	{
		MvnRetargetAsset->SetFlags(Flags | RF_NoFlags);
		MvnRetargetAsset->Modify();
		MvnRetargetAsset->Rename(*Name.ToString(), InParent);
	}
	else
	{
		UClass* BlueprintClass = nullptr;
		UClass* BlueprintGeneratedClass = nullptr;

		IKismetCompilerInterface& KismetCompilerModule = FModuleManager::LoadModuleChecked<IKismetCompilerInterface>("KismetCompiler");
		KismetCompilerModule.GetBlueprintTypesForClass(Class, BlueprintClass, BlueprintGeneratedClass);

		MvnRetargetAsset = FKismetEditorUtilities::CreateBlueprint(Class, InParent, Name, BPTYPE_Normal, BlueprintClass, BlueprintGeneratedClass);

	}
	return MvnRetargetAsset;
}