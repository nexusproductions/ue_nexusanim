﻿#include "NxRemapDataTableAssetFactory.h"
#include "NexusLiveLinkRetargetAsset.h"
#include "UObject/Object.h"
#include "UnrealEd.h"
#include "Kismet2/KismetEditorUtilities.h"
#include "KismetCompilerModule.h"
#include "NxLiveLinkRetargetAsset.h"

UNxRemapDataTableAssetFactory::UNxRemapDataTableAssetFactory(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UDataTable::StaticClass();
}

UObject* UNxRemapDataTableAssetFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	if (DataTable != nullptr)
	{
		DataTable->SetFlags(Flags | RF_NoFlags);
		DataTable->Modify();
		DataTable->Rename(*Name.ToString(), InParent);
	}
	else
	{
		UClass* BlueprintClass = nullptr;
		UClass* BlueprintGeneratedClass = nullptr;

		IKismetCompilerInterface& KismetCompilerModule = FModuleManager::LoadModuleChecked<IKismetCompilerInterface>("KismetCompiler");
		KismetCompilerModule.GetBlueprintTypesForClass(Class, BlueprintClass, BlueprintGeneratedClass);

		DataTable = NewObject<UDataTable>(InParent, Class, Name, Flags | RF_Standalone);
		DataTable->RowStruct = FNxRemappingRow::StaticStruct();
	}
	return DataTable;
}