﻿#pragma once
#include "NxLiveLinkRetargetAsset.h"
#include "Factories/Factory.h"
#include "NxLiveLinkRetargetAssetFactory.generated.h"


UCLASS()
class NEXUSANIMEDITOR_API UNxLiveLinkRetargetAssetFactory : public UFactory
{
	GENERATED_BODY()

public:
	UNxLiveLinkRetargetAssetFactory(const FObjectInitializer& ObjectInitializer);
	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;

	UBlueprint* MvnRetargetAsset;
};