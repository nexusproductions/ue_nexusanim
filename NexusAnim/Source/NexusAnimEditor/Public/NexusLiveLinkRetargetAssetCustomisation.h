﻿// Copyright 2020 Xsens Technologies B.V., Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "IDetailCustomization.h"
#include "NxMappingEnum.h"
#include "Engine\DataTable.h"
#include "NxMapBuilder.h"

class IPropertyHandle;
class IDetailChildrenBuilder;

class NexusLiveLinkRetargetAssetCustomisation : public IDetailCustomization, IDetailMapBuilderCallback
{
public:

	static TSharedRef<IDetailCustomization> MakeInstance();

	void GenerateChildWidget(IDetailChildrenBuilder& ChildrenBuilder, TSharedRef<IPropertyHandle> ItemProperty);
	
private:

	// This is where all the UI overriding magic is hapenning.
	virtual void CustomizeDetails(IDetailLayoutBuilder & DetailBuilder) override;

	void UpdateRowNames(const UDataTable* DataTable);

private:
	TArray<TSharedPtr<FString>> m_row_names;
};


