﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AnimNode_TwistBones.h"
#include "AnimGraphNode_Base.h"
#include "AnimGraphNode_SkeletalControlBase.h"
#include "AnimGraphNode_TwistBones.generated.h"



UCLASS()
class NEXUSANIMEDITOR_API UAnimGraphNode_TwistBones : public UAnimGraphNode_SkeletalControlBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_TwistBones Node;

	//~ Begin UEdGraphNode Interface.
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	//~ End UEdGraphNode Interface.

	protected:
	// UAnimGraphNode_SkeletalControlBase interface
	virtual FText GetControllerDescription() const override;
	virtual const FAnimNode_SkeletalControlBase* GetNode() const override { return &Node; }
	// End of UAnimGraphNode_SkeletalControlBase interface

	UAnimGraphNode_TwistBones(const FObjectInitializer& ObjectInitializer);
};
