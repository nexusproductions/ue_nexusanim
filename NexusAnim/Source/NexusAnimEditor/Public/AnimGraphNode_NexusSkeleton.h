﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AnimNode_NexusSkeleton.h"
#include "AnimGraphNode_Base.h"
#include "AnimGraphNode_NexusSkeleton.generated.h"

/**
 * 
 */
UCLASS()
class NEXUSANIMEDITOR_API UAnimGraphNode_NexusSkeleton : public UAnimGraphNode_Base
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_NexusSkeleton Node;

	//~ Begin UEdGraphNode Interface.
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	//~ End UEdGraphNode Interface.

	//~ Begin UAnimGraphNode_Base Interface
	virtual FString GetNodeCategory() const override;
	//~ End UAnimGraphNode_Base Interface

	UAnimGraphNode_NexusSkeleton(const FObjectInitializer& ObjectInitializer);
};
