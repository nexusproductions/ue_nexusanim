// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "CoreMinimal.h"
#include "NexusSkeleton.h"
#include "LiveLinkRetargetAsset.h"
#include "LiveLinkTypes.h"
#include "RetargetNexusSkeleton.h"
#include "XsensBoneMappingEnum.h"

#include "NexusLiveLinkRetargetAsset.generated.h"

/**
 * 
 */
UCLASS()
class NEXUSANIMEDITOR_API UNexusLiveLinkRetargetAsset : public ULiveLinkRetargetAsset
{
	GENERATED_BODY()
	// Need to include this declaration when using GENERATED_BODY rather than GENERATED_UCLASS_BODY
	UNexusLiveLinkRetargetAsset(const FObjectInitializer& ObjectInitializer);
	
	virtual void BuildPoseFromAnimationData(float DeltaTime, const FLiveLinkSkeletonStaticData* InSkeletonData, const FLiveLinkAnimationFrameData* InFrameData, FCompactPose& OutPose) override;
	FName GetRemappedBoneName(const FName& BoneName) const;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bone Mapping")
		TMap<FName, FName> BoneNameMap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool DebugTPose;

	/* Map that stores all the information about bone remapping. This will be populated dynamically from the skeletal mesh. */
	//UPROPERTY(EditAnywhere, DisplayName = "Remapping Assets", Category = "Nexus Anim")
	//TMap<EXsensBoneMapping, FRemappingRowHandle> m_remapping_rows;
	
private:
	std::map<FName, EXsensBoneMapping, FNameFastLess> m_remap_bones_names;

	TArray<FName> RemapBoneNames(const FLiveLinkSkeletonStaticData* InSkeletonData);
	//UNxSkeletonMocap LiveLinkSkeleton;
	//UNxSkeletonMocap TargetSkeleton;
	
	TArray<FQuat> TargetLocalPoseCorrections;
	TArray<FCompactPoseBoneIndex> CompactPoseBoneIndices;

	int32 GetSourceBoneIndex(const FCompactPoseBoneIndex& Index);

	FTransform GetWorldPose(const FCompactPose& Pose, const FCompactPoseBoneIndex& Index, bool UsePoseCorrection);
	FTransform GetWorldReferencePose(const FCompactPose& Pose, const FCompactPoseBoneIndex& Index, bool UsePoseCorrection);
	
	int m_retarget;

	bool m_doLog;
};
