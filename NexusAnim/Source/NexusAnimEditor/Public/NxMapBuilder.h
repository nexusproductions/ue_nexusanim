﻿// Copyright 2020 Xsens Technologies B.V., Inc. All Rights Reserved.

#pragma once

#include "PropertyHandle.h"
#include "SResetToDefaultMenu.h"
#include "Components/HorizontalBox.h"
#include "IDetailCustomNodeBuilder.h"
#include "DetailWidgetRow.h"
#include "NxMappingEnum.h"
#include "NxRemappingRow.h"
#include "IDetailPropertyRow.h"
#include "IDetailChildrenBuilder.h"
#include "Templates/SharedPointer.h"
#include "map"
#include "Widgets/Input/SComboBox.h"
#include "DetailLayoutBuilder.h"
#include "functional"

#define LOCTEXT_NAMESPACE "DetailMapBuilder"

// This Macro is used to get the display name out of UEnum
#define GETENUM_DISPLAYNAME(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetDisplayNameTextByIndex((int32)evalue) : FText::FromName("Invalid - are you sure enum uses UENUM() macro?") )

class IDetailMapBuilderCallback
{
public:
	virtual void GenerateChildWidget(IDetailChildrenBuilder& ChildrenBuilder, TSharedRef<IPropertyHandle> ItemProperty) = 0;
};

class FNxDetailMapBuilder : public IDetailCustomNodeBuilder
{
public:

	FNxDetailMapBuilder(TSharedRef<IPropertyHandle> InBaseProperty, IDetailMapBuilderCallback* ChildGeneratorFunc, bool InGenerateHeader = true, bool InDisplayResetToDefault = true, bool InDisplayElementNum = true)
		: MapProperty(InBaseProperty->AsMap())
		, BaseProperty(InBaseProperty)
		, MapBuilderCallbackClass(ChildGeneratorFunc)
		, bGenerateHeader(InGenerateHeader)
		, bDisplayResetToDefault(InDisplayResetToDefault)
		, bDisplayElementNum(InDisplayElementNum)
	{
		check(MapProperty.IsValid());

		// Delegate for when the number of children in the array changes
		FSimpleDelegate OnNumChildrenChanged = FSimpleDelegate::CreateRaw(this, &FNxDetailMapBuilder::OnNumChildrenChanged);
		MapProperty->SetOnNumElementsChanged(OnNumChildrenChanged);

		BaseProperty->MarkHiddenByCustomization();
	}

	~FNxDetailMapBuilder()
	{
		FSimpleDelegate Empty;
		MapProperty->SetOnNumElementsChanged(Empty);
	}

	void SetDisplayName(const FText& InDisplayName)
	{
		DisplayName = InDisplayName;
	}

	virtual bool RequiresTick() const override { return false; }

	virtual void Tick(float DeltaTime) override {}

	virtual FName GetName() const override
	{
		return BaseProperty->GetProperty()->GetFName();
	}

	virtual bool InitiallyCollapsed() const override { return false; }

	virtual void GenerateHeaderRowContent(FDetailWidgetRow& NodeRow) override
	{
		if (bGenerateHeader)
		{
			const bool bDisplayResetToDefaultInNameContent = false;

			TSharedPtr<SHorizontalBox> ContentHorizontalBox;
			SAssignNew(ContentHorizontalBox, SHorizontalBox);
			if (bDisplayElementNum)
			{
				ContentHorizontalBox->AddSlot()
					[
						BaseProperty->CreatePropertyValueWidget()
					];
			}

			NodeRow
			.FilterString(!DisplayName.IsEmpty() ? DisplayName : BaseProperty->GetPropertyDisplayName())
			.NameContent()
			[
				BaseProperty->CreatePropertyNameWidget(DisplayName, FText::GetEmpty(), bDisplayResetToDefaultInNameContent)
			]
			.ValueContent()
			[
				ContentHorizontalBox.ToSharedRef()
			];

			if (bDisplayResetToDefault)
			{
				TSharedPtr<SResetToDefaultMenu> ResetToDefaultMenu;
				ContentHorizontalBox->AddSlot()
					.AutoWidth()
					.Padding(FMargin(2.0f, 0.0f, 0.0f, 0.0f))
					[
						SAssignNew(ResetToDefaultMenu, SResetToDefaultMenu)
					];
				ResetToDefaultMenu->AddProperty(BaseProperty);
			}
		}
	}

	virtual void GenerateChildContent(IDetailChildrenBuilder& ChildrenBuilder) override
	{
		uint32 NumChildren = 0;
		MapProperty->GetNumElements(NumChildren);

		for (uint32 ChildIndex = 0; ChildIndex < NumChildren; ++ChildIndex)
		{
			TSharedRef<IPropertyHandle> ItemProperty = BaseProperty->GetChildHandle(ChildIndex).ToSharedRef();
			if (ItemProperty->IsValidHandle())
			{
				MapBuilderCallbackClass->GenerateChildWidget(ChildrenBuilder, ItemProperty);
			}
		}
	}

	virtual void RefreshChildren()
	{
		OnRebuildChildren.ExecuteIfBound();
	}

	virtual TSharedPtr<IPropertyHandle> GetPropertyHandle() const
	{
		return BaseProperty;
	}

protected:

	virtual void SetOnRebuildChildren(FSimpleDelegate InOnRebuildChildren) override { OnRebuildChildren = InOnRebuildChildren; }

	void OnNumChildrenChanged()
	{
		OnRebuildChildren.ExecuteIfBound();
	}

private:

	FText DisplayName;
	TSharedPtr<IPropertyHandleMap> MapProperty;
	TSharedRef<IPropertyHandle> BaseProperty;
	FSimpleDelegate OnRebuildChildren;
	IDetailMapBuilderCallback* MapBuilderCallbackClass;
	bool bGenerateHeader;
	bool bDisplayResetToDefault;
	bool bDisplayElementNum;
};

#undef LOCTEXT_NAMESPACE
