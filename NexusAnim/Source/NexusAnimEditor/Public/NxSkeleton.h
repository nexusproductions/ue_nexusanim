﻿#pragma once

class NxSkeleton
{
public:
	NxSkeleton() = default;
	
private:
	TArray<TSharedPtr<FTransform>> JointTransformsArray;
	TArray<FName> JointNamesArray;
	TArray<int32> ParentsArray;
	int32 Length = 0;
	
	TSharedPtr<FTransform> operator[](const int32 JointIndex)
	{
		if(!JointTransformsArray.IsValidIndex(JointIndex)) return nullptr;
		return JointTransformsArray[JointIndex];
	}

	TSharedPtr<FTransform> operator[](const FName JointName)
	{
		const int32 JointIndex = JointIndexFromName(JointName);
		return this->operator[](JointIndex);
	}

	int32 JointIndexFromPointer(const TSharedPtr<FTransform> TransformPointer) const
	{
		return JointTransformsArray.Find(TransformPointer);
	}

	int32 JointIndexFromName(const FName JointName) const
	{
		return JointNamesArray.Find(JointName);
	}
	
};
