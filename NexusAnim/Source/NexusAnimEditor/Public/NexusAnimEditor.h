// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "NexusAnimBPLib.h"
#include "Modules/ModuleManager.h"
#include "IAssetTypeActions.h"

class FNexusAnimEditorModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
private:
	TArray<TSharedPtr<IAssetTypeActions>> CreatedAssetTypeActions;
	
};
