﻿#pragma once

//#include "SimpleSkeleton.generated.h"

/*
UCLASS(BlueprintType)
class USkeletalNode : public UObject
{
public:
	GENERATED_BODY()

	USkeletalNode() = default;

	USkeletalNode(
		const FName& NodeName,
		const TSharedPtr<USkeletalNode>& Parent=nullptr,
		const FTransform& LocalTransform=FTransform::Identity,
		const TArray<TSharedPtr<USkeletalNode>> Children=TArray<TSharedPtr<USkeletalNode>>()) : NodeName(NodeName),
		  Parent(Parent),
		  Children(Children),
		NumChildren(Children.Num()),
	LocalTransform(LocalTransform)
	{
	}
	
private:
	FName NodeName;
	TSharedPtr<USkeletalNode> Parent;
	TArray<TSharedPtr<USkeletalNode>> Children;
	int32 NumChildren;
	FTransform LocalTransform;
	
public:

	FName GetNodeName() const
	{
		return NodeName;
	}

	void SetName(FName NewName)
	{
		NodeName = NewName;
	}
	
	void SetLocalTransform(FTransform Transform)
	{
		LocalTransform = Transform;
	} 
	
	void SetWorldTransform(FTransform Transform)
	{
		LocalTransform = Parent->GetWorldTransform().Inverse() * Transform;
	}

	FTransform GetLocalTransform() const
	{
		return LocalTransform;
	}
	
	FTransform GetWorldTransform() const
	{
		return Parent->GetWorldTransform() * LocalTransform;
	}

	void AddChild(USkeletalNode Child)
	{
		Children.Add(MakeShared<USkeletalNode>(Child));
	}
	
};

UCLASS(BlueprintType)
class USimpleSkeleton : public UObject
{
	GENERATED_BODY()

	TArray<USkeletalNode> Joints;
	
	
};

UCLASS(BlueprintType)
class USimpleBipedSkeleton : public UObject
{
	GENERATED_BODY()

	USkeletalNode Reference;
	
};
*/