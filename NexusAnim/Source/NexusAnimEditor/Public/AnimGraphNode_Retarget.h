// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AnimNode_Retarget.h"
#include "AnimGraphNode_Base.h"
// This include must be here, otherwise we get a cvompile error, as we can't include external editor/dev modules in a runtime module... I think
#include "LiveLinkTypes.h"
#include "Roles/LiveLinkAnimationRole.h"
#include "Roles/LiveLinkAnimationBlueprintStructs.h"

#include "AnimGraphNode_Retarget.generated.h"


UCLASS()
class NEXUSANIMEDITOR_API UAnimGraphNode_Retarget : public UAnimGraphNode_Base
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, Category = Settings)
		FAnimNode_Retarget Node;

	//~ Begin UEdGraphNode Interface.
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	//~ End UEdGraphNode Interface.

	//~ Begin UAnimGraphNode_Base Interface
	virtual FString GetNodeCategory() const override;
	//~ End UAnimGraphNode_Base Interface

	UAnimGraphNode_Retarget(const FObjectInitializer& ObjectInitializer);
public:
	
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Retarget, meta = (PinShownByDefault))
	//	FSubjectFrameHandle LiveLinkAnimationFrameData;

};
