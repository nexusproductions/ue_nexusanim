﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AnimNode_SetCurvesByName.h"
#include "AnimGraphNode_Base.h"
#include "AnimGraphNode_SetCurvesByName.generated.h"


UCLASS()
class NEXUSANIMEDITOR_API USetCurvesByNameAnimGraphNode : public UAnimGraphNode_Base
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_SetCurvesByName Node;

	//~ Begin UEdGraphNode Interface.
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	//~ End UEdGraphNode Interface.

	//~ Begin UAnimGraphNode_Base Interface
	virtual FString GetNodeCategory() const override;
	//~ End UAnimGraphNode_Base Interface

	USetCurvesByNameAnimGraphNode(const FObjectInitializer& ObjectInitializer);
};
