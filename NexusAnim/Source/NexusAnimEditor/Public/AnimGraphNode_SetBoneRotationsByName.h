﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AnimNode_SetBoneRotationsByName.h"
#include "AnimGraphNode_Base.h"
#include "AnimGraphNode_SetBoneRotationsByName.generated.h"


UCLASS()
class NEXUSANIMEDITOR_API USetBoneRotationsByNameAnimGraphNode : public UAnimGraphNode_Base
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_SetBoneRotationsByName Node;

	//~ Begin UEdGraphNode Interface.
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	//~ End UEdGraphNode Interface.

	//~ Begin UAnimGraphNode_Base Interface
	virtual FString GetNodeCategory() const override;
	//~ End UAnimGraphNode_Base Interface

	USetBoneRotationsByNameAnimGraphNode(const FObjectInitializer& ObjectInitializer);
};
