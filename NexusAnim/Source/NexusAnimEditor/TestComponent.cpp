﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TestComponent.h"
#include "DrawDebugHelpers.h"

#include "Animation/PoseAsset.h"


// Sets default values for this component's properties
UTestComponent::UTestComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTestComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTestComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	// ...
}

void UTestComponent::UpdatePose()
{

}

void NEXUSANIMEDITOR_API UTestComponent::DebugDrawPose() const
{
	if (!IsValid(PoseAsset)) return;
	if (PoseAsset->GetNumPoses() == 0 ) return;
	TArray<FTransform> PoseTransforms;
	
	PoseAsset->GetFullPose(0, PoseTransforms);
	for (int i = 0; i<PoseTransforms.Num(); i++)
	{
		FTransform ChildTransform = PoseTransforms[i];
		FTransform ParentTransform;
		const int32 ParentIndex = PoseAsset->GetSkeleton()->GetReferenceSkeleton().GetParentIndex(i);
		if(PoseTransforms.IsValidIndex(ParentIndex))
		{
			ParentTransform = PoseTransforms[ParentIndex];
		}
		DrawDebugLine(GetWorld(),
			ChildTransform.GetLocation(),ParentTransform.GetLocation(),
			PoseDebugColor, false, 0);
		if (DrawAxes) {
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(),
				ChildTransform.GetLocation() + ChildTransform.GetRotation().GetRightVector() * AxesLength, FColor::Red);
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(),
				ChildTransform.GetLocation() + ChildTransform.GetRotation().GetForwardVector() * AxesLength, FColor::Green);
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(),
				ChildTransform.GetLocation() + ChildTransform.GetRotation().GetUpVector() * AxesLength, FColor::Blue);
		}
	}
}




